import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
  Row,
} from 'reactstrap';

const styles = { color: 'blue' }

class DatosConsultaExterna extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  /*  async componentDidMount() {
     try {
       const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscahojaevolucion/' + this.props.consulta.hoja_idcompromiso + '/', {
         headers: {
           Authorization: `JWT ${localStorage.getItem('token')}`,
         }
       })
       const json1 = await respuesta1.json();
       this.setState({ consultadet: json1 });
 
     } catch (e) {
       console.log(e);
     }
   } */

  render() {
    let hojae = this.props.consulta.map((item, indice) => (
      <tr key={indice}>
        <td align="center">{indice + 1}</td>
        <td align="center">{item.hoja_fecha}</td>
        <td align="center">{item.hoja_medico_acargo}</td>
        <td>{item.hoja_motivoconsulta}</td>
        <td>{item.hoja_enfermedadactual}</td>
        <td>{item.hoja_diagnostico}</td>
        <td>{item.hoja_subjetivos}</td>
        <td>{item.hoja_objetivos}</td>
        <td>{item.hoja_analisis}</td>
        <td>{item.hoja_plan}</td>
      </tr>
    ))

    return (
      <Row>
        <Col xs="12" lg="12">
          <Table hover responsive size="sm">
            <thead>
              <tr>
                <td align="center"><strong>Nro.</strong></td>
                <td align="center"><strong>Fecha_Consulta</strong></td>
                <td align="center"><strong>Medico</strong></td>
                <td align="left"><strong>Motivo_de_Consulta</strong></td>
                <td align="left"><strong>Enfermedad Actual</strong></td>
                <td align="left"><strong>Diagnostico</strong></td>
                <td align="left"><strong>Subjetivo</strong></td>
                <td align="left"><strong>Objetivo</strong></td>
                <td align="left"><strong>Analisis</strong></td>
                <td align="left"><strong>Plan</strong></td>
              </tr>
            </thead>
            <tbody>
              {hojae}
            </tbody>
          </Table>
        </Col>
      </Row>
    );
  }
}

export default DatosConsultaExterna;
