import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle,
  Button,
  Label,
  FormText, FormGroup,
  Input,
  Table,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Alert,
  Collapse,
} from 'reactstrap';
import DatosConsultaExterna from './DatosConsultaExterna'
import Search from 'react-search-box';


class ConsultaExterna extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hojaevo: {
        hoja_diagnostico: '',
        hoja_enfermedadactual: '',
        hoja_fecha: this.props.hojaevo.hoja_fecha,
        hoja_idcompromiso: this.props.hojaevo.hoja_idcompromiso,
        hoja_medico_acargo: this.props.hojaevo.hoja_medico_acargo,
        hoja_analisis: '',
        hoja_motivoconsulta: '',
        hoja_plan: '',
        hoja_objetivos:'',
        hoja_subjetivos: '',
      },
      //para el modal
      modal1: false,
      modal2: false,
      activeTab: '1',
      collapse: false,
      cie10: [],
      consultadet: [],
      c1: false,
      c2: false,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.collapseAbrirCerrar = this.collapseAbrirCerrar.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);

    this.cambiarModal1 = this.cambiarModal1.bind(this);
    this.cambiarModal2 = this.cambiarModal2.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
  }

  cambiarTabla(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  collapseAbrirCerrar() {
    this.setState({ collapse: !this.state.collapse });
  }

  cambiarModal1() {
    this.setState({
      modal1: !this.state.modal1,
    });
    let inf1 = this.state.hojaevo;
    let inf2 = this.state.hojaevo;
    let inf3 = this.state.hojaevo;
    let inf4 = this.state.hojaevo;
    let inf5 = this.state.hojaevo;
    let inf6 = this.state.hojaevo;
    let inf7 = this.state.hojaevo;
    let inf8 = this.state.hojaevo;
    let inf9 = this.state.hojaevo;
    let inf10 = this.state.hojaevo;
    inf1['hoja_diagnostico'] = '';
    inf2['hoja_enfermedadactual'] = '';
    inf3['hoja_analisis'] = '';
    inf4['hoja_motivoconsulta'] = '';
    inf5['hoja_plan'] = '';
    inf6['hoja_subjetivos'] = '';
    inf7['hoja_fecha'] = this.props.hojaevo.hoja_fecha;
    inf8['hoja_idcompromiso'] = this.props.hojaevo.hoja_idcompromiso;
    inf9['hoja_medico_acargo'] = this.props.hojaevo.hoja_medico_acargo;
    inf10['hoja_objetivos'] = '';
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8, inf9,inf10 });
    /*  window.location.reload() */
  }
  cambiarModal2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleChange(event) {
    let dato = this.state.hojaevo;
    dato[event.target.name] = event.target.value;
    this.setState({ dato });
  }

  handleOnControll() {
    const { hoja_motivoconsulta, hoja_diagnostico } = this.state.hojaevo

    hoja_motivoconsulta === ""
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    hoja_diagnostico === ""
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (hoja_motivoconsulta !== "" && hoja_diagnostico !== "") {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  async handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2,
      modal1: !this.state.modal1,
    })
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    try {
      fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listahojaevolucion/', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.hojaevo), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscahojaevolucion/' + this.state.hojaevo.hoja_idcompromiso + '/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          })
            .then((response) => { return response.json() })
            .then((consultadet) => { this.setState({ consultadet: consultadet }) })
        );
    } catch (e) {
      console.log(e);
    }
  }

  handleChange2(value) {
    let diagnostico = this.state.hojaevo;
    diagnostico["hoja_diagnostico"] = value;
    this.setState({ diagnostico });
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listacie10/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ cie10: json1 });
    } catch (e) {
      console.log(e);
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscahojaevolucion/' + this.state.hojaevo.hoja_idcompromiso + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
        }
      })
      const json2 = await respuesta2.json();
      this.setState({ consultadet: json2 });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card className="card-accent-primary">
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="12">
                    <CardTitle><strong><i className="fa fa-stethoscope fa-lg mt-1"></i> Consulta Externa</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input"><strong>Consulta*</strong></Label>
                  </Col>
                  <Col xs="12" md="10">
                    <Input type="text" id="hoja_motivoconsulta" name="hoja_motivoconsulta" value={this.state.hojaevo.hoja_motivoconsulta} onChange={this.handleChange} required valid placeholder="" />
                    {this.state.c1 && <FormText color="danger">Describa motivo de consulta del paciente</FormText>}
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"><strong>Enfermedad Actual</strong></Label>
                  </Col>
                  <Col xs="12" md="10">
                    <Input type="text" id="hoja_enfermedadactual" name="hoja_enfermedadactual" value={this.state.hojaevo.hoja_enfermedadactual} onChange={this.handleChange} placeholder="" />
                    <FormText color="darck">Describa enfermedad(s)</FormText>
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"><strong>CIE10</strong></Label>
                  </Col>
                  <Col xs="12" md="10">
                    <Search
                      data={this.state.cie10}
                      onChange={this.handleChange2}
                      placeholder="Seleccione Diagnostico cie10"
                      class="search-class"
                      searchKey="cie_descripcion"
                    />
                    <FormText color="darck">Seleccione diagnostico cie10</FormText>
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input"><strong>Diagnostico Presuntivo*</strong></Label>
                  </Col>
                  <Col xs="12" md="10">
                    <Input type="text" id="hoja_diagnostico" name="hoja_diagnostico" value={this.state.hojaevo.hoja_diagnostico} onChange={this.handleChange} required valid placeholder="" />
                    {this.state.c2 && <FormText color="danger">Describa Diagnostico presuntivo</FormText>}
                  </Col>
                </FormGroup>

                <Collapse isOpen={this.state.collapse} onEntering={this.onEntering} onEntered={this.onEntered} onExiting={this.onExiting} onExited={this.onExited}>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="textarea-input"><strong>Subjetivo</strong></Label>
                    </Col>
                    <Col xs="12" md="10">
                      <Input type="textarea" name="hoja_subjetivos" id="hoja_subjetivos" rows="3" value={this.state.hojaevo.hoja_subjetivos} onChange={this.handleChange} placeholder="Información subjetiva obtenida desde el sujeto, familiar ó persona que acompañe al paciente..." />
                      <FormText color="darck">Registra los síntomas del paciente</FormText>
                    </Col>
                    <Col md="2">
                      <Label htmlFor="textarea-input"><strong>Objetivo</strong></Label>
                    </Col>
                    <Col xs="12" md="10">
                      <Input type="textarea" name="hoja_objetivos" id="hoja_objetivos" rows="3" value={this.state.hojaevo.hoja_objetivos} onChange={this.handleChange} placeholder="Información objetiva ó que se constata..." />
                      <FormText color="darck">Registra datos como los signos vitales, resultados de la exploracion fisica, de las pruebas diagnosticas entre otros.</FormText>
                    </Col>
                    <Col md="2">
                      <Label htmlFor="textarea-input"><strong>Analisis</strong></Label>
                    </Col>
                    <Col xs="12" md="10">
                      <Input type="textarea" name="hoja_analisis" id="hoja_analisis" rows="3" value={this.state.hojaevo.hoja_analisis} onChange={this.handleChange} placeholder="Evaluacion del paciente mediante el analisis de la informacion obtenida..." />
                      <FormText color="darck">Registra lista de problemas del paciente.</FormText>
                    </Col>
                    <Col md="2">
                      <Label htmlFor="textarea-input"><strong>Plan</strong></Label>
                    </Col>
                    <Col xs="12" md="10">
                      <Input type="textarea" name="hoja_plan" id="hoja_plan" rows="3" value={this.state.hojaevo.hoja_plan} onChange={this.handleChange} placeholder="Se debe establecer un plan de tratamiento a seguir en el paciente" />
                      <FormText color="darck">Prescripcion de medicamentos, procedimientos, cirugia.</FormText>
                    </Col>
                  </FormGroup>
                </Collapse>

                <FormGroup row>
                  <Col md="3"></Col>
                  <Col xs="12" md="9">
                    <Button color="link" size="lg" onClick={this.collapseAbrirCerrar} style={{ marginBottom: '1rem' }}><h5><strong>Hoja de evolución utilizando el sistema SOAP</strong></h5></Button>
                    {/* ctrl + shift + i   da formato al documento (ordena el codigo) */}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs="12" md="12">
                    <Button type="" size="lg" active block color="primary" onClick={this.handleOnControll}><i className="fa fa-floppy-o"></i> <strong>Registrar Consulta</strong></Button>
                    {/* ctrl + shift + i   da formato al documento (ordena el codigo) */}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs="12" md="12">
                    {<DatosConsultaExterna consulta={this.state.consultadet} />}
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>

            <Modal isOpen={this.state.modal1} className={'modal-sm modal-success ' + this.props.className}>
              <ModalHeader>Mensaje</ModalHeader>
              <ModalBody>
                Consulta Externa Registrada
              </ModalBody>
              <ModalFooter>
                <Button color="success" onClick={this.cambiarModal1}>Cerrar</Button>
              </ModalFooter>
            </Modal>

            <Modal isOpen={this.state.modal2} className={'modal-lg modal-info ' + this.props.className}>
              <ModalHeader>Desea registrar los siguientes datos ?</ModalHeader>
              <ModalBody>
                <Table hover responsive size="sm">
                  <thead>
                    <tr>
                      <td align="center"><strong>Nro.</strong></td>
                      <td align="center"><strong>Fecha Atencion</strong></td>
                      <td align="center"><strong>Motivo de Consulta</strong></td>
                      <td align="center"><strong>Enfermedad Actual</strong></td>
                      <td align="center"><strong>Diagnostico</strong></td>
                      <td align="center"><strong>Subjetivo</strong></td>
                      <td align="center"><strong>Objetivo</strong></td>
                      <td align="center"><strong>Analisis</strong></td>
                      <td align="center"><strong>Plan</strong></td>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td align="center">1</td>
                      <td>{this.state.hojaevo.hoja_fecha}</td>
                      <td align="center">{this.state.hojaevo.hoja_motivoconsulta}</td>
                      <td>{this.state.hojaevo.hoja_enfermedadactual}</td>
                      <td>{this.state.hojaevo.hoja_diagnostico}</td>
                      <td>{this.state.hojaevo.hoja_subjetivos}</td>
                      <td>{this.state.hojaevo.hoja_objetivos}</td>
                      <td>{this.state.hojaevo.hoja_analisis}</td>
                      <td>{this.state.hojaevo.hoja_plan}</td>
                    </tr>
                  </tbody>
                </Table>
              </ModalBody>
              <ModalFooter>
                <Button color="success" onClick={this.handleOnSubmit}>Aceptar</Button>
                <Button color="primary" onClick={this.cambiarModal2}>Cancelar</Button>
              </ModalFooter>
            </Modal>

          </Col>
        </Row>
      </div>
    );
  }
}

export default ConsultaExterna;
