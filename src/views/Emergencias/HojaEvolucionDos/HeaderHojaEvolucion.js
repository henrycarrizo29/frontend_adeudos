import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle, CardSubtitle,
  FormGroup,
  Alert,
  Nav, NavItem, NavLink,
  TabPane, TabContent,
  Button,
} from 'reactstrap';


const styles = { color: '#0288d1' }
const styles1 = { color: '#2e7d32' }
const styles2 = { color: '#ef6c00' }

class HeaderHojaEvolucion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      detalleEstudiante: {},
      medicoConsulta: [],
      hojaEvolucion: {},
    }
  }

  componentDidMount() {
    fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/detallehojaevolucion/' + this.props.hojaevoid + '/')
      .then((response) => { return response.json() })
      .then((hojaEvolucion) => { this.setState({ hojaEvolucion: hojaEvolucion }) })
    fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.hojaevoestudiante + '/')
      .then((response) => { return response.json() })
      .then((detalleEstudiante) => { this.setState({ detalleEstudiante: detalleEstudiante }) })
    fetch('http://0.0.0.0:8000/adeudos/personal/v1/detallepersonal/' + this.props.hojaevomedico + '/')
      .then((response) => { return response.json() })
      .then((medicoConsulta) => { this.setState({ medicoConsulta: medicoConsulta }) })
  }

  render() {
    return (
      <div>
        <FormGroup row>
          <Col md="6">
            <i className="fa fa-user-md fa-lg mt-1" style={styles2}></i><strong> Estudiante:</strong>{' ' + this.state.detalleEstudiante.est_nombre + ' ' + this.state.detalleEstudiante.est_apellido1 + ' ' + this.state.detalleEstudiante.est_apellido2}
          </Col>
          <Col md="6">
            <i className="fa fa-user-md fa-lg mt-1" style={styles2}></i><strong> Medico:</strong>{' ' + this.state.medicoConsulta[0].per_nombre + ' ' + this.state.medicoConsulta[0].per_apellido1 + ' ' + this.state.medicoConsulta[0].per_apellido2}
          </Col>
          <Col md="4">
            <i className="fa fa-calendar fa-lg mt-1" style={styles1}></i><strong> Fecha:</strong>{' ' + this.props.hojaevofecha}
          </Col>
        </FormGroup>
      </div>
    );
  }
}

export default HeaderHojaEvolucion;