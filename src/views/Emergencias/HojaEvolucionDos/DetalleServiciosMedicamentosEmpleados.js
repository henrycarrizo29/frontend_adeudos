import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
} from 'reactstrap';

const styles = { color: '#191970' }

class DetalleServiciosMedicamentosEmpleados extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
    this.buscaNombre = this.buscaNombre.bind(this)
  }

  buscaNombre(idSME) {
    let itemDSME = this.props.listaprecio.filter(lp => lp.id === idSME);
    return itemDSME[0].lista_descripcion
  }


  render() {
    
    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive borderless hover size="sm">
              <thead>
                <tr>
                  <th style={{ maxWidth: '5px', textAlign: 'center' }}>Nro</th>
                  <th style={{ maxWidth: '5px', textAlign: 'left' }}>Nombre</th>
                  <th style={{ maxWidth: '5px', textAlign: 'center' }}>Cantidad</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.props.prestaciones.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td style={{ maxWidth: '5px', textAlign: 'center' }}>{i + 1}</td>
                      <td style={{ maxWidth: '15px' }} >{this.buscaNombre(u.sem_serviciosprestados)} </td>
                      <td style={{ maxWidth: '5px', textAlign: 'center' }}>{u.sem_cantidad}</td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleServiciosMedicamentosEmpleados;

