import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle, CardSubtitle,
  FormGroup,
  Alert,
  Nav, NavItem, NavLink,
  TabPane, TabContent,
} from 'reactstrap';

import classnames from 'classnames';
import ServiciosMedicamentosEmpleados from './ServiciosMedicamentosEmpleados';
import ConsultaExterna from './ConsultaExterna'

const styles = { color: '#0288d1' }
const styles1 = { color: '#2e7d32' }
const styles2 = { color: '#ef6c00' }

class HojaEvolucionDos extends Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      /* date = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear(); */
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

    this.state = {
      hoja_evo: this.props.hojaevo,
      usuario: this.props.usuario,
      compromisosEst: this.props.compromisosEst,
      estudiante: {},
      fecha: date,

      medicoConsulta: [],
      hojaEvolucion: {},

      activeTab: '1',
    }
  }

  cambiarTabla(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.state.compromisosEst.com_estudiante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({
        estudiante: json1,
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="12">
                    <CardTitle><strong><i className="icon-note icons font-3xl"></i> Hoja de Evolucion Hospitalaria</strong></CardTitle>
                    <CardSubtitle>FORMULARIO H.C. N°3  -  Hoja A.</CardSubtitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <Alert color="light">
                  <FormGroup row>
                    <Col md="5">
                      <i className="fa fa-user fa-lg mt-1" style={styles}></i><strong> Estudiante:</strong>{' ' + this.state.estudiante.nombres + ' ' + this.state.estudiante.primer_apellido + ' ' + this.state.estudiante.segundo_apellido}
                    </Col>
                    <Col md="5">
                      <i className="fa fa-user-md fa-lg mt-1" style={styles2}></i><strong> Medico:</strong>{' ' + this.state.usuario.nombres + ' ' + this.state.usuario.primer_apellido + ' ' + this.state.usuario.segundo_apellido}
                    </Col>
                    <Col md="2">
                      <i className="fa fa-calendar fa-lg mt-1" style={styles1}></i><strong> Fecha:</strong>{' ' + this.state.fecha}
                    </Col>
                  </FormGroup>
                  {/* <HeaderHojaEvolucion hojaevoid={this.props.hojae.hoja_id} hojaevoestudiante={this.props.hojae.hoja_estudiante} hojaevomedico={this.props.hojae.hoja_medico_acargo} hojaevofecha={this.props.hojae.hoja_fecha} />  */}
                </Alert>
                <Row>
                  <Col xs="12" md="12" className="mb-4">
                    <Nav tabs>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.cambiarTabla('1'); }}>
                          <code style={styles}><h5><i className="fa fa-stethoscope fa-lg mt-1"></i><span className={this.state.activeTab === '1' ? '' : 'd-none'}> <strong>Consulta Externa</strong></span></h5></code>
                        </NavLink>
                      </NavItem>
                      {/* <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.cambiarTabla('2'); }} >
                          <code style={styles1}><h4><i className="fa fa-list-ol fa-lg mt-1"></i><i className="fa fa-odnoklassniki-square fa-lg mt-1"></i><span className={this.state.activeTab === '2' ? '' : 'd-none'}> <strong>Examnes Requeridos</strong></span></h4></code>
                        </NavLink>
                      </NavItem> */}
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.cambiarTabla('3'); }} >
                          <code style={styles2}><h5><i className="fa fa-medkit fa-lg mt-1"></i><span className={this.state.activeTab === '3' ? '' : 'd-none'}> <strong>Servicios y Medicamentos Empleados</strong></span></h5></code>
                        </NavLink>
                      </NavItem>
                    </Nav>

                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="1">
                        <ConsultaExterna hojaevo={this.props.hojaevo} />
                      </TabPane>
                      {/* <TabPane tabId="2">
                        <ExamenesRequeridos hojaevo={this.props.hojaevo} />
                      </TabPane> */}
                      <TabPane tabId="3">
                        <ServiciosMedicamentosEmpleados hojaevo={this.props.hojaevo} />
                      </TabPane>
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default HojaEvolucionDos;