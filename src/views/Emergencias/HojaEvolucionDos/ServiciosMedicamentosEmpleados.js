import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col, Row,
  Button,
  Label,
  FormText, FormGroup,
  Input,
  Table,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
import Search from 'react-search-box';
import DetalleServiciosMedicamentosEmpleados from './DetalleServiciosMedicamentosEmpleados'

const styles = { color: '#191970' }

class ServiciosMedicamentosEmpleados extends Component {
  constructor(props) {
    super(props);

    this.state = {

      todoloempleado: {
        sem_cantidad: 0,
        sem_idcompromiso: this.props.hojaevo.hoja_idcompromiso,
        sem_serviciosprestados: 0,
      },

      serviobtenidos: [],
      serviciosPrestados: [],
      prestaciones: [],
      //para el modal
      modal3: false,
      modal5: false,
      show1: false,
      c1: false,
      c2: false,
      msj: '',
    }
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleOnSubmit2 = this.handleOnSubmit2.bind(this);
    this.handleOnAddServiciosMedicamentos = this.handleOnAddServiciosMedicamentos.bind(this);

    this.cambiarModal3 = this.cambiarModal3.bind(this);
    this.confirmacionGuardar = this.confirmacionGuardar.bind(this);
    this.modalsw5 = this.modalsw5.bind(this);
  }

  cambiarModal3() {
    this.setState({
      modal3: !this.state.modal3,
      show1: true,
    });
    //window.location.reload()
    //window.location='#/compromisodepago/vercompromiso';
  }

  modalsw5() {
    this.setState({
      modal5: !this.state.modal5,
      show1: true,
    });
    /* window.location.reload()
    window.location = '#/compromisodepago/vercompromiso'; */
  }

  confirmacionGuardar(e) {
    this.setState({
      modal3: !this.state.modal3,
      show1: false,
    })
  }

  handleChange2(value) {
    let itemDSP = this.state.serviciosPrestados.filter(servicioprestado => servicioprestado.lista_descripcion === value);
    let dato = this.state.todoloempleado;
    dato["sem_serviciosprestados"] = itemDSP[0].id;
    this.setState({ dato });
  }

  handleChange3(event) {
    let dato = this.state.todoloempleado;
    dato[event.target.name] = event.target.value;
    this.setState({ dato });
  }

  handleOnAddServiciosMedicamentos(event) {
    /*preguarado*/
    const { sem_cantidad, sem_serviciosprestados } = this.state.todoloempleado

    sem_cantidad === 0
      ? (
        this.setState({
          c2: true,
          msj: 'Ingrese cantidad'
        })
      )
      : (
        sem_cantidad < 0
          ? (
            this.setState({
              c2: true,
              msj: 'Ingrese una cantidad positiva',
            })
          )
          : (
            this.setState({ c2: false })
          )
      )

    sem_serviciosprestados === 0
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    if (sem_cantidad !== 0 && sem_serviciosprestados !== 0) {
      event.preventDefault();
      let sme = {
        sem_cantidad: this.state.todoloempleado.sem_cantidad,
        sem_serviciosprestados: this.state.todoloempleado.sem_serviciosprestados,
        sem_idcompromiso: this.state.todoloempleado.sem_idcompromiso,
      };
      this.setState({
        serviobtenidos: this.state.serviobtenidos.concat([sme]),
      });
      let td1 = this.state.todoloempleado;
      td1['sem_serviciosprestados'] = 0
      let td2 = this.state.todoloempleado;
      td2['sem_cantidad'] = 0
      this.setState({ td1,td2 });
    }
  }

  buscaNombre(idDSP) {
    let itemDSP = this.state.serviciosPrestados.filter(servicioprestado => servicioprestado.id === idDSP);
    return itemDSP[0].lista_descripcion
  }

  Eliminar(i) {
    const nuevo = this.state.serviobtenidos.filter(serviobtenidos => {
      return serviobtenidos !== i
    })
    if (this.state.serviobtenidos.length != 1) {
      this.setState({
        serviobtenidos: [...nuevo]
      })
    }
    else {
      this.setState({
        serviobtenidos: [...nuevo],
      })
    }
  }


  handleOnSubmit2(e) {
    this.setState({
      modal3: !this.state.modal3,
    })
    this.state.serviobtenidos.map((data, i) => {
      let data_serviobtenidos = {
        sem_idcompromiso: data.sem_idcompromiso,
        sem_serviciosprestados: data.sem_serviciosprestados,
        sem_cantidad: data.sem_cantidad
      }
      e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
      try {
        fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listaserviciosexamenesmedicamentos/', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(data_serviobtenidos), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscaserviciosmedicamentosempleados/' + this.props.hojaevo.hoja_idcompromiso + '/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`,
              }
            })
              .then((response) => { return response.json() })
              .then((prestaciones) => { this.setState({ prestaciones: prestaciones }) })
          );
      } catch (e) {
        console.log(e);
      }
    })
    this.setState({
      modal5: !this.state.modal5,
      serviobtenidos: [],
    })
  }


  async componentDidMount() {
    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listaprecio/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      this.setState({
        serviciosPrestados: json5,
      });

      try {
        const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscaserviciosmedicamentosempleados/' + this.props.hojaevo.hoja_idcompromiso + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json6 = await respuesta6.json();
        this.setState({
          prestaciones: json6,
        });

      } catch (e) {
        console.log(e);
      }

    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card className="card-accent-warning">
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="12">
                    <CardTitle><strong><i className="fa fa-medkit fa-lg mt-1"></i> Servicios y Medicamentos Empleados</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col xs="12" md="6">
                    <Label htmlFor="text-input">Prestaciones *</Label>
                    <Search
                      data={this.state.serviciosPrestados}
                      onChange={this.handleChange2}
                      placeholder="Ingrese dato a buscar"
                      class="search-class"
                      searchKey="lista_descripcion"
                    />
                    {this.state.c1 && <FormText color="danger">Seleccione un item</FormText>}
                  </Col>
                  <Col md="2">
                    <Label htmlFor="text-input">Cantidad *</Label>
                    <Input type="number" name="sem_cantidad" id="sem_cantidad" valid value={this.state.todoloempleado.sem_cantidad} onChange={this.handleChange3}></Input>
                    {this.state.c2 && <FormText color="danger">{this.state.msj}</FormText>}
                  </Col>
                  <Col xs="12" md="3">
                    <br />
                    <Button type="" size="" color="success" onClick={this.handleOnAddServiciosMedicamentos}><i className="fa fa-plus"></i> </Button>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col xs="12" md="12">
                    <Table responsive borderless hover size="sm">
                      <thead>
                        <tr>
                          <th style={{ maxWidth: '5px', textAlign: 'center' }}>Nro</th>
                          <th style={{ maxWidth: '5px', textAlign: 'left' }}>Nombre</th>
                          <th style={{ maxWidth: '5px', textAlign: 'center' }}>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.serviobtenidos.map((u, i) => {
                          return (
                            <tr key={i}>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>{i + 1}</td>
                              <td style={{ maxWidth: '15px' }} >{this.buscaNombre(u.sem_serviciosprestados)} </td>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>{u.sem_cantidad}</td>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>
                                <Button onClick={(e) => this.Eliminar(u)} color="danger" type="button"><i className="fa fa-remove"></i></Button>
                              </td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </Table>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs="12" md="12">
                    <Button type="submit" size="lg" active block color="success" onClick={this.confirmacionGuardar}><i className="fa fa-floppy-o"></i> <strong>Registrar</strong></Button>
                    {/* ctrl + shift + i   da formato al documento (ordena el codigo) */}
                  </Col>
                </FormGroup>

                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h3><strong><code style={styles}>Servicios y Medicamentos Empleados</code></strong></h3>
                    <DetalleServiciosMedicamentosEmpleados listaprecio={this.state.serviciosPrestados} prestaciones={this.state.prestaciones} />
                  </Col>
                </FormGroup>

                <Modal isOpen={this.state.modal3} className={'modal-lg ' + 'modal-success ' + this.props.className}>
                  <ModalHeader><strong>Desea registrar los siguientes datos ?</strong></ModalHeader>
                  <ModalBody>
                    <Table responsive borderless hover size="sm">
                      <thead>
                        <tr>
                          <th style={{ maxWidth: '5px', textAlign: 'center' }}>Nro</th>
                          <th style={{ maxWidth: '5px', textAlign: 'left' }}>Nombre</th>
                          <th style={{ maxWidth: '5px', textAlign: 'center' }}>Cantidad</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.serviobtenidos.map((u, i) => {
                          return (
                            <tr key={i}>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>{i + 1}</td>
                              <td style={{ maxWidth: '15px' }} >{this.buscaNombre(u.sem_serviciosprestados)} </td>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>{u.sem_cantidad}</td>
                              <td style={{ maxWidth: '5px', textAlign: 'center' }}>
                                <Button onClick={(e) => this.Eliminar(u)} color="danger" type="button">Eliminar</Button>
                              </td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </Table>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={this.handleOnSubmit2}>Aceptar</Button>
                    <Button color="primary" onClick={this.cambiarModal3}>Cancelar</Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal5} className={'modal-sm ' + 'modal-success ' + this.props.className}>
                  <ModalHeader><strong>Mensaje</strong></ModalHeader>
                  <ModalBody>
                    Datos registrados con exito
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" onClick={this.modalsw5}>Cerrar</Button>
                  </ModalFooter>
                </Modal>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ServiciosMedicamentosEmpleados;