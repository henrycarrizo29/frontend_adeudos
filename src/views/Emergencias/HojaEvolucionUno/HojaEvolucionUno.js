import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle,
  Button,
  FormText, FormGroup,
} from 'reactstrap';
import Search from 'react-search-box';
import CardEstudiante from '../../Compromisos/Compromiso/CardEstudiante';
import HojaEvolucionDos from '../HojaEvolucionDos/HojaEvolucionDos'
import TablaEstudiante from '../../Compromisos/Compromiso/TablaEstudiante';
import DatosConsultaExterna from '../HojaEvolucionDos/DatosConsultaExterna'

const styles = { color: '#191970' }
const styles0 = { color: '#0288d1' }
const styles1 = { color: '#2e7d32' }
const styles2 = { color: '#ef6c00' }

class HojaEvolucionUno extends Component {
  constructor(props) {
    super(props);
    //calculamos la fecha actual
    var hoy = new Date(),
      /* date = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear(); */
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

    this.state = {
      hoja_evo: {
        hoja_fecha: date,
        hoja_subjetivos: '',
        hoja_analisis: '',
        hoja_plan: '',
        hoja_motivoconsulta: '',
        hoja_enfermedadactual: '',
        hoja_diagnostico: '',
        hoja_idcompromiso: null,
        hoja_medico_acargo: '',
      },
      compromisosEst: {},
      fecha: date,
      listaEstudianteCDP: [],

      username: {},
      usuario: {},
      sw1: false,
      sw2: false,
      logged_in: localStorage.getItem('token') ? true : false,
    }

    this.handleChange1 = this.handleChange1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    window.location.reload();
  }

  //capturando los valores que escribe el usuario, con la funcion hadleChage

  /* handleChange(event) {
    let hevo = this.state.hoja_evo;
    hevo[event.target.name] = event.target.value;
    this.setState({ hevo });
    console.log(hevo);
  } */


  // obteniendo la lista de todos los medicos, estudiantes y garantes
  async componentDidMount() {
    try {
      if (this.state.logged_in) {
        const response1 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json1 = await response1.json();
        this.setState({ username: json1 });

        try {
          const response2 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/detalleusuario/' + this.state.username.id + '/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
          const json2 = await response2.json();
          this.setState({ usuario: json2[0] });
        } catch (error) {
          console.log(error);
        }

        try {
          const response3 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
          const json3 = await response3.json();
          this.setState({ listaEstudianteCDP: json3 });
        } catch (error) {
          console.log(error);
        }

      }
    } catch (error) {
      console.log(error);
    }
  }

  //capturando el valor del estudiante en el buscador
  handleChange1(value) {

    let compromiso = this.state.listaEstudianteCDP.filter(idcdp => idcdp.com_estudiante === value);
    let dato1 = this.state.hoja_evo;
    dato1['hoja_idcompromiso'] = compromiso[0].id;
    let dato2 = this.state.hoja_evo;
    dato2['hoja_medico_acargo'] = this.state.usuario.ci;
    this.setState({
      compromisosEst: compromiso[0],
      dato1,
      dato2,
      sw1: true,
    });
  }


  // registrando el compromiso de pago
  handleOnSubmit(e) {
    if (this.state.hoja_evo.hoja_estudiante !== '' && this.state.hoja_evo.hoja_medico_acargo !== '' && this.state.hoja_evo.hoja_fecha !== '' && this.state.hoja_evo.hoja_idcompromiso !== '') {
      e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
      try {
        fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listahojaevolucion/', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.hoja_evo), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success...:', response))

      } catch (e) {
        console.log(e);
      }
      this.setState({
        modal2: !this.state.modal2,
      });
    } else {
      this.setState({
        modal1: !this.state.modal1,
      });
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" md="6">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="11">
                    <CardTitle><strong><i className="icon-note icons font-3xl"></i> Ingresar Consulta Externa</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="8">
                    <i className="fa fa-user-md fa-lg mt-1" style={styles2}></i><strong> Medico:</strong>{' ' + this.state.usuario.nombres + ' ' + this.state.usuario.primer_apellido + ' ' + this.state.usuario.segundo_apellido}
                  </Col>
                  <Col md="4">
                    <i className="fa fa-calendar fa-lg mt-1" style={styles1}></i><strong> Fecha:</strong>{' ' + this.state.fecha}
                  </Col>
                </FormGroup>
                <hr />
                <FormGroup row>
                  {/* <Col xs="auto">
                    <h4><strong> <code style={styles}>Nueva Consulta </code></strong></h4>
                  </Col> */}
                  <Col xs="12" md="6">
                    <Search
                      data={this.state.listaEstudianteCDP}
                      onChange={this.handleChange1}
                      placeholder="Ingresar c.i. del estudiante"
                      class="search-class"
                      searchKey="com_estudiante"
                    />
                    <FormText color="dark">Ingresa c.i. y selecciona </FormText>
                  </Col>
                  <Col xs="12" md="4">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                </FormGroup>
                <hr />
                {/* this.state.sw1 && <Button type="submit" size="" active block color="primary" onClick={this.handleOnSubmit}><i className="fa fa-dot-circle-o"></i> <strong>Ingresar Consulta Externa</strong></Button> */}
              </CardBody>
            </Card>
          </Col>
          <Col xs="12" md="6">
            {this.state.sw1 && <CardEstudiante estudianteid={this.state.compromisosEst.com_estudiante} />}
          </Col>
          <Col xs="12" md="12">
            {this.state.sw1 && <HojaEvolucionDos hojaevo={this.state.hoja_evo} usuario={this.state.usuario} compromisosEst={this.state.compromisosEst} />}
          </Col>
        </Row>
      </div>
    );
  }
}

export default HojaEvolucionUno;