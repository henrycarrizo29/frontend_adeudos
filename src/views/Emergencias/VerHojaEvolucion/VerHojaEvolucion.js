import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle,
  FormText,
  Button,
} from 'reactstrap';
import VerDetalleHojaEvolucion from './DetalleHojaEvolucion/VerDetalleHojaEvolucion'
import Search from 'react-search-box'
import CardEstudiante from '../../Compromisos/Compromiso/CardEstudiante';

class VerHojaEvolucion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cdpestudiante: [],
      listaCompromisoDePago: [],
      show: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    window.location.reload();
  }

  async componentDidMount() {
    try {
      const response3 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json3 = await response3.json();
      this.setState({ listaCompromisoDePago: json3 });
    } catch (error) {
      console.log(error);
    }
  }

  async handleChange(value) {
    try {
      const response4 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromisobusca/' + value + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json4 = await response4.json();
      this.setState({ cdpestudiante: json4 });
      this.setState({ show: true })
    } catch (error) {
      console.log(error);
    }
  }

  /*  handleChange1 = value => {
     this.setState({ estudiante: value });
     this.setState({ show: true })
   }; */

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" md="6">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><i className="fa fa-search fa-lg mt-1"></i><strong>Ver Hoja Evolucion</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col md="1"></Col>
                  <Col xs="12" md="5" className="text-right mt-1">
                    <Search
                      data={this.state.listaCompromisoDePago}
                      onChange={this.handleChange.bind(this)}
                      placeholder="c.i..."
                      class="search-class"
                      searchKey="com_estudiante"
                    />
                    <FormText color="dark">Ingresa c.i. y selecciona </FormText>
                  </Col>
                  <Col xs="12" md="5" className="text-left mt-1">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                  <Col xs="12" md="1"></Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
          <hr />
          <Col xs="12" md="6">
            {this.state.show && <CardEstudiante estudianteid={this.state.cdpestudiante[0].com_estudiante} />}
          </Col>
        </Row>
        {this.state.show && <VerDetalleHojaEvolucion cdpest={this.state.cdpestudiante} />}
      </div>
    );
  }
}

export default VerHojaEvolucion;
