import React, { Component } from 'react';
import {
  Col,
  FormGroup,
} from 'reactstrap';
import DetalleServiciosMedicamentosEmpleados from '../../HojaEvolucionDos/DetalleServiciosMedicamentosEmpleados';

class VerServiciosMedicamentos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      prestaciones: [],
      serviciosPrestados: [],
    }
  }

  async componentDidMount() {
    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listaprecio/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      this.setState({
        serviciosPrestados: json5,
      });

      try {
        const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscaserviciosmedicamentosempleados/' + this.props.idCDP + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json6 = await respuesta6.json();
        this.setState({
          prestaciones: json6,
        });

      } catch (e) {
        console.log(e);
      }

    } catch (e) {
      console.log(e);
    }
  }



  render() {
    return (
      <FormGroup row>
        <Col xs="12" md="12">
          <DetalleServiciosMedicamentosEmpleados listaprecio={this.state.serviciosPrestados} prestaciones={this.state.prestaciones} />
        </Col>
      </FormGroup>
    );
  }
}
export default VerServiciosMedicamentos;