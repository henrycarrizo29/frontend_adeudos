import React, { Component } from 'react';
import {
  Col, Row,
  Card, CardBody, CardHeader, CardTitle, CardSubtitle,
  FormGroup,
  Alert,
  Button,
  Collapse
} from 'reactstrap';
import VerConsultaExterna from './VerConsultaExterna';
import VerServiciosMedicamentos from './VerServiciosMedicamentos';

const styles1 = { color: '#2e7d32' }

class VerDetalleHojaEvolucion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      custom: [true, false, false],
    };

    this.toggleCustom = this.toggleCustom.bind(this);
  }

  toggleCustom(tab) {
    const prevState = this.state.custom;
    const state = prevState.map((x, index) => tab === index ? !x : false);
    this.setState({
      custom: state,
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="11">
                    <CardTitle><strong><i className="icon-doc icons font-3xl"></i> Hoja Evolucion Paciente</strong></CardTitle>
                    <CardSubtitle></CardSubtitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                {this.props.cdpest.map((item, indice) => (
                  <Card key={indice}>
                    <CardHeader>
                      <FormGroup row>
                        <Col md="8"></Col>
                        <Col md="4">
                          <i className="fa fa-calendar fa-lg mt-1" style={styles1}></i><strong> Fecha de Compromiso:</strong>{' ' + item.com_fecha}
                        </Col>
                      </FormGroup>
                    </CardHeader>
                    <CardBody>
                      <div>
                        <div className="item">
                          <Button className="m-0 p-0" color="link" onClick={() => this.toggleCustom(0)} aria-expanded={this.state.custom[0]}>
                            Consulta Externa
                          </Button>
                          <Collapse isOpen={this.state.custom[0]}>
                            <Alert color="">
                              <VerConsultaExterna idCDP={item.id} />
                            </Alert>
                          </Collapse>
                        </div>
                        <div className="item">
                          <Button className="m-0 p-0" color="link" onClick={() => this.toggleCustom(1)} aria-expanded={this.state.custom[1]} >
                            Servicios y Medicamentos Empleados
                          </Button>
                          <Collapse isOpen={this.state.custom[1]}>
                            <small>
                              <Alert color="warning"><VerServiciosMedicamentos idCDP={item.id} /></Alert>
                            </small>
                          </Collapse>
                        </div>

                        {/* <div className="item">
                          <Button className="m-0 p-0" color="link" onClick={() => this.toggleCustom(2)} aria-expanded={this.state.custom[2]} >
                            Examenes Requeridos
                          </Button>
                          <Collapse isOpen={this.state.custom[2]}>
                            <small>
                              <Alert color="info"><ExamenesLaboratorio idExamen={item.hoja_id} /></Alert>
                            </small>
                          </Collapse>
                        </div> */}
                      </div>
                    </CardBody>
                  </Card>
                ))}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default VerDetalleHojaEvolucion;
