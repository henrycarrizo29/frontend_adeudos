import React, { Component } from 'react';
import {
  Col,
  FormGroup,
} from 'reactstrap';
import DatosConsultaExterna from '../../HojaEvolucionDos/DatosConsultaExterna';

class VerConsultaExterna extends Component {
  constructor(props) {
    super(props);

    this.state = {
      consultadet: [],
    }
  }

  async componentDidMount() {
    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/buscahojaevolucion/' + this.props.idCDP + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
        }
      })
      const json2 = await respuesta2.json();
      this.setState({ consultadet: json2 });
    } catch (e) {
      console.log(e);
    }
  }


  render() {
    return (
      <FormGroup row>
      <Col xs="12" md="12">
        {<DatosConsultaExterna consulta={this.state.consultadet} />}
      </Col>
    </FormGroup>
    );
  }
}
export default VerConsultaExterna;