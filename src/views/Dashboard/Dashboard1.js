import React, { Component, lazy } from 'react';
import {
  Row, Col,
} from 'reactstrap';

const Widget04 = lazy(() => import('./Widget04'));

class Dashboard2 extends Component {

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="6" md="3">
            <a href="#/gastos">
              <Widget04 icon="fa fa-money fa-lg mt-1" color="success" header="GASTOS" value="50" invert>Gestione un nuevo gasto</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/categoria">
              <Widget04 icon="fa fa-cubes fa-lg mt-1" color="success" header="CATEGORIAS" value="50" invert>Categorias para la gestion de gastos</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/certificadosdna">
              <Widget04 icon="fa fa-id-card fa-lg mt-1" color="success" header="CERTIFICADOS" value="50" invert>Generar certificado de no adeudo</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/liquidaciones">
              <Widget04 icon="fa fa-list-alt fa-lg mt-1" color="success" header="LIQUIDACIÓN" value="50" invert>Generar liquidacion de compromiso</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/listadeprecios">
              <Widget04 icon="fa fa-dollar fa-lg mt-1" color="success" header="Costos" value="50" invert>Lista de costos</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/vercompromisocont">
              <Widget04 icon="fa fa-copy fa-lg mt-1" color="success" header="Ver Compromisos" value="50" invert>Lista de Compromisos</Widget04>
            </a>
          </Col>
          {/*  <Col sm="6" md="3">
            <a href="#/listaliquidacion">
              <Widget04 icon="fa fa-list-alt fa-lg mt-1" color="success" header="Ver Liquidaciones" value="50" invert>Ver Liquidaciones</Widget04>
            </a>
          </Col> */}
          <Col sm="6" md="3">
            <a href="#/listacertificados">
              <Widget04 icon="fa fa-newspaper-o fa-lg mt-1" color="success" header="Ver Certificados" value="50" invert>Lista de Certificados</Widget04>
            </a>
          </Col>
          {/* <Col sm="6" md="3">
            <a href="#/hojaevolucion">
              <Widget04 icon="fa fa-edit fa-lg mt-1" color="warning" header="HOJA EVOLUCIÓN" value="50" invert>Registrar consulta del paciente</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/verhojaevolucion">
              <Widget04 icon="fa fa-folder-open fa-lg mt-1" color="warning" header="EXPEDIENTE" value="50" invert>Expediente del paciente</Widget04>
            </a>
          </Col> */}
          <Col sm="6" md="3">
            <a href="#/compromiso">
              <Widget04 icon="fa fa-handshake-o fa-lg mt-1" color="primary" header="COMPROMISOS" value="50" invert>Generar un nuevo compromiso de pago</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/garante">
              <Widget04 icon="fa fa-user-plus fa-lg mt-1" color="primary" header="GARANTE" value="50" invert>Generar un nuevo garante</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/vergarantes">
              <Widget04 icon="fa fa-users fa-lg mt-1" color="primary" header="LISTAR GARANTES" value="50" invert>Lista y busqueda de garantes</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/vercompromiso">
              <Widget04 icon="fa fa-file-text-o fa-lg mt-1" color="primary" header="COMPROMISOS ESTUDIANTE" value="50" invert>Compromisos por categoria</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/buscarcompromiso">
              <Widget04 icon="fa fa-search fa-lg mt-1" color="warning" header="BUSCAR COMPROMISO" value="50" invert>Ver detalle de compromiso</Widget04>
            </a>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard2;
