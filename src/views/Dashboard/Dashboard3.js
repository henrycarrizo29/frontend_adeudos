import React, { Component, lazy } from 'react';
import {
  Row, Col,
} from 'reactstrap';

const Widget04 = lazy(() => import('./Widget04'));

class Dashboard3 extends Component {

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="6" md="3">
            <a href="#/compromiso">
              <Widget04 icon="fa fa-handshake-o fa-lg mt-1" color="primary" header="COMPROMISOS" value="50" invert>Generar un nuevo compromiso de pago</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/garante">
              <Widget04 icon="fa fa-user-plus fa-lg mt-1" color="primary" header="GARANTE" value="50" invert>Generar un nuevo garante</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/vergarantes">
              <Widget04 icon="fa fa-users fa-lg mt-1" color="primary" header="LISTAR GARANTES" value="50" invert>Lista y busqueda de garantes</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/vercompromiso">
              <Widget04 icon="fa fa-file-text-o fa-lg mt-1" color="primary" header="COMPROMISOS ESTUDIANTE" value="50" invert>Compromisos por categoria</Widget04>
            </a>
          </Col>
          <Col sm="6" md="3">
            <a href="#/buscarcompromiso">
              <Widget04 icon="fa fa-search fa-lg mt-1" color="warning" header="BUSCAR COMPROMISO" value="50" invert>Ver detalle de compromiso</Widget04>
            </a>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Dashboard3;
