import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class EditarCertificado extends Component {
  constructor(props) {
    super(props);

    this.state = {
      certificadoDNA: {
        cer_usuario: this.props.certificado.cer_usuario,
        cer_ciestudiante: this.props.certificado.cer_ciestudiante,
        cer_motivo: this.props.certificado.cer_motivo,
        cer_fecha: this.props.certificado.cer_fecha,
        cer_observacion: this.props.certificado.cer_observacion,
      },
      idCertificado: {},
      c1: false,
      c2: false,
      modal1: false,
      modal2: false,
    }
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleOnControll() {
    const { cer_motivo, cer_fecha } = this.state.certificadoDNA

    cer_motivo === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    cer_fecha === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (cer_motivo !== '' && cer_fecha !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/detallecertificados/' + this.props.certificado.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(this.state.certificadoDNA), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }


  handleChange(event) {
    let cerDNA = this.state.certificadoDNA;
    cerDNA[event.target.name] = event.target.value;
    this.setState({ cerDNA });
  }

  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size=''>
          <ModalHeader>
            <i className="fa fa-edit fa-lg mt-1"></i>  Datos del Certificado de NO ADEUDO
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <FormGroup row>
                <Col md="4"> </Col>
                <Col md="5">
                  {/* <strong>CERTIFICADO N° </strong>{(this.state.idCertificado.id) + 1} */}
                  <strong>CERTIFICADO N° </strong>{this.props.certificado.id === undefined ? 1 : (this.props.certificado.id) + 1}
                </Col>
              </FormGroup>
              <hr />
              <Row>
                <Col xs="12" sm="1"></Col>
                <Col xs="12" sm="10 ">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Motivo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="select" name="cer_motivo" id="cer_motivo" required valid value={this.state.certificadoDNA.cer_motivo} onChange={this.handleChange}>
                        <option value=""></option>
                        {this.props.listaMotivo.map((item1, i) => (
                          <option value={item1.id} key={i}>{item1.motivo}</option>
                        ))}
                      </Input>
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-id-card fa-lg mt-1"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese motivo del certificado</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="cer_fecha" name="cer_fecha" required valid value={this.state.certificadoDNA.cer_fecha} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese fecha</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="text-input">Observaciones</Label>
                    <Input type="textarea" rows="2" id="cer_observacion" name="cer_observacion" value={this.state.certificadoDNA.cer_observacion} onChange={this.handleChange} />
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>

          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button color="success" onClick={(event) => { this.props.actualizaCer(); this.modalsw2(); }} >Cerrar</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default EditarCertificado;

