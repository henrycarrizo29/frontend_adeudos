import React, { Component } from 'react';
import {
  Alert,
  Col, Row,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormText,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

import Search from 'react-search-box'
import TablaEstudiante from '../Compromisos/Compromiso/TablaEstudiante';
import CertificadosDNA from './CertificadosDNA';

const stylesIcono = { color: '#4682b4' }

class Certificados extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estudiante: {},
      estudianteCCP: '',
      listaEstudiante: [],
      usuario: {},
      show: false,
      modal1: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
    window.location.reload();
  }

  // obteniendo los compromisos de pago registrado de un estudiante
  async componentDidMount() {
    try {
      const response3 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/listaestudiante', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json3 = await response3.json();
      this.setState({ listaEstudiante: json3 });
    } catch (error) {
      console.log(error);
    }
  }

  async handleChange(value) {
    try {
      const response4 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromisobusca/' + value + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json4 = await response4.json();

      let itemFiltrado = json4.filter(item => item.com_estado === 'PENDIENTE');

      if (itemFiltrado.length === 0) {
        const respuesta1 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json1 = await respuesta1.json();
        this.setState({ usuario: json1 });

        const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + value + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json2 = await respuesta2.json();
        this.setState({ estudiante: json2 });
        this.setState({ show: true })

      } else {
        this.setState({ estudianteCCP: value });
        this.setState({ modal1: true })
      }
    } catch (error) {
      console.log(error);
    }
  }

  handleClose() {
    window.location.reload();
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" lg="5">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-id-card fa-lg mt-1"></i> Certificados de NO ADEUDO</strong></CardTitle>
                {/*  <CardSubtitle>Gestionar Liquidacion</CardSubtitle> */}
              </CardHeader>
              <CardBody>
                <Row>
                  <Col md="1"></Col>
                  <Col xs="12" md="6" className="text-right mt-1">
                    <Search
                      data={this.state.listaEstudiante}
                      onChange={this.handleChange.bind(this)}
                      placeholder="c.i..."
                      class="search-class"
                      searchKey="ci"
                    />
                    <FormText color="dark">Seleccione numero de c.i.</FormText>
                  </Col>
                  <Col xs="12" md="5" className="text-left mt-2">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                </Row>
                <hr />
                {this.state.show && <h4><strong><code style={stylesIcono}><i className="fa fa-user"></i> Estudiante </code></strong></h4>}
                {this.state.show && <TablaEstudiante estudianteid={this.state.estudiante.ci} />}
              </CardBody>

              <CardFooter>
                <Modal isOpen={this.state.modal1} className={'modal-lg modal-danger ' + this.props.className}>
                  <ModalHeader><span className="display-4"><strong><i className="fa fa-warning fa-sm"></i> Advertencia</strong></span></ModalHeader>
                  <Alert color="danger">
                    <ModalBody>
                      <h3><strong>ESTUDIANTE CON COMPROMISO DE PAGO PENDIENTE</strong></h3>
                      <TablaEstudiante estudianteid={this.state.estudianteCCP} />
                    </ModalBody>
                    <ModalFooter>
                      <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="" href={'#/buscarcompromiso'}><i className="fa fa-pencil-square-o fa-sm"></i><span><strong>Ver Compromiso</strong></span></Button>
                      <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                    </ModalFooter>
                  </Alert>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
          <Col xs="12" lg="7">
            {this.state.show && <CertificadosDNA estudianteid={this.state.estudiante} usuarioid={this.state.usuario.id} />}
          </Col>
        </Row>
      </div>
    );
  }
}

export default Certificados;
