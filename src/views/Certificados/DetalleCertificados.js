import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col,
  FormGroup,
  Table,
  Button,
} from 'reactstrap';
import PDFcertificado from './PDFcertificado';
import EditarCertificado from './EditarCertificado'



class Detallecertificados extends Component {
  constructor(props) {
    super(props);

    this.state = {
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro.</strong></td>
                  <td align="center"><strong>Motivo</strong></td>
                  <td align="center"><strong>Fecha</strong></td>
                  <td align="left"><strong>Observacion</strong></td>
                  <td></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.props.certificados.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{i + 1}</td>
                      <td align="center">{u.cer_motivo}</td>
                      <td align="center">{u.cer_fecha}</td>
                      <td align="left">{u.cer_observacion}</td>
                      <td>
                        <PDFcertificado idcertificado={u.id} idestudiante={u.cer_ciestudiante} />
                      </td>
                      <td>
                        <EditarCertificado certificado={u} actualizaCer={this.props.actualizaCer} listaMotivo={this.props.listaMotivo} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default Detallecertificados;

