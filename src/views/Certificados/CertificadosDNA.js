import React, { Component } from 'react';
import {
  Col, Row,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormText, FormGroup,
  Button,
  InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Table,
  Label, Input,
} from 'reactstrap';
import DetalleCertificados from './DetalleCertificados'

const stylesIcono = { color: '#4682b4' }

class CertificadosDNA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      certificadoDNA: {
        cer_usuario: this.props.usuarioid,
        cer_ciestudiante: this.props.estudianteid.ci,
        cer_motivo: '',
        cer_fecha: '',
        cer_observacion: '',
      },
      motiv: {
        motivo: '',
      },
      certificadosReg: [],
      idCertificado: {},
      listaMotivo: [],
      c1: false,
      c2: false,
      c5: false,
      modal2: false,
      modal3: false,
      modal5: false,
      modal6: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnSubmitMotivo = this.handleOnSubmitMotivo.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.modalsw5 = this.modalsw5.bind(this);
    this.modalsw6 = this.modalsw6.bind(this);
    this.actualizaCertificado = this.actualizaCertificado.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
  }

  async actualizaCertificado() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/detallecertificadosbusca/' + this.props.estudianteid.ci + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ certificadosReg: json1 });
    } catch (error) {
      console.log(error);
    }
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  // modalsw3 confirmacion de registro 
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf1 = this.state.certificadoDNA;
    let inf2 = this.state.certificadoDNA;
    let inf3 = this.state.certificadoDNA;
    let inf4 = this.state.certificadoDNA;
    let inf5 = this.state.certificadoDNA;
    inf1['cer_motivo'] = '';
    inf2['cer_fecha'] = '';
    inf3['cer_observacion'] = '';
    inf4['cer_usuario'] = this.props.usuarioid;
    inf5['cer_ciestudiante'] = this.props.estudianteid.ci;
    this.setState({ inf1, inf2, inf3, inf4, inf5 });
  }

  modalsw5() {
    this.setState({
      modal5: !this.state.modal5,
    });
  }

  async modalsw6() {
    let mt = this.state.motiv;
    mt['motivo'] = ''
    this.setState({ mt });

    this.setState({
      modal6: !this.state.modal6,
      motivo: '',
    });
    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listamotivo', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaMotivo: json4 });
    } catch (error) {
      console.log(error);
    }
  }

  handleOnSubmitMotivo(e) {
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    if (this.state.motiv.motivo !== '') {
      this.setState({ modal5: !this.state.modal5 })
      try {
        fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listamotivo', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.motiv), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal6: !this.state.modal6
            })
          );
      } catch (e) {
        console.log(e);
      }
    } else {
      this.setState({ c5: true })
    }
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    try {
      fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listacertificados', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.certificadoDNA), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        )
        .then(() =>
          fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/detallecertificadosbusca/' + this.props.estudianteid.ci + '/', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          })
            .then((response) => { return response.json() })
            .then((certificadosReg) => { this.setState({ certificadosReg: certificadosReg }) })
        )
        .then(() =>
          fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listacertificados', {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          })
            .then((response) => { return response.json() })
            .then((idCertificado) => { this.setState({ idCertificado: idCertificado[0] }) })
        );
    } catch (e) {
      console.log(e);
    }
  }

  // obteniendo los compromisos de pago registrado de un estudiante
  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/detallecertificadosbusca/' + this.props.estudianteid.ci + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ certificadosReg: json1 });

      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listacertificados', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();
      this.setState({ idCertificado: json2[0] });

      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listamotivo', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaMotivo: json4 });

    } catch (error) {
      console.log(error);
    }
  }

  handleOnControll() {
    const { cer_motivo, cer_fecha } = this.state.certificadoDNA

    cer_motivo === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    cer_fecha === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (cer_motivo !== '' && cer_fecha !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  handleChange(event) {
    let cerDNA = this.state.certificadoDNA;
    cerDNA[event.target.name] = event.target.value;
    this.setState({ cerDNA });
  }

  handleChange3(event) {
    let mt = this.state.motiv;
    mt[event.target.name] = event.target.value.toUpperCase();
    this.setState({ mt });
  }

  handleClose() {
    window.location.reload();
  }

  render() {
    const { cer_motivo, cer_fecha, cer_observacion } = this.state.certificadoDNA

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-edit fa-lg mt-1"></i> Datos del Certificado de NO ADEUDO</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="4"> </Col>
                  <Col md="5">
                    {/* <strong>CERTIFICADO N° </strong>{(this.state.idCertificado.id) + 1} */}
                    <strong>CERTIFICADO N° </strong>{this.state.idCertificado === undefined ? 1 : (this.state.idCertificado.id) + 1}
                  </Col>
                </FormGroup>
                <hr />
                <Row>
                  <Col xs="12" sm="1"></Col>
                  <Col xs="12" sm="10 ">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Motivo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="cer_motivo" id="cer_motivo" required valid value={this.state.certificadoDNA.cer_motivo} onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaMotivo.map((item1, i) => (
                            <option value={item1.id} key={i}>{item1.motivo}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <Button type="button" color="success" onClick={this.modalsw5}><i className="fa fa-plus"></i></Button>
                        </InputGroupAddon>
                      </InputGroup>

                      {this.state.c1 && <FormText color="danger">Ingrese motivo del certificado</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="cer_fecha" name="cer_fecha" required valid value={this.state.certificadoDNA.cer_fecha} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese fecha</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="text-input">Observaciones</Label>
                      <Input type="textarea" rows="2" id="cer_observacion" name="cer_observacion" value={this.state.certificadoDNA.cer_observacion} onChange={this.handleChange} />
                    </FormGroup>
                    <FormGroup row>
                      <Col xs="12" md="4"></Col>
                      <Col xs="12" md="7">
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h4><strong><code style={stylesIcono}><i className="fa fa-id-card fa-lg mt-1"></i> Certificados Emitidos </code></strong></h4>
                    <DetalleCertificados certificados={this.state.certificadosReg} actualizaCer={this.actualizaCertificado} listaMotivo={this.state.listaMotivo} />
                  </Col>
                </FormGroup>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    Datos para la emisión  del certificado
                  </ModalHeader>
                  <ModalBody>
                    <Col md="12">
                      <strong> Motivo: </strong>{cer_motivo}
                    </Col>
                    <Col md="12">
                      <strong> Fecha: </strong>{cer_fecha}
                    </Col>
                    <Col md="12">
                      <strong> Observacion: </strong>{cer_observacion}
                    </Col>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Datos</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Datos registrados para la emisión del certificado
                    </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal5} className={'modal-success ' + this.props.className} size=''>
                  <ModalHeader>Nuevo Motivo</ModalHeader>
                  <ModalBody>
                    <form>
                      <FormGroup row>
                        <Col md="4">
                          <Label htmlFor="text-input">Nuevo Motivo</Label>
                        </Col>
                        <Col md="8">
                          <Input type="text" id="motivo" name="motivo" required valid value={this.state.motiv.motivo} onChange={this.handleChange3} />
                          {this.state.c5 && <FormText color="danger">Ingrese nuevo motivo</FormText>}
                        </Col>
                      </FormGroup>
                    </form>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmitMotivo}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw5}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal6} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Nuevo motivo registrado
                  </ModalBody>
                  <ModalFooter>
                    <Button color="success" size="" onClick={this.modalsw6}>Cerrar</Button>
                  </ModalFooter>
                </Modal>

              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default CertificadosDNA;
