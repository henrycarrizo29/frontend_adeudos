import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle,
  FormText,
  Button,
} from 'reactstrap';
import DescripcionCompromiso from '../DescripcionCompromiso/DescripcionCompromiso'
import Search from 'react-search-box'

class BuscarCompromiso extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estudiante: '',
      listacompromisos: [],
      detalleCompromiso: [],
      show: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    window.location.reload();
  }

  async componentDidMount() {
    try {
      const respuesta = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json1 = await respuesta.json();
      this.setState({ listacompromisos: json1 });
    } catch (e) {
      console.log(e);
    }
  }

  /* handleChange(value) {
    console.log(value);
    this.setState({estudiante:value});
    console.log(this.state.estudiante);
  } */

  async handleChange(value) {
    try {
      const respuesta = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromisobusca/' + value + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json1 = await respuesta.json();
      this.setState({
        detalleCompromiso: json1,
        show: true,
      });
    } catch (e) {
      console.log(e);
    }
  }


  render() {
    /* console.log(this.state.detalleCompromiso)
    const ccdd = this.state.detalleCompromiso.map((detalleCompromiso, i) => (
      <DescripcionCompromiso datoscom={detalleCompromiso[i]} key={i} />
    )) */
    return (
      <div className="animated fadeIn">
        <Row>
          <Col md="3"></Col>
          <Col xs="12" md="5">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-search fa-lg mt-1"></i> Buscar Compromiso de Pago</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col md="1"></Col>
                  <Col xs="12" md="5" className="text-right mt-1">
                    <Search
                      data={this.state.listacompromisos}
                      onChange={this.handleChange}
                      placeholder="c.i..."
                      class="search-class"
                      searchKey="com_estudiante"
                    />
                    <FormText color="dark">Ingresa c.i. y selecciona </FormText>
                  </Col>
                  <Col xs="12" md="5" className="text-left mt-2">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                  <Col xs="12" md="1"></Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {this.state.show && this.state.detalleCompromiso.map((detalleCompromiso, i) => (
          <DescripcionCompromiso datoscom={detalleCompromiso} key={i} />
        ))}
      </div>
    );
  }
}

export default BuscarCompromiso;