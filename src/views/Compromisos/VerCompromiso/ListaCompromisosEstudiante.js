import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  Table,
} from 'reactstrap';
import CompromisosFirmados from './CompromisosFirmados'

class ListaCompromisosEstudiante extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }


  render() {    
    const listaComp = this.props.listaC.filter((item, indice) => {
      return item.com_estudiante === this.props.idEstudiante;
    });    
    return (
      <div>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-list icons font-3xl"></i> Compromisos de Pago</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive size="sm" hover>
                  <thead>
                    <tr>
                      <td align="center"><strong>Nro.</strong></td>
                      <td align="center"><strong>C.I. Est.</strong></td>
                      <td align="center"><strong>Estudiante</strong></td>
                      <td align="center"><strong>Carrera</strong></td>
                      <td align="center"><strong>Garante</strong></td>
                      <td align="center"><strong>Fecha Compromiso</strong></td>
                      <td align="center"><strong>Medico tratante</strong></td>
                      <td align="center"><strong>Observacion</strong></td>
                      {/* <td align="center"><strong>Accion</strong></td> */}
                    </tr>
                  </thead>
                  <tbody>
                    {listaComp.map((compromiso, i) => (
                      <CompromisosFirmados datoscom={compromiso} />
                    ))}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ListaCompromisosEstudiante;
