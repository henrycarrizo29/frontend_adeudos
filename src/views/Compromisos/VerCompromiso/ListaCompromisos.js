import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  Table,
  Pagination, PaginationItem, PaginationLink,
} from 'reactstrap';
import CompromisosFirmados from './CompromisosFirmados'

class ListaCompromisos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      compromisos: this.props.listaC,
      paginaActual: 0,
      elementosPag: 15,
    };
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e, index) {
    e.preventDefault();
    this.setState({
      paginaActual: index
    });
  }

  // obteniendo la lista de compromisos registrados
  /* async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({
        compromisos: json1,
      });
      console.log(json1)
    } catch (e) {
      console.log(e);
    }
  } */


  render() {
    const contarPaginas = Math.ceil(this.state.compromisos.length / this.state.elementosPag);
    const { paginaActual, elementosPag } = this.state;
    const fin = (paginaActual + 1) * elementosPag;
    const inicio = fin - elementosPag;
    const listaComp = this.state.compromisos.slice(inicio, fin);

    return (
      <div>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-list icons font-3xl"></i> Compromisos de Pago</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive size="sm" hover>
                  <thead>
                    <tr>
                      <td align="center"><strong>Nro.</strong></td>
                      <td align="center"><strong>C.I. Est.</strong></td>
                      <td align="center"><strong>Estudiante</strong></td>
                      <td align="center"><strong>Carrera</strong></td>
                      <td align="center"><strong>Garante</strong></td>
                      <td align="center"><strong>Fecha Compromiso</strong></td>
                      <td align="center"><strong>Medico tratante</strong></td>
                      <td align="center"><strong>Observacion</strong></td>
                      {/* <td align="center"><strong>Accion</strong></td> */}
                    </tr>
                  </thead>
                  <tbody>
                    {listaComp.map((compromiso, i) => (
                      <CompromisosFirmados datoscom={compromiso}/>
                    ))}
                  </tbody>
                </Table>
                <Row>
                  <Col md="10">
                    <Pagination size="lg">
                      {[...Array(contarPaginas)].map((page, i) =>
                        <PaginationItem active={i === paginaActual} key={i}>
                          <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                            {i + 1}
                          </PaginationLink>
                        </PaginationItem>
                      )}
                    </Pagination>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ListaCompromisos;
