import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  FormGroup, FormText,
  Input, Label,
  Button,
} from 'reactstrap';
import ListaCompromisos from './ListaCompromisos';
import ListaCompromisosPorFecha from './ListaCompromisosPorFecha';
import ListaCompromisosEstudiante from './ListaCompromisosEstudiante';
import Search from 'react-search-box'

const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }

class VerCompromiso extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fecha1: '',
      fecha2: '',
      c1: false,
      c2: false,
      checked: [false, false, false],
      compromisos: [],
      idEstudiante: '',
      comFecha: false,
      comEstudiante: false,
    };
    this.handleSearchKey = this.handleSearchKey.bind(this)
    this.handleChange = this.handleChange.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    window.location.reload();
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleChange2(value) {
    this.setState({
      idEstudiante: value,
      comEstudiante: true,
    });
  }

  handleSearchKey(e) {
    const { value, name, id } = e.target;
    var a = new Array(4).fill(false)
    switch (id) {
      case "1":
        a[0] = true
        this.setState({
          fecha1: '',
          fecha2: '',
          comEstudiante: false,
          comFecha: false,
        })
        break;
      case "2":
        a[1] = true
        this.setState({
          comEstudiante: false
        })
        break;
      case "3":
        a[2] = true
        this.setState({
          fecha1: '',
          fecha2: '',
          comFecha: false,
        })
        break;

      default:
        a[0] = true
    }
    this.setState({
      checked: a,
    })
  }

  handleOnControll() {
    const { fecha1, fecha2 } = this.state

    fecha1 === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    fecha2 === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (fecha1 !== '' && fecha2 !== '') {
      this.setState({
        comFecha: true
      });
    }
  }


  // obteniendo la lista de compromisos registrados
  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({
        compromisos: json1
      });
    } catch (e) {
      console.log(e);
    }
  }


  render() {

    return (
      <div>
        <Row>
          <Col md="2"></Col>
          <Col xs="12" md="8">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-list icons font-3xl"></i> Ver Compromisos de pago</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="12">
                    <FormGroup check inline>
                      <Input onChange={this.handleSearchKey} checked={this.state.checked[0]} className="form-check-input" type="radio" id="1" name="busqueda" value="option1" />
                      <Label className="form-check-label" check htmlFor="radio1"><i className="fa fa-users fa-lg mt-0" style={stylesUsuarios}></i> Todos los Compromisos</Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Input onChange={this.handleSearchKey} checked={this.state.checked[1]} className="form-check-input" type="radio" id="2" name="busqueda" value="option2" />
                      <Label className="form-check-label" check htmlFor="radio2"><i className="fa fa-calendar fa-lg mt-0" style={stylesFecha}></i> Compromisos por Fecha</Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Input onChange={this.handleSearchKey} checked={this.state.checked[2]} className="form-check-input" type="radio" id="3" name="busqueda" value="option3" />
                      <Label className="form-check-label" check htmlFor="radio3"><i className="fa fa-user fa-lg mt-0" style={stylesUsuario}></i> Compromisos por Estudiante</Label>
                    </FormGroup>
                  </Col>
                </FormGroup>

                <hr></hr>

                {this.state.checked[1] && <FormGroup row>
                  <Col xs="12" md="4">
                    <Label htmlFor="text-input">Fecha inicio <i className="fa fa-calendar-minus-o fa-lg mt-0" style={stylesIcono}></i></Label>
                    <Input type="date" id="fecha1" name="fecha1" value={this.state.fecha1} required valid onChange={this.handleChange} />
                    {this.state.c1 && <FormText color="danger">Ingrese fecha inicio </FormText>}
                  </Col>
                  <Col xs="12" md="4">
                    <Label htmlFor="text-input">Fecha fin <i className="fa fa-calendar-plus-o fa-lg mt-0" style={stylesIcono}></i></Label>
                    <Input type="date" id="fecha2" name="fecha2" value={this.state.fecha2} required valid onChange={this.handleChange} />
                    {this.state.c2 && <FormText color="danger">Ingrese fecha fin</FormText>}
                  </Col>
                  <Col xs="12" md="4">
                    <br></br>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleOnControll}><i className="fa fa-bullseye fa-sm"></i><span><strong>Ver</strong></span></Button>
                  </Col>
                </FormGroup>}

                {this.state.checked[2] && <FormGroup row>
                  <Col xs="12" md="2"></Col>
                  <Col xs="12" md="4">
                    <Search
                      data={this.state.compromisos}
                      onChange={this.handleChange2}
                      placeholder="c.i..."
                      class="search-class"
                      searchKey="com_estudiante"
                    />
                    <FormText color="dark">Ingresa c.i. y selecciona </FormText>
                  </Col>
                  <Col xs="12" md="4">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                  <Col xs="12" md="1"></Col>
                </FormGroup>}

              </CardBody>
            </Card>
          </Col>
          <Col md="2"></Col>

          <Col md="12">
            {this.state.checked[0] && <ListaCompromisos listaC={this.state.compromisos} />}
          </Col>

          <Col md="12">
            {this.state.comFecha && <ListaCompromisosPorFecha listaC={this.state.compromisos} f1={this.state.fecha1} f2={this.state.fecha2} />}
          </Col>

          <Col md="12">
            {this.state.comEstudiante && <ListaCompromisosEstudiante listaC={this.state.compromisos} idEstudiante={this.state.idEstudiante} />}
          </Col>

        </Row>
      </div>
    );
  }
}

export default VerCompromiso;
