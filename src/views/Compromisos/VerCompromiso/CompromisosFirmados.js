import React, { Component } from 'react';
import { Popover, PopoverBody, Button } from 'reactstrap';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'

class PopoverItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popoverOpen: false,
    };

    this.popoversw = this.popoversw.bind(this);
  }

  popoversw() {
    this.setState({
      popoverOpen: !this.state.popoverOpen,
    });
  }

  render() {
    return (
      <span>
        <Button className="mr-1" color="secondary" size="sm" id={'Popover-' + this.props.id} onClick={this.popoversw}>
          Ver
        </Button>
        <Popover placement="left" isOpen={this.state.popoverOpen} target={'Popover-' + this.props.id} popoversw={this.popoversw}>
          <PopoverBody>{this.props.itemObs}</PopoverBody>
        </Popover>
      </span>
    );
  }
}



class CompromisosFirmados extends Component {
  constructor(props) {
    super(props)

    this.state = {
      est: {},
      gar: {},
      med: {},
      carr: {},
      fac: {},
      par: {},
      dia: '',
      mes: '',
      anio: '',
    }
  }

  async componentDidMount() {
    if (this.props.datoscom.com_medico_acargo !== null) {
      try {
        const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/detallepersonal/' + this.props.datoscom.com_medico_acargo + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        });
        const json1 = await respuesta1.json();
        this.setState({ med: json1[0] });
      } catch (error) {
        console.log(error);
      }
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.datoscom.com_estudiante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json2 = await respuesta2.json();
      this.setState({ est: json2 });

      try {
        const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallecarrera/' + this.state.est.Carrera, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        });
        const json4 = await respuesta4.json();
        this.setState({ carr: json4[0] });

        try {
          const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallefacultad/' + this.state.carr.facultad, {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            },
          });
          const json5 = await respuesta5.json();
          this.setState({ fac: json5[0] });
        } catch (error) {
          console.log(error);
        }
      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.datoscom.com_garante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json3 = await respuesta3.json();
      this.setState({ gar: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detalleparentesco/' + this.props.datoscom.com_parentesco, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json6 = await respuesta6.json();
      this.setState({ par: json6[0] });
    } catch (error) {
      console.log(error);
    }

    var fe = this.props.datoscom.com_fecha;
    var fecha = fe.split("-");
    this.setState({
      dia: fecha[2],
      mes: fecha[1],
      anio: fecha[0],
    })
  }

  render() {
    return (
      <tr>
        <td align="center">
          {this.props.datoscom.id}
        </td>
        <td>
          {this.state.est.ci}
        </td>
        <td>
          {this.state.est.nombres + ' ' + this.state.est.primer_apellido + ' ' + this.state.est.segundo_apellido}
        </td>
        <td>
          {this.state.carr.carrera}
        </td>
        <td>
          {this.state.gar.gar_nombre + ' ' + this.state.gar.gar_apellido1 + ' ' + this.state.gar.gar_apellido2}
        </td>
        <td align="center">
          {this.props.datoscom.com_fecha}
        </td>
        <td>
          {this.state.med.nombres !== undefined
            ? this.state.med.nombres + ' ' + this.state.med.primer_apellido + ' ' + this.state.med.segundo_apellido
            : this.state.med.nombres
          }
        </td>
        <td align="center">
          <PopoverItem itemObs={this.props.datoscom.com_observacion} id={this.props.datoscom.id} />
         {/*  {this.props.datoscom.com_observacion} */}
        </td>
      </tr>
    )
  }
}

export default CompromisosFirmados;
