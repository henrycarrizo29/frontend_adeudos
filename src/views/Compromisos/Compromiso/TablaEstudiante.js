import React, { Component } from 'react';
import {
  Table,
} from 'reactstrap';

class TablaEstudiante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logged_in: localStorage.getItem('token') ? true : false,
      estudiantedet: {},
      carrera: {},
      facultad: {},
    };
  }

  /*  buscaCarrera(idCarr) {
     fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallecarrera/' + idCarr + '/')
       .then((response) => { return response.json() })
       .then((carrera) => { this.setState({ carrera: carrera }) })
   }
 
   async  buscaCarrera(idCarr) {
     console.log(idCarr)
     const respuesta = await fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallecarrera/' + idCarr + '/');
     const json = await respuesta.json();
     return (json[0].carr_nombre);
   } */


  componentDidMount() {
    fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.estudianteid + '/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      },
    })
      .then((response) => { return response.json() })
      .then((estudiantedet) => { this.setState({ estudiantedet: estudiantedet }) })
      .then((res) => {
        fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallecarrera/' + this.state.estudiantedet.Carrera + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        })
          .then((response) => { return response.json() })
          .then((carrera) => { this.setState({ carrera: carrera[0] }) })
          .then(() => {
            fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallefacultad/' + this.state.carrera.facultad + '/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              },
            })
              .then((response) => { return response.json() })
              .then((facultad) => { this.setState({ facultad: facultad[0] }) })
          })
      })
  }

  render() {
    return (
      <div>
        <Table hover responsive borderless size="sm">
          <tbody>
            <tr>
              <td align="right"><strong>c.i.:</strong></td>
              <td>-{this.state.estudiantedet.ci}</td>
            </tr>
            <tr>
              <td align="right"><strong>Nombre</strong></td>
              <td>-{this.state.estudiantedet.nombres + ' ' + this.state.estudiantedet.primer_apellido + ' ' + this.state.estudiantedet.segundo_apellido}</td>
            </tr>
            {/* <tr>
                    <td align="right"><strong>Genero:</strong></td>
                    <td>-{this.state.estudiantedet.est_genero}</td>
                  </tr> */}
            <tr>
              <td align="right"><strong>Direccion:</strong></td>
              <td>-{this.state.estudiantedet.direccion}</td>
            </tr>
            <tr>
              <td align="right"><strong>Carrera:</strong></td>
              <td>-{this.state.carrera.carrera}</td>
            </tr>
            <tr>
              <td align="right"><strong>Facultad:</strong></td>
              <td>-{this.state.facultad.facultad}</td>
            </tr>
            <tr>
              <td align="right"><strong>Telefono:</strong></td>
              <td>-{this.state.estudiantedet.telefono}</td>
            </tr>
            <tr>
              <td align="right"><strong>Celular:</strong></td>
              <td>-{this.state.estudiantedet.celular}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaEstudiante;
