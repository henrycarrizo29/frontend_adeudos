import React, { Component } from 'react';
import {
  Table,
} from 'reactstrap';

class TablaGarante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      garantedet: {},
    };
  }

  async componentDidMount() {
    try {
      const response = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.garanteid + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json1 = await response.json();
      this.setState({ garantedet: json1 });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    return (
      <div>
        <Table hover responsive borderless size="sm">
          <tbody>
            <tr>
              <td align="right"><strong>c.i.:</strong></td>
              <td>-{this.state.garantedet.gar_ci + ' ' + this.state.garantedet.gar_ciorigen}</td>
            </tr>
            <tr>
              <td align="right"><strong>Nombre:</strong></td>
              <td>-{this.state.garantedet.gar_nombre + ' ' + this.state.garantedet.gar_apellido1 + ' ' + this.state.garantedet.gar_apellido2}</td>
            </tr>
            {/*  <tr>
                    <td align="right"><strong>Genero:</strong></td>
                    <td>-{this.state.garantedet.gar_genero}</td>
                  </tr> */}
            <tr>
              <td align="right"><strong>Direccion:</strong></td>
              <td>-{this.state.garantedet.gar_direccion}</td>
            </tr>
            <tr>
              <td align="right"><strong>Telefono:</strong></td>
              <td>-{this.state.garantedet.gar_telefono}</td>
            </tr>
            <tr>
              <td align="right"><strong>Celular:</strong></td>
              <td>-{this.state.garantedet.gar_celular}</td>
            </tr>
            <tr>
              <td align="right"><strong>Correo:</strong></td>
              <td>-{this.state.garantedet.gar_correo}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaGarante;
