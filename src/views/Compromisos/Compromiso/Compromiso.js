import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader,
  Col, Row,
  CardTitle, CardFooter,
  Button,
  Label,
  FormText, FormGroup,
  Input,
  Modal, ModalBody, ModalHeader, ModalFooter,
  Alert,
  InputGroupAddon, InputGroup,
} from 'reactstrap';

import Search from 'react-search-box';
import CardEstudiante from './CardEstudiante'
import CardGarante from './CardGarante';
import DatosCompromiso from './DatosCompromiso'
import DescripcionCompromiso from '../DescripcionCompromiso/DescripcionCompromiso';

const stylesIcono = { color: '#4682b4' }

class Compromiso extends Component {
  constructor(props) {
    super(props);
    //calculamos la fecha actual
    var hoy = new Date(),
      date = hoy.getDate() + '/' + (hoy.getMonth() + 1) + '/' + hoy.getFullYear();
    /* date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate(); */

    this.state = {
      logged_in: localStorage.getItem('token') ? true : false,
      fecha: date,
      cdp: {
        com_usuario: 0,
        com_estudiante: '',
        com_garante: '',
        com_parentesco: '',
        com_fecha: '',
        com_medico_acargo: '',
        com_observacion: '',
      },
      listaMedicos: [],
      listaEstudiante: [],
      listaGarante: [],
      listaCompromiso: [],
      listaParentesco: [],
      parent: {
        parentesco: '',
      },

      modal2: false,
      modal3: false,
      modal4: false,
      modal5: false,
      modal6: false,
      show1: false,
      show2: false,
      show3: false,
      disabled: false,

      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.modalsw4 = this.modalsw4.bind(this);
    this.modalsw5 = this.modalsw5.bind(this);
    this.modalsw6 = this.modalsw6.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleOnSubmitParentesco = this.handleOnSubmitParentesco.bind(this);
  }

  // modalsw2 para confirmar el registro del compromiso de pago
  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  // modalsw3 de confirmacion de registro de compromiso de pago
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
      show3: true,
      disabled: true,
    });
    /* window.location.reload()
    window.location = '/promes/vercompromiso'; */
  }

  modalsw4() {
    this.setState({
      modal4: !this.state.modal4,
    });
  }

  modalsw5() {
    this.setState({
      modal5: !this.state.modal5,
    });
  }

  async modalsw6() {
    let pr = this.state.parent;
    pr['parentesco'] = ''
    this.setState({ pr });

    this.setState({
      modal6: !this.state.modal6,
      parentesco: '',
    });
    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listaparentesco', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaParentesco: json4 });
    } catch (error) {
      console.log(error);
    }
  }

  handleClose() {
    window.location.reload();
  }

  //capturando los valores que escribe el usuario, con la funcion hadleChage
  handleChange(event) {
    let compr = this.state.cdp;
    compr[event.target.name] = event.target.value;
    this.setState({ compr });
    console.log(compr);
  }

  /* handleChange = (e) => {
    const target = e.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value,
    })
  } */


  /* handleChange(e) {
    this.setState({ [e.target.name] : e.target.value });
 } */

  //capturando el valor del estudiante en el buscador
  handleChange1(value) {
    const nuevo = this.state.listaCompromiso.filter(valor => {
      return valor.com_estudiante === value
    })

    if (nuevo.length !== 0) {
      this.setState({
        modal4: !this.state.modal4,
      });
    }

    let compr = this.state.cdp;
    compr["com_estudiante"] = value;
    this.setState({ compr });
    this.setState({
      show1: true
    })
  }

  //capturando el valor del garante en el buscador
  handleChange2(value) {
    let compr = this.state.cdp;
    compr["com_garante"] = value;
    this.setState({ compr });
    this.setState({
      show2: true
    })
  }

  handleChange3(event) {
    let pr = this.state.parent;
    pr[event.target.name] = event.target.value.toUpperCase();
    this.setState({ pr });
  }

  // obteniendo la lista de todos los medicos, estudiantes y garantes
  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/listaestudiante', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ listaEstudiante: json1 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/listagarante', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();
      this.setState({ listaGarante: json2 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ listaMedicos: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listaparentesco', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaParentesco: json4 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      this.setState({ listaCompromiso: json5 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json6 = await respuesta6.json();
      let compr = this.state.cdp;
      compr['com_usuario'] = json6.id
      this.setState({ compr });
    } catch (error) {
      console.log(error);
    }
  }

  // registrando el compromiso de pago
  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    try {
      fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.cdp), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  handleOnSubmitParentesco(e) {
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    if (this.state.parent.parentesco !== '') {
      this.setState({ modal5: !this.state.modal5 })
      try {
        fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listaparentesco', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.parent), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal6: !this.state.modal6
            })
          );
      } catch (e) {
        console.log(e);
      }
    } else {
      this.setState({ c5: true })
    }
  }


  // control para los modales
  handleOnControll() {
    const { com_estudiante, com_garante, com_parentesco, com_fecha } = this.state.cdp

    com_estudiante === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    com_garante === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    com_parentesco === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    com_fecha === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    if (com_estudiante !== '' && com_garante !== '' && com_parentesco !== '' && com_fecha !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }


  render() {
    return (
      <div>
        <Row>
          <Col xs="12" md="6">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-note icons font-3xl"></i> Compromiso de Pago</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col xs="12" md="9">
                    <h5><strong style={stylesIcono}> Formulario de registro </strong></h5>
                  </Col>
                  <Col xs="12" md="3">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="sm" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>
                </FormGroup>
                <hr />
                <form>
                  <FormGroup row>
                    <Col xs="12" md="6">
                      <Label htmlFor="text-input">Estudiante C.I. *</Label>
                      <Search
                        data={this.state.listaEstudiante}
                        onChange={this.handleChange1}
                        placeholder="c.i."
                        class="search-class"
                        searchKey="ci"
                      />
                      {this.state.c1 && <FormText color="danger">Se requiere C.I. del Estudiante</FormText>}
                    </Col>

                    <Col xs="12" md="6">
                      <Label htmlFor="text-input">Garante C.I. *</Label>
                      <Search
                        data={this.state.listaGarante}
                        onChange={this.handleChange2}
                        placeholder="c.i."
                        class="search-class"
                        searchKey="gar_ci"
                      />
                      {this.state.c2 && <FormText color="danger">Se requiere C.I. del Garante</FormText>}
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="6">
                      <Label htmlFor="name">Parentesco *</Label>
                      <InputGroup>
                        <Input type="select" name="com_parentesco" id="com_parentesco" required valid value={this.state.com_parentesco} onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaParentesco.map((item1, i) => (
                            <option value={item1.id} key={i}>{item1.parentesco}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <Button type="button" color="success" onClick={this.modalsw5}><i className="fa fa-plus"></i></Button>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Se requiere relación de parentesco</FormText>}
                    </Col>

                    <Col xs="12" md="6">
                      <Label htmlFor="text-input">Fecha de Compromiso *</Label>
                      <Input type="date" id="com_fecha" name="com_fecha" value={this.state.com_fecha} required valid onChange={this.handleChange} />
                      {this.state.c4 && <FormText color="danger">Ingrese fecha hoy es: {this.state.fecha}</FormText>}
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col xs="auto" md="4">
                      <Label htmlFor="text-input">Medico Tratante</Label>
                    </Col>
                    <Col xs="12" md="8">
                      <Input type="select" name="com_medico_acargo" id="com_medico_acargo" value={this.state.com_medico_acargo} onChange={this.handleChange}>
                        <option value=""></option>
                        {this.state.listaMedicos.map((item2, i) => (
                          <option value={item2.ci} key={i}>{item2.nombres + ' ' + item2.primer_apellido}</option>
                        ))}
                      </Input>
                      {/* this.state.c5 && <FormText color="danger">Se requiere medico tratante</FormText> */}
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="4">
                      <Label htmlFor="text-input">Observaciones</Label>
                    </Col>
                    <Col md="8">
                      <Input type="text" id="com_observacion" name="com_observacion" value={this.state.com_observacion} onChange={this.handleChange} />
                    </Col>
                  </FormGroup>
                </form>
              </CardBody>
              <CardFooter>
                <FormGroup row>
                  <Col xs="12" md="2"></Col>
                  <Col xs="12" md="10">
                    <Button disabled={this.state.disabled} className="btn-vine btn-brand mr-1 mb-1" size="" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Compromiso de Pago</strong></span></Button>
                    {/* ctrl + shift + i   da formato al documento (ordena el codigo) */}
                    <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                      <ModalHeader>
                        <i className="fa fa-file-text-o fa-lg mt-1"> </i> Compromiso de pago
                      </ModalHeader>
                      <ModalBody>
                        <DatosCompromiso datoscom={this.state.cdp} />
                      </ModalBody>
                      <ModalFooter>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Compromiso de Pago</strong></span></Button>
                        <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                        {/* <Button color="primary" onClick={this.handleOnSubmit}>Registrar Compromiso de Pago</Button>
                        <Button color="secondary" onClick={this.modalsw2}>Cancelar</Button> */}
                      </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                      <ModalHeader>
                        Mensaje
                      </ModalHeader>
                      <ModalBody>
                        Compromiso de pago registrado
                    </ModalBody>
                      <ModalFooter>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                        {/*  <Button color="success" onClick={this.modalsw3}>Cerrar</Button> */}
                      </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.modal4} className={'modal-lg modal-danger ' + this.props.className}>
                      <ModalHeader><span className="display-3"><strong>Advertencia</strong></span></ModalHeader>
                      <Alert color="danger">
                        <ModalBody>
                          <h3><strong>ESTUDIANTE CON COMPROMISO DE PAGO PENDIENTE</strong></h3>
                        </ModalBody>
                        <ModalFooter>
                          <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw4}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar de Todas Formas</strong></span></Button>
                          <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleClose}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar Registro</strong></span></Button>
                          {/* <Button color="danger" size="lg" onClick={this.modalsw4}>Registrar de Todas Formas</Button>
                          <Button color="secondary" size="lg" onClick={this.handleClose}>Cancelar Registro </Button> */}
                        </ModalFooter>
                      </Alert>
                    </Modal>

                    <Modal isOpen={this.state.modal5} className={'modal-primary ' + this.props.className} size=''>
                      <ModalHeader>Nuevo Parentesco</ModalHeader>
                      <ModalBody>
                        <form>
                          <FormGroup row>
                            <Col md="4">
                              <Label htmlFor="text-input">Nuevo Parentesco</Label>
                            </Col>
                            <Col md="8">
                              <Input type="text" id="parentesco" name="parentesco" required valid value={this.state.parent.parentesco} onChange={this.handleChange3} />
                              {this.state.c5 && <FormText color="danger">Ingrese nuevo parentesco</FormText>}
                            </Col>
                          </FormGroup>
                        </form>
                      </ModalBody>
                      <ModalFooter>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmitParentesco}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                        <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw5}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                      </ModalFooter>
                    </Modal>

                    <Modal isOpen={this.state.modal6} className={'modal-success ' + this.props.className} size='sm'>
                      <ModalHeader>Mensaje</ModalHeader>
                      <ModalBody>
                        Nuevo parentesco registrado
                      </ModalBody>
                      <ModalFooter>
                        <Button color="success" size="" onClick={this.modalsw6}>Cerrar</Button>
                      </ModalFooter>
                    </Modal>

                  </Col>
                </FormGroup>
              </CardFooter>
            </Card>
          </Col>
          <Col xs="12" md="6">
            <FormGroup row>
              <Col xs="12" md="12">
                {this.state.show1 && <CardEstudiante estudianteid={this.state.cdp.com_estudiante} />}
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs="12" md="12">
                {this.state.show2 && <CardGarante garanteid={this.state.cdp.com_garante} />}
              </Col>
            </FormGroup>
          </Col>
          <Col xs="12" md="12">
            {this.state.show3 && <DescripcionCompromiso datoscom={this.state.cdp} />}
          </Col>
        </Row>
      </div >
    );
  }
}

export default Compromiso;