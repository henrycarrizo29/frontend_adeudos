import React, { Component } from 'react';
import {
  Table,
} from 'reactstrap';

class TablaEstudiante2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estudiantedet: {},
      carrera: {},
      facultad: {},
    };
  }

  /*  buscaCarrera(idCarr) {
     fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallecarrera/' + idCarr + '/')
       .then((response) => { return response.json() })
       .then((carrera) => { this.setState({ carrera: carrera }) })
   }
 
   async  buscaCarrera(idCarr) {
     console.log(idCarr)
     const respuesta = await fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallecarrera/' + idCarr + '/');
     const json = await respuesta.json();
     return (json[0].carr_nombre);
   } */


  componentDidMount() {
    fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.estudianteid + '/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      },
    })
      .then((response) => { return response.json() })
      .then((estudiantedet) => { this.setState({ estudiantedet: estudiantedet }) })
      .then((estudiantedet) => {
        fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallecarrera/' + this.state.estudiantedet.est_carrera + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        })
          .then((response) => { return response.json() })
          .then((carrera) => { this.setState({ carrera: carrera[0] }) })
          .then(() => {
            fetch('http://0.0.0.0:8000/adeudos/facultades_carreras/v1/detallefacultad/' + this.state.carrera.carr_facultad + '/', {
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`
              },
            })
              .then((response) => { return response.json() })
              .then((facultad) => { this.setState({ facultad: facultad[0] }) })
          })
      })
  }

  render() {
    return (
      <div>
        <Table hover responsive borderless size="sm">
          <tbody>
            <tr>
              <td align="center"><strong>C.I.</strong></td>
              <td align="center"><strong>Nombre</strong></td>
              <td align="center"><strong>Direccion</strong></td>
              <td align="center"><strong>Carrera</strong></td>
              <td align="center"><strong>Facultad</strong></td>
              <td align="center"><strong>Telefono</strong></td>
              <td align="center"><strong>Celular</strong></td>
            </tr>
            <tr>
              <td>-{this.state.estudiantedet.ci}</td>
              <td>-{this.state.estudiantedet.nombres + ' ' + this.state.estudiantedet.primer_apellido + ' ' + this.state.estudiantedet.segundo_apellido}</td>
              <td>-{this.state.estudiantedet.direccion}</td>
              <td>-{this.state.carrera.carrera}</td>
              <td>-{this.state.facultad.facultad}</td>
              <td>-{this.state.estudiantedet.telefono}</td>
              <td>-{this.state.estudiantedet.celular}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaEstudiante2;
