import React, { Component } from 'react';
import {
  Col,
  Row,
  Card, CardHeader, CardBody, CardTitle,
} from 'reactstrap';
import TablaEstudiante from './TablaEstudiante'

class CardEstudiante extends Component {

  render() {
    return (
      <Row>
        <Col xs="12" md="12">
          <Card className="border-primary">
            <CardHeader tag="h5">
              <CardTitle><strong><i className="fa fa-user fa-lg mt-1"></i> Estudiante</strong></CardTitle>
            </CardHeader>
            <CardBody>
              <TablaEstudiante estudianteid={this.props.estudianteid}/>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default CardEstudiante;
