import React, { Component } from 'react';
import { Col, Row, Alert } from 'reactstrap';
import TablaEstudiante from './TablaEstudiante'
import TablaGarante from './TablaGarante'
import TablaMedico from './TablaMedico'

const stylesIcono = { color: '#4682b4' }

class DatosCompromiso extends Component {

  render() {
    return (
      <Row>
        <Col xs="12" md="12">
          <h5><strong style={stylesIcono}><i className="fa fa-user fa-sm"></i> Estudiante </strong></h5>
          <TablaEstudiante estudianteid={this.props.datoscom.com_estudiante} />
          <h5><strong style={stylesIcono}><i className="fa fa-user-o fa-sm"></i> Garante </strong></h5>
          <TablaGarante garanteid={this.props.datoscom.com_garante} />
          <h5><strong style={stylesIcono}><i className="fa fa-file-text fa-sm"></i> Compromiso </strong></h5>
          <TablaMedico medicoid={this.props.datoscom} />
        </Col>
      </Row>
    );
  }
}

export default DatosCompromiso;
