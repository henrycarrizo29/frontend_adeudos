import React, { Component } from 'react';
import {
  Col,
  Row,
  Card, CardHeader, CardBody, CardTitle,
} from 'reactstrap';
import TablaGarante from './TablaGarante'

class CardGarante extends Component {

  render() {
    return (
      <Row>
        <Col xs="12" md="12">
          <Card className="border-primary">
            <CardHeader tag="h5">
              <CardTitle><strong><i className="fa fa-user-o fa-lg mt-1"></i> Garante</strong></CardTitle>
            </CardHeader>
            <CardBody>
              <TablaGarante garanteid={this.props.garanteid}/>
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default CardGarante;