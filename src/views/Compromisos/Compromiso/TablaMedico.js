import React, { Component } from 'react';
import {
  Table,
} from 'reactstrap';

class TablaMedico extends Component {
  constructor(props) {
    super(props);
    this.state = {
      medicodet: {
        nombres:'',
        primer_apellido:'',
        segundo_apellido:'',
      },
    };
  }

  async componentDidMount() {
    if (this.props.medicoid.com_medico_acargo !== '') {
      try {
        const response = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/detallepersonal/' + this.props.medicoid.com_medico_acargo + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        })
        const json1 = await response.json();
        this.setState({ medicodet: json1[0] });
      } catch (error) {
        console.log(error);
      }
    }
  }

  render() {
    return (
      <div>
        <Table hover responsive borderless size="sm">
          <tbody>
            <tr>
              <td align="right"><strong>Medico tratante:</strong></td>
              <td>-{this.state.medicodet.nombres + ' ' + this.state.medicodet.primer_apellido + ' ' + this.state.medicodet.segundo_apellido}</td>
            </tr>
            <tr>
              <td align="right"><strong>Fecha de compromiso:</strong></td>
              <td>-{this.props.medicoid.com_fecha}</td>
            </tr>
            <tr>
              <td align="right"><strong>Observaciones:</strong></td>
              <td>-{this.props.medicoid.com_observacion}</td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}

export default TablaMedico;
