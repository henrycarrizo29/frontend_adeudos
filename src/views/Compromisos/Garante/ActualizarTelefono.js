import React, { Component } from 'react';
import { Button, Col, Input, Label, FormText, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table, FormGroup, CardTitle } from 'reactstrap';
import 'jspdf-autotable'
import TablaGarante from '../Compromiso/TablaGarante';

class ActualizarTelefono extends Component {
  constructor(props) {
    super(props)

    this.state = {
      gar: {},
      c6: false,
      c7: false,

      gar_telefono: '',
      gar_celular: '',

      modal1: true,
      modal2: false,
      modal3: false,
    }
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.cancelarRegistro = this.cancelarRegistro.bind(this);
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    window.location.href = '#/vergarantes'
  }

  cancelarRegistro() {
    window.location.href = '#/vergarantes'
  }

  handleChange1(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();
    try {
      fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.match.params.gar_ci + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(this.state.gar), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.match.params.gar_ci + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ gar: json3 });
    } catch (error) {
      console.log(error);
    }
  }

  handleOnControll() {
    const { gar_telefono, gar_celular } = this.state

    gar_telefono === ''
      ? (this.setState({ c6: true }))
      : (this.setState({ c6: false }))

    gar_celular === ''
      ? (this.setState({ c7: true }))
      : (this.setState({ c7: false }))

    if (gar_telefono !== '' || gar_celular !== '') {
      let gar = this.state.gar;
      gar.gar_telefono === null
        ? gar['gar_telefono'] = gar_telefono
        : gar['gar_telefono'] = gar_telefono + '-' + gar.gar_telefono;

      gar.gar_celular === null
        ? gar['gar_celular'] = gar_celular
        : gar['gar_celular'] = gar_celular + '-' + gar.gar_celular;

      this.setState({
        gar,
        modal2: !this.state.modal2,
      });
    }
  }



  render() {
    return (
      <div>
        <Row>
          <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='lg'>
            <ModalHeader>
              <CardTitle><strong><i className="icon-user-follow icons font-3xl"></i> Garante</strong></CardTitle>
            </ModalHeader>
            <ModalBody>
              <h5><strong>Datos personales del garante</strong></h5>
              <TablaGarante garanteid={this.props.match.params.gar_ci} />
              <hr />
              <form>
                <h5><strong>Actualizar Telefono del Garante</strong></h5>
                <FormGroup row>
                  <Col md="1"> </Col>
                  <Col md="2">
                    <FormGroup>
                      <Label htmlFor="name">Telefono</Label>
                      <Input type="text" id="gar_telefono" name="gar_telefono" value={this.state.gar_telefono} onChange={this.handleChange1} />
                      {this.state.c6 && <FormText color="danger">Ingrese telefono</FormText>}
                    </FormGroup>
                  </Col>
                  <Col md="2">
                    <FormGroup>
                      <Label htmlFor="name">Celular</Label>
                      <Input type="text" id="gar_celular" name="gar_celular" value={this.state.gar_celular} onChange={this.handleChange1} />
                      {this.state.c7 && <FormText color="danger">Ingrese celular</FormText>}
                    </FormGroup>
                  </Col>
                  <Col md="5">
                    <FormGroup>
                      <Label htmlFor="name">Correo</Label>
                      <Input type="text" id="gar_correo" name="gar_correo" value={this.state.gar_correo} onChange={this.handleChange1} />
                      {/* <FormText color="dark">Ingrese correo electronico</FormText> */}
                    </FormGroup>
                  </Col>
                </FormGroup>
              </form>
              <FormGroup row>
                <Col md="3"> </Col>
                <Col xs="12" md="3">
                  <FormGroup>
                    <Button className="btn-github btn-brand mr-1 mb-1" size="sm" type="submit" onClick={this.cancelarRegistro}><i className="fa icon-logout fa-sm"></i><span><strong>Cancelar Registro</strong></span></Button>
                  </FormGroup>
                </Col>
                <Col xs="12" md="3">
                  <FormGroup>
                    <Button className="btn-vimeo btn-brand mr-1 mb-1" size="sm" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guardar Cambios</strong></span></Button>
                  </FormGroup>
                </Col>
              </FormGroup>
            </ModalBody>
          </Modal>



          <Modal isOpen={this.state.modal2} className={'modal-info ' + this.props.className} size=''>
            <ModalHeader><strong>Guardar cambios de telefono</strong></ModalHeader>
            <ModalBody>
              <Table size="sm" borderless>
                <tbody>
                  <tr>
                    <td align="right"><strong>C.I.:</strong></td>
                    <td>{this.state.gar.gar_ci + ' ' + this.state.gar.gar_ciorigen}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Nombre:</strong></td>
                    <td>{this.state.gar.gar_nombre}{' '}{this.state.gar.gar_apellido1}{' '}{this.state.gar.gar_apellido2}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Direccion:</strong></td>
                    <td>{this.state.gar.gar_direccion}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Telefono:</strong></td>
                    <td>{this.state.gar.gar_telefono}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Celular:</strong></td>
                    <td>{this.state.gar.gar_celular}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Correo:</strong></td>
                    <td>{this.state.gar.gar_correo}</td>
                  </tr>
                </tbody>
              </Table>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.handleOnSubmit}>Aceptar</Button>
              <Button color="secondary" onClick={this.modalsw2}>Cancelar</Button>
            </ModalFooter>
          </Modal>

          <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
            <ModalHeader>Mensaje</ModalHeader>
            <ModalBody>
              Datos Actualizado
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={this.modalsw3}>Cerrar</Button>
            </ModalFooter>
          </Modal>
        </Row>
      </div>
    )
  }
}

export default ActualizarTelefono;
