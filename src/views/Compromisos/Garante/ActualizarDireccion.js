import React, { Component } from 'react';
import { Button, Col, Input, Label, FormText, Modal, ModalBody, ModalFooter, ModalHeader, Row, Table, FormGroup, CardTitle } from 'reactstrap';
import 'jspdf-autotable'
import TablaGarante from '../Compromiso/TablaGarante';

class ActualizarDirreccion extends Component {
  constructor(props) {
    super(props)

    this.state = {
      gar: {},
      c6: false,
      c7: false,
      c8: false,
      c9: false,

      gar_ubicacion: '',
      gar_nom_ubicacion: '',
      gar_nro: '',
      gar_nom_edificio: '',
      gar_cod_dpto: '',
      gar_nom_zona: '',
      gar_direccion: '',
      gar_telefono: '',
      gar_celular: '',

      modal1: true,
      modal2: false,
      modal3: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.cancelarRegistro = this.cancelarRegistro.bind(this);
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    window.location.href = '#/vergarantes'
  }

  cancelarRegistro() {
    window.location.href = '#/vergarantes'
  }

  handleChange(e) {
    let gar = this.state.gar;
    gar[e.target.name] = e.target.value;
    this.setState({ gar });
  }

  handleChange1(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();
    try {
      fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.match.params.gar_ci + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(this.state.gar), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  async componentDidMount() {
    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.match.params.gar_ci + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ gar: json3 });
    } catch (error) {
      console.log(error);
    }
  }

  handleOnControll() {
    const { gar_ubicacion, gar_nom_ubicacion, gar_nro, gar_nom_zona, gar_nom_edificio, gar_cod_dpto } = this.state

    gar_ubicacion === ''
      ? (this.setState({ c6: true }))
      : (this.setState({ c6: false }))

    gar_nom_ubicacion === ''
      ? (this.setState({ c7: true }))
      : (this.setState({ c7: false }))

    gar_nro === ''
      ? (this.setState({ c8: true }))
      : (this.setState({ c8: false }))

    gar_nom_zona === ''
      ? (this.setState({ c9: true }))
      : (this.setState({ c9: false }))

    if (gar_ubicacion !== '' && gar_nom_ubicacion !== '' && gar_nro !== '' && gar_nom_zona !== '') {
      let gar = this.state.gar;
      gar['gar_direccion'] = gar_ubicacion + ': ' + gar_nom_ubicacion + ' N°: ' + gar_nro + ' ZONA: ' + gar_nom_zona + ' EDIFICIO: ' + gar_nom_edificio + ' DPTO: ' + gar_cod_dpto;
      this.setState({
        gar,
        modal2: !this.state.modal2,
      });
    }
  }



  render() {
    return (
      <div>
        <Row>
          <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='lg'>
            <ModalHeader>
              <CardTitle><strong><i className="icon-user-follow icons font-3xl"></i> Garante</strong></CardTitle>
            </ModalHeader>
            <ModalBody>
              <h5><strong>Datos personales del garante</strong></h5>
              <TablaGarante garanteid={this.props.match.params.gar_ci} />
              <hr />
              <form>
                <h5><strong>Actualizar Direccion del Garante</strong></h5>
                <FormGroup row>
                  <Col md="1"> </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Domicilio ubicado en</Label>
                      <Input type="select" name="gar_ubicacion" required valid value={this.state.gar_ubicacion} onChange={this.handleChange1}>
                        <option value=""></option>
                        <option value="CALLE">Calle</option>
                        <option value="AVENIDA">Avenida</option>
                      </Input>
                      {this.state.c6 && <FormText color="danger">Seleccione un campo</FormText>}
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Ubicacion</Label>
                      <Input type="text" id="gar_nom_ubicacion" name="gar_nom_ubicacion" required valid value={this.state.gar_nom_ubicacion} onChange={this.handleChange1} />
                      {this.state.c7 && <FormText color="danger">Se requiere ubicacion</FormText>}
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Nro.</Label>
                      <Input type="text" id="gar_nro" name="gar_nro" required valid value={this.state.gar_nro} onChange={this.handleChange1} />
                      {this.state.c8 && <FormText color="danger">Se requiere Nro.</FormText>}
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="1"> </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Edificio</Label>
                      <Input type="text" id="gar_nom_edificio" name="gar_nom_edificio" value={this.state.gar_nom_edificio} onChange={this.handleChange1} />
                      {/* <FormText color="dark">Nombre de edificio</FormText> */}
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Dpto.</Label>
                      <Input type="text" id="gar_cod_dpto" name="gar_cod_dpto" value={this.state.gar_cod_dpto} onChange={this.handleChange1} />
                      {/* <FormText color="dark">Nro. de Dpto.</FormText> */}
                    </FormGroup>
                  </Col>
                  <Col md="3">
                    <FormGroup>
                      <Label htmlFor="name">Zona</Label>
                      <Input type="text" id="gar_nom_zona" name="gar_nom_zona" required valid value={this.state.gar_nom_zona} onChange={this.handleChange1} />
                      {this.state.c9 && <FormText color="danger">Se requiere zona</FormText>}
                    </FormGroup>
                  </Col>
                </FormGroup>
              </form>
              <FormGroup row>
                <Col md="3"> </Col>
                <Col xs="12" md="3">
                  <FormGroup>
                    <Button className="btn-github btn-brand mr-1 mb-1" size="sm" type="submit" onClick={this.cancelarRegistro}><i className="fa icon-logout fa-sm"></i><span><strong>Cancelar Registro</strong></span></Button>
                  </FormGroup>
                </Col>
                <Col xs="12" md="3">
                  <FormGroup>
                    <Button className="btn-vimeo btn-brand mr-1 mb-1" size="sm" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guardar Cambios</strong></span></Button>
                  </FormGroup>
                </Col>
              </FormGroup>
            </ModalBody>
          </Modal>

          <Modal isOpen={this.state.modal2} className={'modal-info ' + this.props.className} size=''>
            <ModalHeader><strong>Guardar cambios de la direccion del garante</strong></ModalHeader>
            <ModalBody>
              <Table size="sm" borderless>
                <tbody>
                  <tr>
                    <td align="right"><strong>C.I.:</strong></td>
                    <td>{this.state.gar.gar_ci + ' ' + this.state.gar.gar_ciorigen}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Nombre:</strong></td>
                    <td>{this.state.gar.gar_nombre}{' '}{this.state.gar.gar_apellido1}{' '}{this.state.gar.gar_apellido2}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Direccion:</strong></td>
                    <td>{this.state.gar.gar_direccion}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Telefono:</strong></td>
                    <td>{this.state.gar.gar_telefono}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Celular:</strong></td>
                    <td>{this.state.gar.gar_celular}</td>
                  </tr>
                  <tr>
                    <td align="right"><strong>Correo:</strong></td>
                    <td>{this.state.gar.gar_correo}</td>
                  </tr>
                </tbody>
              </Table>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.handleOnSubmit}>Aceptar</Button>
              <Button color="secondary" onClick={this.modalsw2}>Cancelar</Button>
            </ModalFooter>
          </Modal>

          <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
            <ModalHeader>Mensaje</ModalHeader>
            <ModalBody>
              Datos Actualizado
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={this.modalsw3}>Cerrar</Button>
            </ModalFooter>
          </Modal>

        </Row>
      </div>
    )
  }
}

export default ActualizarDirreccion;
