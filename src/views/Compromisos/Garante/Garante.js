import React, { Component } from 'react';
import {
  Row, Col,
  Card, CardTitle, CardBody, CardFooter, CardHeader,
  FormGroup, FormText,
  Label,
  Input,
  Button, Modal, ModalHeader, ModalBody, ModalFooter,
  Table,
} from 'reactstrap'

const stylesIcono = { color: '#4682b4' }

class Garante extends Component {
  constructor(props) {
    super(props)
    this.state = {
      garante: {
        gar_ci: '',
        gar_ciorigen: '',
        gar_nombre: '',
        gar_apellido1: '',
        gar_apellido2: '',
        gar_genero: '',
        gar_direccion: '',
        gar_telefono: '',
        gar_celular: '',
        gar_correo: '',
      },
      gar_ubicacion: '',
      gar_nom_ubicacion: '',
      gar_nro: '',
      gar_nom_edificio: '',
      gar_cod_dpto: '',
      gar_nom_zona: '',

      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      c6: false,
      c7: false,
      c8: false,
      c9: false,
      sw: false,

      modal2: false,
      modal3: false,
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleChange1 = this.handleChange1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    window.location.href = '#/compromiso/'
    /* window.location.reload() */
  }


  handleOnControll() {
    const { gar_ci, gar_ciorigen, gar_nombre, gar_apellido1, gar_genero } = this.state.garante
    const { gar_ubicacion, gar_nom_ubicacion, gar_nro, gar_nom_zona, gar_nom_edificio, gar_cod_dpto } = this.state
    gar_ci === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    gar_ciorigen === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    gar_nombre === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    gar_apellido1 === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    gar_genero === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    gar_ubicacion === ''
      ? (this.setState({ c6: true }))
      : (this.setState({ c6: false }))

    gar_nom_ubicacion === ''
      ? (this.setState({ c7: true }))
      : (this.setState({ c7: false }))

    gar_nro === ''
      ? (this.setState({ c8: true }))
      : (this.setState({ c8: false }))

    gar_nom_zona === ''
      ? (this.setState({ c9: true }))
      : (this.setState({ c9: false }))

    if (gar_ci !== '' && gar_ciorigen !== '' && gar_nombre !== '' && gar_apellido1 !== '' && gar_genero !== '' && gar_nom_ubicacion !== '' && gar_nro !== '' && gar_nom_zona !== '') {
      let garante = this.state.garante;
      garante['gar_direccion'] = gar_ubicacion + ': ' + gar_nom_ubicacion + ' N°: ' + gar_nro + ' ZONA: ' + gar_nom_zona + ' EDIFICIO: ' + gar_nom_edificio + ' DPTO: ' + gar_cod_dpto;
      this.setState({
        garante,
        modal2: !this.state.modal2,
      });
    }
  }

  handleChange(e) {
    let garante = this.state.garante;
    garante[e.target.name] = e.target.value;
    this.setState({ garante });
  }

  handleChange1(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  /* handleChange = value => {
    this.setState({ garante:value });
    this.setState({show:true})
  }; */

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();
    try {
      fetch('http://0.0.0.0:8000/adeudos/garante/v1/listagarante', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.garante), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-user-plus fa-lg mt-1"></i> Garante</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <form>
                  <h5><strong style={stylesIcono}> Formulario de registro</strong></h5>
                  <hr />
                  <FormGroup row>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Cedula de Identidad *</Label>
                        <Input type="text" id="gar_ci" name="gar_ci" required valid value={this.state.gar_ci} onChange={this.handleChange} />
                        {this.state.c1 && <FormText color="danger">Se requiere C.I.</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Origen C.I. *</Label>
                        <Input type="select" id="gar_ciorigen" name="gar_ciorigen" required valid value={this.state.gar_ciorigen} onChange={this.handleChange}>
                          <option value=""></option>
                          <option value="LP">La Paz</option>
                          <option value="OR">Oruro</option>
                          <option value="PT">Potosi</option>
                          <option value="CB">Cochabamba</option>
                          <option value="CH">Chuquisaca</option>
                          <option value="TJ">Tarija</option>
                          <option value="SC">Santa Cruz</option>
                          <option value="BN">Beni</option>
                          <option value="PA">Pando</option>
                        </Input>
                        {this.state.c2 && <FormText color="danger">Seleccione campo</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Nombre *</Label>
                        <Input type="text" id="gar_nombre" name="gar_nombre" required valid value={this.state.gar_nombre} onChange={this.handleChange} />
                        {this.state.c3 && <FormText color="danger">Se requiere nombre</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Apellido 1 *</Label>
                        <Input type="text" id="gar_apellido1" name="gar_apellido1" required valid value={this.state.gar_apellido1} onChange={this.handleChange} />
                        {this.state.c4 && <FormText color="danger">Se requiere apellido</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Apellido 2</Label>
                        <Input type="text" id="gar_apellido2" name="gar_apellido2" value={this.state.gar_apellido2} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Genero *</Label>
                        <Input type="select" name="gar_genero" required valid value={this.state.gar_genero} onChange={this.handleChange}>
                          <option value=""></option>
                          <option value="F">Femenino</option>
                          <option value="M">Masculino</option>
                        </Input>
                        {this.state.c5 && <FormText color="danger">Seleccione un campo</FormText>}
                      </FormGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Domicilio ubicado en</Label>
                        <Input type="select" name="gar_ubicacion" required valid value={this.state.gar_ubicacion} onChange={this.handleChange1}>
                          <option value=""></option>
                          <option value="CALLE">Calle</option>
                          <option value="AVENIDA">Avenida</option>
                        </Input>
                        {this.state.c6 && <FormText color="danger">Seleccione un campo</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Ubicacion</Label>
                        <Input type="text" id="gar_nom_ubicacion" name="gar_nom_ubicacion" required valid value={this.state.gar_nom_ubicacion} onChange={this.handleChange1} />
                        {this.state.c7 && <FormText color="danger">Se requiere ubicacion</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Nro.</Label>
                        <Input type="text" id="gar_nro" name="gar_nro" required valid value={this.state.gar_nro} onChange={this.handleChange1} />
                        {this.state.c8 && <FormText color="danger">Se requiere Nro.</FormText>}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Edificio</Label>
                        <Input type="text" id="gar_nom_edificio" name="gar_nom_edificio" value={this.state.gar_nom_edificio} onChange={this.handleChange1} />
                        {/* <FormText color="dark">Nombre de edificio</FormText> */}
                      </FormGroup>
                    </Col>
                    <Col md="1">
                      <FormGroup>
                        <Label htmlFor="name">Dpto.</Label>
                        <Input type="text" id="gar_cod_dpto" name="gar_cod_dpto" value={this.state.gar_cod_dpto} onChange={this.handleChange1} />
                        {/* <FormText color="dark">Nro. de Dpto.</FormText> */}
                      </FormGroup>
                    </Col>
                    <Col md="3">
                      <FormGroup>
                        <Label htmlFor="name">Zona</Label>
                        <Input type="text" id="gar_nom_zona" name="gar_nom_zona" required valid value={this.state.gar_nom_zona} onChange={this.handleChange1} />
                        {this.state.c9 && <FormText color="danger">Se requiere zona</FormText>}
                      </FormGroup>
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Telefono</Label>
                        <Input type="text" id="gar_telefono" name="gar_telefono" value={this.state.gar_telefono} onChange={this.handleChange} />
                        {/* <FormText color="dark">Ingrese Nro. de telefono</FormText> */}
                      </FormGroup>
                    </Col>
                    <Col md="2">
                      <FormGroup>
                        <Label htmlFor="name">Celular</Label>
                        <Input type="text" id="gar_celular" name="gar_celular" value={this.state.gar_celular} onChange={this.handleChange} />
                        {/* <FormText color="dark">Ingrese Nro. de celular</FormText> */}
                      </FormGroup>
                    </Col>
                    <Col md="4">
                      <FormGroup>
                        <Label htmlFor="name">Correo</Label>
                        <Input type="text" id="gar_correo" name="gar_correo" value={this.state.gar_correo} onChange={this.handleChange} />
                        {/* <FormText color="dark">Ingrese correo electronico</FormText> */}
                      </FormGroup>
                    </Col>
                  </FormGroup>
                </form>
              </CardBody>

              <CardFooter>
                <FormGroup row>
                  <Col xs="12" md="4"></Col>
                  <Button className="btn-vine btn-brand mr-1 mb-1" size="lg" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Garante</strong></span></Button>

                  <Modal isOpen={this.state.modal2} className={'modal-info ' + this.props.className} size=''>
                    <ModalHeader><strong>Registrar nuevo garante</strong></ModalHeader>
                    <ModalBody>
                      <Table size="sm" borderless>
                        <tbody>
                          <tr>
                            <td align="right"><strong>C.I.:</strong></td>
                            <td>{this.state.garante.gar_ci + ' ' + this.state.garante.gar_ciorigen}</td>
                          </tr>
                          <tr>
                            <td align="right"><strong>Nombre:</strong></td>
                            <td>{this.state.garante.gar_nombre}{' '}{this.state.garante.gar_apellido1}{' '}{this.state.garante.gar_apellido2}</td>
                          </tr>
                          <tr>
                            <td align="right"><strong>Direccion:</strong></td>
                            <td>{this.state.garante.gar_direccion}</td>
                          </tr>
                          <tr>
                            <td align="right"><strong>Telefono:</strong></td>
                            <td>{this.state.garante.gar_telefono}</td>
                          </tr>
                          <tr>
                            <td align="right"><strong>Celular:</strong></td>
                            <td>{this.state.garante.gar_celular}</td>
                          </tr>
                        </tbody>
                      </Table>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="primary" onClick={this.handleOnSubmit}>Aceptar</Button>
                      <Button color="secondary" onClick={this.modalsw2}>Cancelar</Button>
                    </ModalFooter>
                  </Modal>

                  <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                    <ModalHeader>Mensaje</ModalHeader>
                    <ModalBody>
                      Garante registrado
                    </ModalBody>
                    <ModalFooter>
                      <Button color="success" onClick={this.modalsw3}>Cerrar</Button>
                    </ModalFooter>
                  </Modal>
                </FormGroup>
              </CardFooter>
            </Card>
          </Col>

        </Row>
      </div>
    );
  }
}

export default Garante;
