import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col, Row,
  Table,
  Label,
  Input,
  FormText, FormGroup,
  Button,
  InputGroup, InputGroupAddon, InputGroupText,
} from 'reactstrap';

class VerGarante extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listagar: [],
      valor: '',
    };
    this.buscarValor = this.buscarValor.bind(this)
  }


  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/listagarante', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ listagar: json1 });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    let items_filtrados = this.state.listagar.filter(
      (item) => {
        return (item.gar_ci + ' ' + item.gar_nombre + ' ' + item.gar_apellido1 + ' ' + item.gar_apellido2 + ' ' + item.gar_telefono)
          .toUpperCase().indexOf(
            this.state.valor.toUpperCase()) !== -1;
      }
    );

    let itemsGarante = items_filtrados.map((item, indice) => (
      <tr key={indice}>
        <td align="center">{indice + 1}</td>
        <td>{item.gar_ci + ' ' + item.gar_ciorigen}</td>
        <td>{item.gar_nombre + ' ' + item.gar_apellido1 + ' ' + item.gar_apellido2}</td>
        <td align="center">{item.gar_genero}</td>
        <td>{item.gar_direccion}</td>
        <td>{item.gar_telefono}</td>
        <td>{item.gar_celular}</td>
        <td>{item.gar_correo}</td>
        <td align="center"> <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" href={'#/actualizardireccion/' + item.gar_ci}><i className="fa fa-pencil-square fa-lg mt-0"></i></Button></td>
        <td align="center"> <Button size="sm" className="btn-linkedin btn-brand icon mr-1 mb-1" href={'#/actualizartelefono/' + item.gar_ci}><i className="fa fa-phone-square fa-lg mt-0"></i></Button></td>
      </tr>
    ))

    return (
      <div>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-list font-3xl"></i> Lista de Garantes</strong>
                  <div className="card-header-actions">
                    <InputGroup>
                      <Input type="text" value={this.state.valor} onChange={this.buscarValor} valid placeholder="Ci, nombre, apellido, tel..." />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-search fa-lg mt-1"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </div>
                </CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" lg="12">
                    <Table hover responsive size="sm">
                      <thead>
                        <tr>
                          <td align="center"><strong>Nro.</strong></td>
                          <td align="center"><strong>C.I.</strong></td>
                          <td align="center"><strong>Nombre</strong></td>
                          <td align="center"><strong>Genero</strong></td>
                          <td align="center"><strong>Direccion</strong></td>
                          <td align="center"><strong>Telefono</strong></td>
                          <td align="center"><strong>Celular</strong></td>
                          <td align="center"><strong>Correo</strong></td>
                          <td align="center"><strong></strong></td>
                          <td align="center"><strong></strong></td>
                        </tr>
                      </thead>
                      <tbody>
                        {itemsGarante}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default VerGarante;