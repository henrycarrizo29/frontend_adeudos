import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, CardImg, Col, Container, FormText, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Alert } from 'reactstrap';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      usernamex: '',
      passwordx: '',
      submitted: false,
      error: false,
      usuario: [],
      unidad: '',
      logged_in: localStorage.getItem('token') ? true : false,
    };
    this.handleChange = this.handleChange.bind(this)
    this.handleOnSubmit = this.handleOnSubmit.bind(this)
  }

  async componentDidMount() {
    try {
      if (this.state.logged_in) {
        const response = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json1 = await response.json();
        this.setState({ username: json1.username });
      }
    } catch (error) {
      console.log(error);
    }
  }

  handleOnSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true });
    const { usernamex, passwordx } = this.state;

    // stop here if form is invalid
    if (!(usernamex && passwordx)) {
      return;
    }
    let data = {
      username: usernamex,
      password: passwordx,
    }

    fetch('http://0.0.0.0:8000/token-auth/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
      .then(res => res.json())
      .then(json => {
        localStorage.setItem('token', json.token);
        // alert(json.token)
        if (typeof (json.token) === 'undefined') {
          // alert(json.token)
          this.setState({
            error: true,
            usernamex: '',
            passwordx: '',
          });
          localStorage.clear()
        } else {
          this.setState({
            logged_in: true,
            username: json.user.username
          });
          fetch('http://0.0.0.0:8000/adeudos/personal/v1/detalleusuario/' + json.user.id, {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            }
          })
            .then((response) => { return response.json() })
            .then((usuario) => { this.setState({ usuario: usuario }) })
            .then((res) => {
              fetch('http://0.0.0.0:8000/adeudos/personal/v1/detalleunidad/' + this.state.usuario[0].unidad, {
                headers: {
                  Authorization: `JWT ${localStorage.getItem('token')}`
                }
              })
                .then((response) => { return response.json() })
                .then((unidad) => { this.setState({ unidad: unidad[0].descripcion }) })
                .then((res) => {
                  switch (this.state.unidad) {
                    case 'EMERGENCIA':
                      window.location.href = '#/dashboard2/'
                      break;
                    case "CONTABILIDAD":
                      window.location.href = '#/dashboard1/'
                      break;
                    case "ENFERMERÍA":
                      window.location.href = '#/dashboard3/'
                      break;
                    default:
                      window.location.href = '#/dashboard4/'
                  }
                })
            })
        }
      });
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
    console.log(this.state.usernamex);
  }

  render() {
    const { usernamex, passwordx, submitted, error } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-1" style={{ width: '44%' }}>
                  <CardBody>

                    <h1>Iniciar Sesión</h1>
                    <p className="text-muted">Inicie sesión en su cuenta</p>

                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Usuario" autoComplete="usernamex" id="usernamex" name="usernamex" value={this.state.usernamex} onChange={this.handleChange} />
                      <InputGroup className="mb-3">
                        {submitted && !usernamex &&
                          <FormText color="danger">Se requiere nombre de usuario</FormText>
                        }
                      </InputGroup>
                    </InputGroup>

                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Contraseña" autoComplete="current-password" id="passwordx" name="passwordx" value={this.state.passwordx} onChange={this.handleChange} />
                      <InputGroup className="mb-3">
                        {submitted && !passwordx &&
                          <FormText color="danger">Se requiere contraseña</FormText>
                        }
                      </InputGroup>
                    </InputGroup>
                    <Row>
                      <Col xs="3"> </Col>
                      <Col xs="6">
                        <Button color="primary" type="submit" onClick={this.handleOnSubmit}>Iniciar Sesión</Button>
                      </Col>
                    </Row>
                    {error &&
                      <Alert color="danger">
                        Error:&nbsp;&nbsp;Usuario o contraseña incorrecta. Intente de nuevo.
                    </Alert>
                    }
                  </CardBody>
                </Card>
                {/*  <Card className=" py-1 d-md-down-none" style={{ width: 20 + '%' }} >
                  <CardBody className="text-center">
                    <div>
                      <img src={'assets/img/dashboard/img6.png'} alt="icon-promes" height="330" width="330" />
                    </div>
                  </CardBody>
                </Card> */}
                <Card className="text-white bg-primary py-1 d-md-down-none" style={{ width: 20 + '%' }}>
                  <CardBody className="text-center">
                    <CardImg top width="100%" src="assets/img/dashboard/icono_promes.png" alt="SI-GESHO-PROMES" />
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
