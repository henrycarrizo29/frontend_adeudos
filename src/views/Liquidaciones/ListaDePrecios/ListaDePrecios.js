import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle, CardSubtitle,
  Col, Row,
  Table,
  Label,
  Input,
  FormText, FormGroup,
  InputGroup, InputGroupAddon, InputGroupText,
} from 'reactstrap';
import AdicionarItem from './AdicionaItem'
import ActualizaPrecios from './ActualizaPrecios'
import EliminaPrecios from './EliminaPrecios'

const stylesIcono = { color: '#4682b4' }

class ListaDePrecios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listapre: [],
      valor: '',
    };
    this.buscarValor = this.buscarValor.bind(this)
    this.actualizaLista = this.actualizaLista.bind(this)
  }

  actualizaLista(){
    this.componentDidMount();
  }

  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listaprecio/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ listapre: json1 });
    } catch (e) {
      console.log(e);
    }
  }


  render() {
    let items_filtrados = this.state.listapre.filter(
      (item) => {
        return (item.lista_descripcion + ' ' + item.lista_costo)
          .toUpperCase().indexOf(
            this.state.valor.toUpperCase()) !== -1;
      }
    );

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-dollar fa-lg mt-1"></i> Costos de atencion medica</strong></CardTitle>
                <CardSubtitle>Lista Honorarios profecionales - Servicios prestados - Medicamentos
                  <div className="card-header-actions">
                    <AdicionarItem actualiza={this.actualizaLista}/>
                  </div>
                </CardSubtitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md={{ size: 4, offset: 8 }} className="text-center mt-1">
                    <FormGroup>
                      <InputGroup>
                        <Input type="text" value={this.state.valor} onChange={this.buscarValor} valid placeholder="Busqueda por descripcion y costo" />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-search fa-lg mt-1"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <Table hover responsive size="sm">
                      <thead>
                        <tr>
                          <td align="center"><strong>Nro.</strong></td>
                          <td><strong>Descripcion</strong></td>
                          <td align="right"><strong>Costo Unitario Bs.</strong></td>
                          <td align="center"><strong></strong></td>
                          <td align="center"><strong></strong></td>
                        </tr>
                      </thead>
                      <tbody>
                        {items_filtrados.map((item, indice) => (
                          <tr key={indice}>
                            <td align="center">{indice + 1}</td>
                            <td>{item.lista_descripcion}</td>
                            <td align="right">{item.lista_costo}</td>
                            <td align="center">
                              <ActualizaPrecios datosItem={item} actualizaLista={this.actualizaLista} />
                            </td>
                            <td align="center">
                              <EliminaPrecios datosItem={item} actualizaLista={this.actualizaLista}/>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default ListaDePrecios;