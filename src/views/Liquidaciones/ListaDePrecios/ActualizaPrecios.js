import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class ActualizaPrecios extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemPrecios: {
        lista_id: this.props.datosItem.lista_id,
        lista_descripcion: this.props.datosItem.lista_descripcion,
        lista_costo: this.props.datosItem.lista_costo,
        lista_fechareg: this.props.datosItem.lista_fechareg,
      },
      c1: false,
      c2: false,
      modal1: false,
      modal2: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.datosItem !== prevProps.datosItem) {
      this.setState({
        itemPrecios: this.props.datosItem
      })
    }
  }

  /* componentDidUpdate(prevProps) {
   if (this.props.f1 !== prevProps.f1 || this.props.f2 !== prevProps.f2) {
     var a = new Array(2).fill(false)
     this.setState({
       checked: a,
       estado: '',
     })
     this.componentDidMount();
   }
 } */


  handleOnControll() {
    const { lista_descripcion, lista_costo } = this.state.itemPrecios

    lista_descripcion === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    lista_costo === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (lista_descripcion !== '' && lista_costo !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/detallelistaprecio/' + this.props.datosItem.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(this.state.itemPrecios), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange(event) {
    let itPrecios = this.state.itemPrecios;
    itPrecios[event.target.name] = event.target.value;
    this.setState({ itPrecios });
    console.log(itPrecios);
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Actualizar Item
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" sm="8">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Item:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="lista_descripcion" name="lista_descripcion" required valid value={this.state.itemPrecios.lista_descripcion} onChange={this.handleChange} />
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese descripcion</FormText>}
                  </FormGroup>
                </Col>
                <Col xs="12" sm="4">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Costo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" id="lista_costo" name="lista_costo" required valid value={this.state.itemPrecios.lista_costo} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>Bs.</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese costo del item</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guardar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualizaLista(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>            
          </ModalFooter>
        </Modal>
      </div >
    );
  }
}
export default ActualizaPrecios;

