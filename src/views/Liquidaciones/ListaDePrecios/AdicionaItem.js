import React, { Component } from 'react';
import {
  Card, CardBody,
  Col, Row,
  Input,
  FormText, FormGroup,
  Button,
  InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

const stylesIcono = { color: '#4682b4' }

class ListaDePrecios extends Component {
  constructor(props) {
    super(props);

    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

    this.state = {
      itemPrecios: {
        lista_id: '',
        lista_descripcion: '',
        lista_costo: '',
        lista_fechareg: date,
      },
      c1: false,
      c2: false,
      modal1: false,
      modal2: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
    let inf1 = this.state.itemPrecios;
    let inf2 = this.state.itemPrecios;
    let inf3 = this.state.itemPrecios;
    inf1['lista_id'] = '';
    inf2['lista_descripcion'] = '';
    inf3['lista_costo'] = '';
    this.setState({ inf1, inf2, inf3 });
  }

  handleOnControll() {
    const { lista_descripcion, lista_costo } = this.state.itemPrecios

    lista_descripcion === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    lista_costo === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (lista_descripcion !== '' && lista_costo !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/hoja_evolucion/v1/listaprecio/', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.itemPrecios), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange(event) {
    let itPrecios = this.state.itemPrecios;
    itPrecios[event.target.name] = event.target.value.toUpperCase();
    this.setState({ itPrecios });
  }

  render() {

    return (
      <div>
        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-plus-square fa-sm"></i><span><strong>Adicionar item</strong></span></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Ingresar nuevo item
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" sm="8">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Item:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="lista_descripcion" name="lista_descripcion" required valid value={this.state.itemPrecios.lista_descripcion} onChange={this.handleChange} />
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese descripcion</FormText>}
                  </FormGroup>
                </Col>
                <Col xs="12" sm="4">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Costo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" id="lista_costo" name="lista_costo" required valid value={this.state.itemPrecios.lista_costo} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>Bs.</InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese costo del item</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Item registrado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default ListaDePrecios;