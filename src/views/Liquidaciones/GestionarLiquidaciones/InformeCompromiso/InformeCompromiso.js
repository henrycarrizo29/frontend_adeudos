import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Table,
} from 'reactstrap';
import DetalleInformes from './DetalleInformes'

const stylesIcono = { color: '#4682b4' }

class InformeCompromiso extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informeCDP: {
        inf_receptor: '',
        inf_receptorcargo: '',
        inf_emisor: '',
        inf_emisorcargo: '',
        inf_referencia: '',
        inf_fecha: '',
        inf_contenido: '',
        inf_cdp: this.props.cdpest[0].id,
      },
      informesReg: [],
      contenido: 'Mediante la presente, hago llegar a su autoridad la liquidacion por la internacion y atencion medica hospitalaria del/de la paciente XXXXXXX con cedula de identidad XXXXXXX, numero de compromiso de pago XXXXXXXX de fecha XXXXXXXX, se realiza el cobro al paciente ya que no cuenta con la afiliacion en la presente gestion. Como constancia del cobro correspondiente se adjunta a la presente, la liquidacion N° XXXXXXX , papeleta de deposito. Es en cuanto puedo informar a su autoridad.',
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      modal2: false,
      modal3: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.actualiza = this.actualiza.bind(this);
  }

  // modalsw2 para confirmar el registro 
  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  // modalsw3 confirmacion de registro 
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf1 = this.state.informeCDP;
    let inf2 = this.state.informeCDP;
    let inf3 = this.state.informeCDP;
    let inf4 = this.state.informeCDP;
    let inf5 = this.state.informeCDP;
    let inf6 = this.state.informeCDP;
    let inf7 = this.state.informeCDP;
    let inf8 = this.state.informeCDP;
    inf1['inf_receptor'] = '';
    inf2['inf_emisor'] = '';
    inf3['inf_referencia'] = '';
    inf4['inf_fecha'] = '';
    inf5['inf_contenido'] = '';
    inf6['inf_cdp'] = this.props.cdpest[0].id;
    inf7['inf_emisorcargo'] = '';
    inf8['inf_receptorcargo'] = '';
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8 });
  }


  async actualiza() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/informe_cdp/v1/detalleinformebusca/' + this.props.cdpest[0].id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ informesReg: json1 });
    } catch (error) {
      console.log(error);
    }
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/informe_cdp/v1/detalleinformebusca/' + this.props.cdpest[0].id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ informesReg: json1 });
    } catch (error) {
      console.log(error);
    }
  }

  handleOnControll() {
    const { inf_receptor, inf_emisor, inf_referencia, inf_fecha, inf_contenido } = this.state.informeCDP

    inf_receptor === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    inf_emisor === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    inf_referencia === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    inf_fecha === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    inf_contenido === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    if (inf_receptor !== '' && inf_emisor !== '' && inf_referencia !== '' && inf_fecha !== '' && inf_contenido !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  handleChange(event) {
    let infCDP = this.state.informeCDP;
    infCDP[event.target.name] = event.target.value;
    this.setState({ infCDP });
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    try {
      fetch('http://0.0.0.0:8000/adeudos/informe_cdp/v1/listainforme', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.informeCDP), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        )
        .then(() =>
          this.componentDidMount()
        );
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="9">
                    <CardTitle><strong><i className="fa fa-file fa-lg mt-1"></i> Informe Compromiso de Pago</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="5"> </Col>
                  <Col md="5">
                    <strong>INFORME N° </strong>{this.props.cdpest[0].id}
                  </Col>
                </FormGroup>
                <hr />
                <Row>
                  <Col xs="12" sm="5">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>A:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="inf_receptor" name="inf_receptor" required valid value={this.state.informeCDP.inf_receptor} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Ingrese nombre del receptor</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Cargo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="inf_receptorcargo" name="inf_receptorcargo" value={this.state.informeCDP.inf_receptorcargo} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>De:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="inf_emisor" name="inf_emisor" required valid value={this.state.informeCDP.inf_emisor} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese nombre del emisor</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Cargo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="inf_emisorcargo" name="inf_emisorcargo" value={this.state.informeCDP.inf_emisorcargo} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Ref:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="inf_referencia" name="inf_referencia" required valid value={this.state.informeCDP.inf_referencia} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-pencil-square-o"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese referencia</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="inf_fecha" name="inf_fecha" required valid value={this.state.informeCDP.inf_fecha} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c4 && <FormText color="danger">Ingrese fecha</FormText>}
                    </FormGroup>
                    <FormGroup row>
                      <Col xs="12" md="2"></Col>
                      <Col xs="12" md="10">
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar informe</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="7">
                    <FormGroup>
                      <Label htmlFor="text-input">Contenido</Label>
                      <Input type="textarea" rows="12" id="inf_contenido" name="inf_contenido" required valid value={this.state.informeCDP.inf_contenido} onChange={this.handleChange} />
                      {this.state.c5 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h4><strong><code style={stylesIcono}>Informes registrados</code></strong></h4>
                    <DetalleInformes informes={this.state.informesReg} actualiza={this.actualiza} />
                  </Col>
                </FormGroup>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    Datos del informe
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col xs="12" md="12">
                        <strong>A: </strong> {this.state.informeCDP.inf_receptor}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>De: </strong> {this.state.informeCDP.inf_emisor}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Ref: </strong> {this.state.informeCDP.inf_referencia}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Fecha: </strong> {this.state.informeCDP.inf_fecha}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Contenido: </strong> {this.state.informeCDP.inf_contenido}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Datos de informe registrado
                    </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InformeCompromiso;