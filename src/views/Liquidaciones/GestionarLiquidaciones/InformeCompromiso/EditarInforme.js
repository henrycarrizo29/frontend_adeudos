import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class EditarInforme extends Component {
  constructor(props) {
    super(props);

    this.state = {
      informeCDP: {
        inf_receptor: this.props.informe.inf_receptor,
        inf_receptorcargo: this.props.informe.inf_receptorcargo,
        inf_emisor: this.props.informe.inf_emisor,
        inf_emisorcargo: this.props.informe.inf_emisorcargo,
        inf_referencia: this.props.informe.inf_referencia,
        inf_fecha: this.props.informe.inf_fecha,
        inf_contenido: this.props.informe.inf_contenido,
        inf_cdp: this.props.informe.inf_cdp,
      },
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      modal1: false,
      modal2: false,
    }
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.informe !== prevProps.informe) {
      this.setState({
        informeCDP: this.props.informe
      })
    }
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleOnControll() {
    const { inf_receptor, inf_emisor, inf_referencia, inf_fecha, inf_contenido } = this.state.informeCDP

    inf_receptor === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    inf_emisor === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    inf_referencia === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    inf_fecha === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    inf_contenido === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    if (inf_receptor !== '' && inf_emisor !== '' && inf_referencia !== '' && inf_fecha !== '' && inf_contenido !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/informe_cdp/v1/detalleinforme/' + this.props.informe.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(this.state.informeCDP), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange(event) {
    let infCDP = this.state.informeCDP;
    infCDP[event.target.name] = event.target.value;
    this.setState({ infCDP });
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Informe Compromiso de Pago
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <FormGroup row>
                <Col md="5"> </Col>
                <Col md="5">
                  <strong>INFORME N° </strong>{this.props.informe.inf_cdp}
                </Col>
              </FormGroup>
              <hr />
              <Row>
                <Col xs="12" sm="5">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>A:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="inf_receptor" name="inf_receptor" required valid value={this.state.informeCDP.inf_receptor} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese nombre del receptor</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Cargo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="inf_receptorcargo" name="inf_receptorcargo" value={this.state.informeCDP.inf_receptorcargo} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>De:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="inf_emisor" name="inf_emisor" required valid value={this.state.informeCDP.inf_emisor} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese nombre del emisor</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Cargo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="inf_emisorcargo" name="inf_emisorcargo" value={this.state.informeCDP.inf_emisorcargo} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Ref:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="inf_referencia" name="inf_referencia" required valid value={this.state.informeCDP.inf_referencia} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-pencil-square-o"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c3 && <FormText color="danger">Ingrese referencia</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="inf_fecha" name="inf_fecha" required valid value={this.state.informeCDP.inf_fecha} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c4 && <FormText color="danger">Ingrese fecha</FormText>}
                  </FormGroup>
                </Col>
                <Col xs="12" sm="7">
                  <FormGroup>
                    <Label htmlFor="text-input">Contenido</Label>
                    <Input type="textarea" rows="12" id="inf_contenido" name="inf_contenido" required valid value={this.state.informeCDP.inf_contenido} onChange={this.handleChange} />
                    {this.state.c5 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default EditarInforme;

