import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col,
  FormGroup,
  Table,
  Button,
  Popover, PopoverHeader, PopoverBody
} from 'reactstrap';
import PDFinforme from './PDFinforme';
import EditarInforme from './EditarInforme';

class PopoverItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popoverOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen,
    });
  }

  render() {
    return (
      <span>
        <Button size="sm" className="btn-vine btn-brand icon mr-1 mb-1" id={'Popover-' + this.props.id} onClick={this.toggle}><i className="fa fa-file-text fa-lg mt-0"></i></Button>
        <Popover placement="left" isOpen={this.state.popoverOpen} target={'Popover-' + this.props.id} toggle={this.toggle}>
          <PopoverHeader>Contenido</PopoverHeader>
          <PopoverBody>{this.props.cont}</PopoverBody>
        </Popover>
      </span>
    );
  }
}




class DetalleInformes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informes: this.props.informes
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.informes !== prevProps.informes) {
      this.setState({
        informes: this.props.informes
      })
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro</strong></td>
                  <td align="left"><strong>Remitente</strong></td>
                  <td align="left"><strong>Cargo</strong></td>
                  <td align="left"><strong>Emisor</strong></td>
                  <td align="left"><strong>Cargo</strong></td>
                  <td align="left"><strong>Referencia</strong></td>
                  <td align="center"><strong>FechaInforme</strong></td>
                  <td align="center"><strong>Contenido</strong></td>
                  <td></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.state.informes.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{i + 1}</td>
                      <td align="left">{u.inf_receptor}</td>
                      <td align="left">{u.inf_receptorcargo}</td>
                      <td align="left">{u.inf_emisor}</td>
                      <td align="left">{u.inf_emisorcargo}</td>
                      <td align="left">{u.inf_referencia}</td>
                      <td align="center">{u.inf_fecha}</td>
                      <td align="center">
                        <PopoverItem cont={u.inf_contenido} id={i} />
                      </td>
                      <td>
                        <PDFinforme idinforme={u.id} />
                      </td>
                      <td>
                        <EditarInforme informe={u} actualiza={this.props.actualiza} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleInformes;

