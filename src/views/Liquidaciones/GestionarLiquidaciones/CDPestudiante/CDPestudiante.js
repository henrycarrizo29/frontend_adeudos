import React from 'react';
import DescripcionCompromiso from '../../../Compromisos/DescripcionCompromiso/DescripcionCompromiso';

class CDPestudiante extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {

    return (
      <div>
        {this.props.cdpest.map((detalleCompromiso, i) => (
          <DescripcionCompromiso datoscom={detalleCompromiso} key={i} />
        ))}
      </div>
    );
  }
}

export default CDPestudiante;