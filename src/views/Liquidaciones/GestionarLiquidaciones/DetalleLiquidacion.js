import React, { Component } from 'react';
import {
  Col, Row,
  TabContent, TabPane,
  Nav, NavItem, NavLink,
  Card, CardBody, CardHeader, CardTitle,
  FormGroup,
  Badge,
} from 'reactstrap';
import classnames from 'classnames';
import InformeSocial from './InformeSocial/InformeSocial'
import LiquidacionPaciente from './LiquidacionPaciente/LiquidacionPaciente'
import DepositosCancelados from './DepositosCancelados/DepositosCancelados'
import InformeCompromiso from './InformeCompromiso/InformeCompromiso'
import CDPestudiante from './CDPestudiante/CDPestudiante';

const stylesCompromisos = { color: '#20a8d8' }
const stylesLiquidaciones = { color: '#ff7f50' }
const stylesDepositos = { color: '#4dbd74' }
const stylesInformeSocial = { color: '#e83e8c' }
const stylesInformeCompromiso = { color: '#63c2de' }

class DetalleLiquidacion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '1',
    };

    this.cambiarTabla = this.cambiarTabla.bind(this);
  }

  cambiarTabla(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="11">
                    <CardTitle><strong><i className="icon-doc icons font-3xl"></i> Liquidacion compromiso de pago</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12" className="mb-4">
                    <Nav tabs>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '1' })} onClick={() => { this.cambiarTabla('1'); }}>
                          <span className={this.state.activeTab === '1' ? '' : 'd-none'}></span> {'\u00A0'}<i className="fa fa-file-text fa-lg mt-1" style={stylesCompromisos}></i> Compromisos
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '2' })} onClick={() => { this.cambiarTabla('2'); }} >
                          <span className={this.state.activeTab === '2' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-list-alt fa-lg mt-1" style={stylesLiquidaciones}></i> Liquidaciones
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.cambiarTabla('3'); }} >
                          <span className={this.state.activeTab === '3' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-dollar fa-lg mt-1" style={stylesDepositos}></i> Comprobantes
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.cambiarTabla('4'); }} >
                          <span className={this.state.activeTab === '4' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-file-o fa-lg mt-1" style={stylesInformeSocial}></i> Informe Social
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.cambiarTabla('5'); }} >
                          <span className={this.state.activeTab === '5' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-file fa-lg mt-1" style={stylesInformeCompromiso}></i> Informe Compromiso
                        </NavLink>
                      </NavItem>
                    </Nav>

                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="1">
                        <CDPestudiante cdpest={this.props.cdpest} />
                      </TabPane>
                      <TabPane tabId="2">
                        <LiquidacionPaciente cdpest={this.props.cdpest} />
                      </TabPane>
                      <TabPane tabId="3">
                        <DepositosCancelados cdpest={this.props.cdpest} />
                      </TabPane>
                      <TabPane tabId="4">
                        <InformeSocial cdpest={this.props.cdpest} />
                      </TabPane>
                      <TabPane tabId="5">
                        <InformeCompromiso cdpest={this.props.cdpest} />
                      </TabPane>
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DetalleLiquidacion;
