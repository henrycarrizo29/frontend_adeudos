import React from 'react';
import LiquidacionDetalle from './LiquidacionDetalle';

class LiquidacionPaciente extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <div>
        {this.props.cdpest.map((detalleCompromiso, i) => (
          <LiquidacionDetalle datoscom={detalleCompromiso} key={i} />
        ))}
      </div>
    );
  }
}

export default LiquidacionPaciente;