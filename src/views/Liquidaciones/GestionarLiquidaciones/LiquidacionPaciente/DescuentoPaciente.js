import React from 'react';
import {
  Table,
  Alert,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class LiquidacionDetalle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      informesSociales: [],
      modal1: false,
    };
    this.modalsw1 = this.modalsw1.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
    this.componentDidMount()
  }

 /*  componentDidUpdate(prevProps) {
    if (this.props.idCDP !== prevProps.idCDP) {
      this.setState({
        informeSocial: this.props.informe
      })
    }
  } */

  async componentDidMount() {
    try {
      const respuesta9 = await fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/informesocialbusca/' + this.props.idCDP + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json9 = await respuesta9.json();
      this.setState({
        informesSociales: json9,
      });
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button color="ghost-primary" onClick={this.modalsw1}>Descuento <i className="fa fa-plus-square"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size=''>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Descuentos - Informe Social
          </ModalHeader>
          <ModalBody>
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro</strong></td>
                  <td align="left"><strong>Codigo_Inf.</strong></td>
                  <td align="left"><strong>Fecha_Inf.</strong></td>
                  <td align="center"><strong>Descuento</strong></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.state.informesSociales.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{i + 1}</td>
                      <td align="left">{u.is_codigo}</td>
                      <td align="left">{u.is_fecha}</td>
                      <td align="center">{u.is_descuento + ' %'}</td>
                      <td align="center">
                        {/* <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.handleDescuento(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button> */}
                        <Button color="success" size="sm" className="btn-pill" onClick={(event) => { this.props.handleDescuento(u.is_descuento); this.modalsw1(); }} >Adicionar</Button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" size="sm" className="btn-pill" onClick={(event) => { this.props.handleDescuento(0); this.modalsw1(); }} >Sin Descuento</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default LiquidacionDetalle;