import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col,
  FormGroup,
  Table,
  Button,
} from 'reactstrap';

class LiquidacionesGuardadas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      liq_cdp: '',
      liq_total: '',
      liq_descuento: '',
      liq_total_descuento: '',
      swB: false,
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.liquidacionVec !== prevProps.liquidacionVec) {
      if (this.props.liquidacionVec.length !== 0) {
        this.setState({
          liq_cdp: this.props.liquidacionVec[0].liq_cdp,
          liq_total: this.props.liquidacionVec[0].liq_total,
          liq_descuento: this.props.liquidacionVec[0].liq_descuento,
          liq_total_descuento: this.props.liquidacionVec[0].liq_total_descuento,
          swB: true,
        });
      }
    }
  }

  async componentDidMount() {
    if (this.props.liquidacionVec.length !== 0) {
      this.setState({
        liq_cdp: this.props.liquidacionVec[0].liq_cdp,
        liq_total: this.props.liquidacionVec[0].liq_total,
        liq_descuento: this.props.liquidacionVec[0].liq_descuento,
        liq_total_descuento: this.props.liquidacionVec[0].liq_total_descuento,
        swB: true,
      });
    }
  }

  render() {
    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro. CDP</strong></td>
                  <td align="center"><strong>Subtotal ( Bs. )</strong></td>
                  <td align="center"><strong>Descuento {'( ' + this.state.liq_descuento + ' % )'}</strong></td>
                  <td align="center"><strong>Total ( Bs. )</strong></td>
                  <td align="center"><strong>PDF</strong></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td align="center">{this.state.liq_cdp}</td>
                  <td align="center">{this.state.liq_total}</td>
                  <td align="center">{this.state.liq_descuento === '' ? this.state.liq_total : ((this.state.liq_total) * (this.state.liq_descuento / 100)).toFixed(2)}</td>
                  <td align="center">{this.state.liq_total_descuento}</td>
                  <td align="center">
                    {this.state.swB && <Button size="sm" className="btn-linkedin btn-brand icon mr-1 mb-1" onClick={this.props.pdfLiquidacion}><i className="fa fa-file-pdf-o fa-lg mt-0"></i></Button>}
                  </td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default LiquidacionesGuardadas;

