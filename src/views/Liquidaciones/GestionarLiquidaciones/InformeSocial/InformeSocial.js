import React from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup, FormText,
  Label,
  Input, InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
import DetalleInformeSocial from './DetalleInformeSocial'

const stylesIcono = { color: '#4682b4' }

class InformeSocial extends React.Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.state = {
      informeSocial: {
        is_codigo: '',
        is_fecha: '',
        is_contenido: null,
        is_descuento: 0,
        is_aceptado: false,
        is_observaciones: '',
        is_cdp: this.props.cdpest[0].id,
      },
      informesSocialesRegistrados: [],
      fecha: date,
      c1: false,
      c2: false,
      c3: false,
      modal2: false,
      modal3: false,
      filePDF: null,
      file: null,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangePDF = this.handleChangePDF.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.actualiza = this.actualiza.bind(this);
  }

  actualiza() {
    this.componentDidMount()
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf1 = this.state.informeSocial;
    let inf2 = this.state.informeSocial;
    let inf3 = this.state.informeSocial;
    let inf4 = this.state.informeSocial;
    let inf5 = this.state.informeSocial;
    let inf6 = this.state.informeSocial;
    let inf7 = this.state.informeSocial;
    inf1['is_fecha'] = '';
    inf2['is_descuento'] = 0;
    inf3['is_codigo'] = '';
    inf4['is_contenido'] = null;
    inf5['is_aceptado'] = false;
    inf6['is_cdp'] = this.props.cdpest[0].id;
    inf7['is_observaciones'] = '';
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6, inf7 });
    this.setState({
      filePDF: null,
      file: null,
    });
  }

  handleChange(event) {
    let info = this.state.informeSocial;
    info[event.target.name] = event.target.value;
    this.setState({ info });
  }

  handleOnControll() {
    const { is_fecha, is_descuento, is_codigo } = this.state.informeSocial

    is_fecha === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    is_descuento === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    is_codigo === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    if (is_fecha !== '' && is_descuento !== '' && is_codigo !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/informesocialbusca/' + this.props.cdpest[0].id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ informesSocialesRegistrados: json1 });
    } catch (error) {
      console.log(error);
    }
  }


  handleChangePDF(event) {
    console.log("pdf:", event)
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      filePDF: event.target.files[0]
    })
    let pdff = event.target.files[0]
  }


  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    // e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    const data = new FormData()
    data.append('is_fecha', this.state.informeSocial.is_fecha);
    data.append('is_descuento', this.state.informeSocial.is_descuento);
    data.append('is_codigo', this.state.informeSocial.is_codigo);
    data.append('is_contenido', this.state.filePDF);
    data.append('is_aceptado', this.state.informeSocial.is_aceptado);
    data.append('is_observaciones', this.state.informeSocial.is_observaciones);
    data.append('is_cdp', this.state.informeSocial.is_cdp);

    if (this.state.filePDF !== null) {
      try {
        fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/listainformesocial/', {
          method: 'POST', // or 'PUT'
          body: data, // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          )
          .then(() =>
            this.componentDidMount()
          );
      } catch (e) {
        console.log(e);
      }

    } else {
      try {
        fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/listainformesocial/', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.informeSocial), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          )
          .then(() =>
            this.componentDidMount()
          );
      } catch (e) {
        console.log(e);
      }
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="9">
                    <CardTitle><strong><i className="fa fa-file-o fa-lg mt-1"></i> Informe Social</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <h4><strong><code style={stylesIcono}>Datos Informe Social</code></strong></h4>
                <hr />
                <Row>
                  <Col xs="12" sm="6">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha Informe:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="is_fecha" name="is_fecha" value={this.state.informeSocial.is_fecha} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Ingrese fecha de informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Codigo Informe:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="is_codigo" name="is_codigo" value={this.state.informeSocial.is_codigo} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-barcode"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese Codigo de Informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Descuento Inf.:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="number" id="is_descuento" name="is_descuento" value={this.state.informeSocial.is_descuento} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-percent"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese descuento (0-100)</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Observaciones:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="is_observaciones" name="is_observaciones" value={this.state.informeSocial.is_observaciones} onChange={this.handleChange} />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <Input type="file" onChange={this.handleChangePDF} />
                      </InputGroup>
                      <FormText color="dark">Cargar PDF</FormText>
                    </FormGroup>
                    <FormGroup row>
                      <Col xs="12" md="3"></Col>
                      <Col xs="12" md="10">
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Informe</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="6">
                    {/* <FormGroup>
                      <a href={`http://0.0.0.0:8000` +this.state.file} target="_blank">Download Pdf</a>
                      <img src={this.state.file} className="img-responsive" width="450" height="280" />
                      <br />
                    </FormGroup> */}
                  </Col>
                </Row>
                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h4><strong><code style={stylesIcono}>Informes Sociales registrados</code></strong></h4>
                    <DetalleInformeSocial informes={this.state.informesSocialesRegistrados} actualiza={this.actualiza} />
                  </Col>
                </FormGroup>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    <i className="fa fa-file-o fa-lg mt-1"></i> Datos Informe Social
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col xs="12" md="12">
                        <strong>Fecha de Informe: </strong> {this.state.informeSocial.is_fecha}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Codigo Informe: </strong> {this.state.informeSocial.is_codigo}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Descuento Inf.: </strong> {this.state.informeSocial.is_descuento}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Observaciones </strong> {this.state.informeSocial.is_observaciones}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Informe Social Registrado
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InformeSocial;