import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col,
  FormGroup,
  Table,
} from 'reactstrap';
import EditarInformeSocial from './EditarInformeSocial';


class DetalleInformeSocial extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informes: this.props.informes
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.informes !== prevProps.informes) {
      this.setState({
        informes: this.props.informes
      })
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro</strong></td>
                  <td align="left"><strong>Codigo_Inf.</strong></td>
                  <td align="left"><strong>Fecha_Inf.</strong></td>
                  <td align="center"><strong>Descuento</strong></td>
                  <td align="left"><strong>Observaciones</strong></td>
                  <td align="center"><strong>PDF</strong></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.state.informes.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{i + 1}</td>
                      <td align="left">{u.is_codigo}</td>
                      <td align="left">{u.is_fecha}</td>
                      <td align="center">{u.is_descuento + ' %'}</td>
                      <td align="left">{u.is_observaciones}</td>
                      <td align="center">
                        <a href={'http://0.0.0.0:8000' + u.is_contenido} target="_blank"><i className="fa fa-file-pdf-o fa-lg mt-0"></i></a>
                      </td>
                      <td align="center">
                        <EditarInformeSocial informe={u} actualiza={this.props.actualiza} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleInformeSocial;

