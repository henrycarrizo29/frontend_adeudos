import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class EditarInformeSocial extends Component {
  constructor(props) {
    super(props);

    this.state = {
      informeSocial: {
        is_codigo: this.props.informe.is_codigo,
        is_fecha: this.props.informe.is_fecha,
        is_contenido: this.props.informe.is_contenido,
        is_descuento: this.props.informe.is_descuento,
        is_aceptado: this.props.informe.is_aceptado,
        is_observaciones: this.props.informe.is_observaciones,
        is_cdp: this.props.informe.is_cdp
      },
      c1: false,
      c2: false,
      c3: false,
      modal1: false,
      modal2: false,
      filePDF: null,
      file: null,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleChangePDF = this.handleChangePDF.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.informe !== prevProps.informe) {
      this.setState({
        informeSocial: this.props.informe
      })
    }
  }

  handleChangePDF(event) {
    let info = this.state.informeSocial;
    info['is_contenido'] = event.target.files[0];
    this.setState({ info });
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      filePDF: event.target.files[0],
    })
    let pdff = event.target.files[0]
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleOnControll() {
    const { is_fecha, is_descuento, is_codigo } = this.state.informeSocial
    let ai = 0
    is_fecha === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    is_descuento === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    is_codigo === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    if (is_fecha !== '' && is_descuento !== '' && is_codigo !== '') {
      const data = new FormData()
      data.append('is_fecha', this.state.informeSocial.is_fecha);
      data.append('is_descuento', this.state.informeSocial.is_descuento);
      data.append('is_codigo', this.state.informeSocial.is_codigo);
      this.state.filePDF !== null
        ? data.append('is_contenido', this.state.filePDF)
        : (ai = ai + 1)
      data.append('is_aceptado', this.state.informeSocial.is_aceptado);
      data.append('is_observaciones', this.state.informeSocial.is_observaciones);
      data.append('is_cdp', this.state.informeSocial.is_cdp);

      const datosInfSoc = {
        is_codigo: this.state.informeSocial.is_codigo,
        is_fecha: this.state.informeSocial.is_fecha,
        is_descuento: this.state.informeSocial.is_descuento,
        is_aceptado: this.state.informeSocial.is_aceptado,
        is_observaciones: this.state.informeSocial.is_observaciones,
        is_cdp: this.state.informeSocial.is_cdp
      }

      if (this.state.filePDF !== null) {
        try {
          fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/detalleinformesocial/' + this.props.informe.id + '/', {
            method: 'PUT', // or 'PUT'
            body: data, // data can be string or {object}!
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
              })
            )
        } catch (e) {
          console.log(e);
        }

      } else {
        try {
          fetch('http://0.0.0.0:8000/adeudos/informe_social_cdp/v1/detalleinformesocial/' + this.props.informe.id + '/', {
            method: 'PUT', // or 'PUT'
            body: JSON.stringify(datosInfSoc), // data can be string or {object}!
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
              'Content-Type': 'application/json'
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
              })
            )
        } catch (e) {
          console.log(e);
        }
      }
    }
  }

  handleChange(event) {
    let info = this.state.informeSocial;
    info[event.target.name] = event.target.value;
    this.setState({ info });
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size=''>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Informe Social
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" sm="12">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha Informe:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="is_fecha" name="is_fecha" value={this.state.informeSocial.is_fecha} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese fecha de informe</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Codigo Informe:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="is_codigo" name="is_codigo" value={this.state.informeSocial.is_codigo} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-barcode"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c3 && <FormText color="danger">Ingrese Codigo de Informe</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Descuento Inf.:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" id="is_descuento" name="is_descuento" value={this.state.informeSocial.is_descuento} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-percent"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese descuento (0-100)</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Observaciones:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="is_observaciones" name="is_observaciones" value={this.state.informeSocial.is_observaciones} onChange={this.handleChange} />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <Input type="file" onChange={this.handleChangePDF} />
                    </InputGroup>
                    <FormText color="dark">Cargar PDF</FormText>
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default EditarInformeSocial;

