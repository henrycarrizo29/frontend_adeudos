import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class EditarComprobante extends Component {
  constructor(props) {
    super(props);

    this.state = {
      depositosAcuenta: {
        dep_fecha_boleta: this.props.comprobante.dep_fecha_boleta,
        dep_monto_total: this.props.comprobante.dep_monto_total,
        dep_numero: this.props.comprobante.dep_numero,
        dep_img_boleta: this.props.comprobante.dep_img_boleta,
        dep_fecha_registro: this.props.comprobante.dep_fecha_registro,
        dep_cdp: this.props.comprobante.dep_cdp,
      },
      imagen: null,
      file: null,

      c1: false,
      c2: false,
      c3: false,
      f1: true,
      f2: false,
      no: false,
      modal1: false,
      modal2: false,
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFoto = this.handleChangeFoto.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.comprobante !== prevProps.comprobante) {
      this.setState({
        depositosAcuenta: this.props.comprobante
      })
    }
  }

  handleChangeFoto(event) {
    let depo = this.state.depositosAcuenta;
    depo['dep_img_boleta'] = event.target.files[0];
    this.setState({ depo });

    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      imagen: event.target.files[0],
      f1: false,
      f2: true,
    })
    let img = event.target.files[0]
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleOnControll() {
    const { dep_fecha_boleta, dep_monto_total, dep_numero } = this.state.depositosAcuenta
    let ai = 0
    dep_fecha_boleta === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    dep_monto_total === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    dep_numero === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    if (dep_fecha_boleta !== '' && dep_monto_total !== '' && dep_numero !== '') {
      const data = new FormData()
      data.append('dep_fecha_boleta', this.state.depositosAcuenta.dep_fecha_boleta);
      data.append('dep_monto_total', this.state.depositosAcuenta.dep_monto_total);
      data.append('dep_numero', this.state.depositosAcuenta.dep_numero);
      this.state.file !== null
        ? data.append('dep_img_boleta', this.state.imagen)
        : (ai = ai + 1)
      data.append('dep_fecha_registro', this.state.depositosAcuenta.dep_fecha_registro);
      data.append('dep_cdp', this.state.depositosAcuenta.dep_cdp);

      const datosComprobante = {
        dep_fecha_boleta: this.state.depositosAcuenta.dep_fecha_boleta,
        dep_monto_total: this.state.depositosAcuenta.dep_monto_total,
        dep_numero: this.state.depositosAcuenta.dep_numero,
        dep_fecha_registro: this.state.depositosAcuenta.dep_fecha_registro,
        dep_cdp: this.state.depositosAcuenta.dep_cdp,
      }

      if (this.state.file !== null) {
        try {
          fetch('http://0.0.0.0:8000/adeudos/depositos_cdp/v1/detalledepositos/' + this.props.comprobante.id + '/', {
            method: 'PUT', // or 'PUT'
            body: data, // data can be string or {object}!   body: JSON.stringify(cdpCancelado),             
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
                f1: true,
                f2: false,
              })
            );
        } catch (e) {
          console.log(e);
        }
      } else {
        try {
          fetch('http://0.0.0.0:8000/adeudos/depositos_cdp/v1/detalledepositos/' + this.props.comprobante.id + '/', {
            method: 'PUT', // or 'PUT'
            body: JSON.stringify(datosComprobante),
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
              'Content-Type': 'application/json'
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
                f1: true,
                f2: false,
              })
            );
        } catch (e) {
          console.log(e);
        }
      }
    }
  }

  handleChange(event) {
    let depo = this.state.depositosAcuenta;
    depo[event.target.name] = event.target.value;
    this.setState({ depo });
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Editar Comprobante
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" md="6">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha Deposito:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="dep_fecha_boleta" name="dep_fecha_boleta" value={this.state.depositosAcuenta.dep_fecha_boleta} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese fecha de deposito</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Total Bolivianos:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" id="dep_monto_total" name="dep_monto_total" value={this.state.depositosAcuenta.dep_monto_total} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-money"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese monto total cancelado</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Codigo Deposito:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="dep_numero" name="dep_numero" value={this.state.depositosAcuenta.dep_numero} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-barcode"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c3 && <FormText color="danger">Ingrese codigo de deposito</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <Input type="file" onChange={this.handleChangeFoto} />
                    </InputGroup>
                    <FormText color="dark">Cargar imagen</FormText>
                  </FormGroup>
                </Col>
                <Col xs="12" md="6">
                  {this.state.f1 && <FormGroup>
                    <img src={'http://0.0.0.0:8000' + this.state.depositosAcuenta.dep_img_boleta} className="img-responsive" width="100%" />
                    <br />
                  </FormGroup>}
                  {this.state.f2 && <FormGroup>
                    <img src={this.state.file} className="img-responsive" width="100%" />
                    <br />
                  </FormGroup>}
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default EditarComprobante;

