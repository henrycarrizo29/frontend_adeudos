import React from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup, FormText,
  Input, InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
import DetalleComprobantes from './DetalleComprobantes'

const stylesIcono = { color: '#4682b4' }

class DepositosCancelados extends React.Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.state = {
      depositosAcuenta: {
        dep_fecha_boleta: '',
        dep_monto_total: '',
        dep_numero: '',
        dep_img_boleta: null,
        dep_fecha_registro: date,
        dep_cdp: this.props.cdpest[0].id,
      },
      comprobantesReg: [],
      fecha: date,
      c1: false,
      c2: false,
      c3: false,
      modal2: false,
      modal3: false,
      imagen: null,
      file: null,
      cdpEstudiante: {},
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFoto = this.handleChangeFoto.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.actualiza = this.actualiza.bind(this);
    this.actualizaCDP = this.actualizaCDP.bind(this);
  }

  actualiza() {
    this.componentDidMount()
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf1 = this.state.depositosAcuenta;
    let inf2 = this.state.depositosAcuenta;
    let inf3 = this.state.depositosAcuenta;
    let inf4 = this.state.depositosAcuenta;
    let inf5 = this.state.depositosAcuenta;
    let inf6 = this.state.depositosAcuenta;
    inf1['dep_fecha_boleta'] = '';
    inf2['dep_monto_total'] = '';
    inf3['dep_numero'] = '';
    inf4['dep_img_boleta'] = null;
    inf5['dep_fecha_registro'] = this.state.fecha;
    inf6['dep_cdp'] = this.props.cdpest[0].id;
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6 });
    this.setState({
      imagen: null,
      file: null,
    });
  }

  handleChange(event) {
    let depo = this.state.depositosAcuenta;
    depo[event.target.name] = event.target.value;
    this.setState({ depo });
  }

  handleOnControll() {
    const { dep_fecha_boleta, dep_monto_total, dep_numero } = this.state.depositosAcuenta

    dep_fecha_boleta === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    dep_monto_total === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    dep_numero === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    if (dep_fecha_boleta !== '' && dep_monto_total !== '' && dep_numero !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/depositos_cdp/v1/detalledepositobusca/' + this.props.cdpest[0].id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ comprobantesReg: json1 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromiso/' + this.props.cdpest[0].id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();
      this.setState({ cdpEstudiante: json2 });
    } catch (error) {
      console.log(error);
    }
  }


  handleChangeFoto(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      imagen: event.target.files[0]
    })
    let img = event.target.files[0]
  }

  actualizaCDP() {

    const cdpCancelado = {
      id: this.state.cdpEstudiante.id,
      com_fecha: this.state.cdpEstudiante.com_fecha,
      com_estado: 'CANCELADO',
      com_nroinforme: this.state.cdpEstudiante.com_nroinforme,
      com_observacion: this.state.cdpEstudiante.com_observacion,
      com_estudiante: this.state.cdpEstudiante.com_estudiante,
      com_usuario: this.state.cdpEstudiante.com_usuario,
      com_garante: this.state.cdpEstudiante.com_garante,
      com_parentesco: this.state.cdpEstudiante.com_parentesco,
      com_medico_acargo: this.state.cdpEstudiante.com_medico_acargo,
    }
    try {
      fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromiso/' + this.state.depositosAcuenta.dep_cdp + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(cdpCancelado),  
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    } catch (e) {
      console.log(e);
    }
  }


  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    // e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    const data = new FormData()
    data.append('dep_fecha_boleta', this.state.depositosAcuenta.dep_fecha_boleta);
    data.append('dep_monto_total', this.state.depositosAcuenta.dep_monto_total);
    data.append('dep_numero', this.state.depositosAcuenta.dep_numero);
    data.append('dep_img_boleta', this.state.imagen);
    data.append('dep_fecha_registro', this.state.depositosAcuenta.dep_fecha_registro);
    data.append('dep_cdp', this.state.depositosAcuenta.dep_cdp);

    if (this.state.imagen !== null) {
      try {
        fetch('http://0.0.0.0:8000/adeudos/depositos_cdp/v1/listadepositos', {
          method: 'POST', // or 'PUT'
          body: data, // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaCDP()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }

    } else {
      try {
        fetch('http://0.0.0.0:8000/adeudos/depositos_cdp/v1/listadepositos', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.depositosAcuenta), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaCDP()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <FormGroup row>
                  <Col md="9">
                    <CardTitle><strong><i className="fa fa-dollar fa-lg mt-1"></i> Comprobantes</strong></CardTitle>
                  </Col>
                </FormGroup>
              </CardHeader>
              <CardBody>
                <h4><strong><code style={stylesIcono}>Datos del comprobante</code></strong></h4>
                <hr />
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha Deposito:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="dep_fecha_boleta" name="dep_fecha_boleta" value={this.state.depositosAcuenta.dep_fecha_boleta} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Ingrese fecha de deposito</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Total Bolivianos:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="number" id="dep_monto_total" name="dep_monto_total" value={this.state.depositosAcuenta.dep_monto_total} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-money"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese monto total cancelado</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Codigo Deposito:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="dep_numero" name="dep_numero" value={this.state.depositosAcuenta.dep_numero} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-barcode"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese codigo de deposito</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <Input type="file" onChange={this.handleChangeFoto} />
                      </InputGroup>
                      <FormText color="dark">Cargar imagen</FormText>
                    </FormGroup>
                    <FormGroup row>
                      <Col xs="12" md="3"></Col>
                      <Col xs="12" md="10">
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Comprobante</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <img src={this.state.file} className="img-responsive" width="100%" />
                      <br />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h4><strong><code style={stylesIcono}>Comprobantes registrados</code></strong></h4>
                    <DetalleComprobantes comprobantes={this.state.comprobantesReg} actualiza={this.actualiza} />
                  </Col>
                </FormGroup>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    <i className="fa fa-dollar fa-lg mt-1"></i> Datos del Comprobante
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col xs="12" md="12">
                        <strong>Fecha de Deposito: </strong> {this.state.depositosAcuenta.dep_fecha_boleta}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Monto Total: </strong> {this.state.depositosAcuenta.dep_monto_total}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Codigo Deposito: </strong> {this.state.depositosAcuenta.dep_numero}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Datos</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Comprobante registrado
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default DepositosCancelados;