import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
} from 'reactstrap';
import ImagenDeposito from './ImagenDeposito';
import EditarComprobante from './EditarComprobante'


class DetalleComprobantes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comprobantes: this.props.comprobantes
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.comprobantes !== prevProps.comprobantes) {
      this.setState({
        comprobantes: this.props.comprobantes
      })
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro</strong></td>
                  <td align="center"><strong>Fecha_C</strong></td>
                  <td align="right"><strong>Total_C</strong></td>
                  <td align="center"><strong>Codigo_C</strong></td>
                  <td align="center"><strong>Img</strong></td>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {this.state.comprobantes.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align='center'>{i + 1}</td>
                      <td align='center'>{u.dep_fecha_boleta}</td>
                      <td align='right'>{u.dep_monto_total + '  Bs.'}</td>
                      <td align='center'>{u.dep_numero}</td>
                      <td align='center'><ImagenDeposito depo={u.dep_img_boleta} /></td>
                      <td align="center">
                        <EditarComprobante comprobante={u} actualiza={this.props.actualiza} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleComprobantes;

