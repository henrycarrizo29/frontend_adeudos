import React, { Component } from 'react';
import {
  Col, Row,
  Card, CardBody, CardHeader, CardTitle, CardSubtitle,
  FormText,
  Button,
} from 'reactstrap';

import Search from 'react-search-box'
import DetalleLiquidacion from './DetalleLiquidacion'
import CardEstudiante from '../../Compromisos/Compromiso/CardEstudiante';

const styles = { color: '#191970' }

class GestionarLiquidaciones extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cdpestudiante: [],
      listaCDP: [],
      show: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  // obteniendo los compromisos de pago registrado de un estudiante
  async componentDidMount() {
    try {
      const response3 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json3 = await response3.json();
      this.setState({ listaCDP: json3 });
    } catch (error) {
      console.log(error);
    }
  }

  async handleChange(value) {
    try {
      const response4 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detallecompromisobusca/' + value + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json4 = await response4.json();
      this.setState({ cdpestudiante: json4 });
      this.setState({ show: true })
    } catch (error) {
      console.log(error);
    }
  }

  handleClose() {
    window.location.reload();
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs="12" lg="6">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-list-alt fa-lg mt-1"></i> Gestionar Liquidacion</strong></CardTitle>
                {/*  <CardSubtitle>Gestionar Liquidacion</CardSubtitle> */}
              </CardHeader>
              <CardBody>
                <Row>
                  <Col md="1"></Col>
                  <Col md="5" className="text-right mt-1">
                    <Search
                      data={this.state.listaCDP}
                      onChange={this.handleChange.bind(this)}
                      placeholder="c.i..."
                      class="search-class"
                      searchKey="com_estudiante"
                    />
                    <FormText color="dark">Seleccione numero de c.i.</FormText>
                  </Col>
                  <Col xs="12" md="5" className="text-left mt-1">
                    <Button className="btn-twitter btn-brand mr-1 mb-1" size="lg" type="reset" onClick={this.handleClose}><i className="fa fa-refresh fa-sm"></i><span><strong>Reset</strong></span></Button>
                  </Col>                  
                </Row>
              </CardBody>
            </Card>
          </Col>
          <hr />
          <Col xs="12" md="6">
            {this.state.show && <CardEstudiante estudianteid={this.state.cdpestudiante[0].com_estudiante} />}
          </Col>
        </Row>
        {this.state.show && <DetalleLiquidacion cdpest={this.state.cdpestudiante} />}
      </div>
    );
  }
}

export default GestionarLiquidaciones;
