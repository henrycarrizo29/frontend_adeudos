import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  Table,
  Pagination, PaginationItem, PaginationLink, Button,
  FormGroup, Input, Label, Badge,
} from 'reactstrap';
import CertificadosExtendidos from './CertificadosExtendidos'
import PDFcertificado from './PDFcertificado';

const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }

class ListaCertificados extends Component {
  constructor(props) {
    super(props);

    this.state = {
      certificados: [],
      certificadosFiltrados: [],
      paginaActual: 0,
      elementosPag: 10,
      checked: [false, false],
    };
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e, index) {
    e.preventDefault();
    this.setState({
      paginaActual: index
    });
  }

  // obteniendo la lista de certificados registrados
  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/listacertificados', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      let jsonx = json1
      if (this.props.f1 !== '' && this.props.f2 !== '') {
        jsonx = json1.filter((item, indice) => {
          return item.cer_fecha >= this.props.f1 && item.cer_fecha <= this.props.f2;
        });

      }
      this.setState({
        certificados: jsonx,
        certificadosFiltrados: jsonx
      });
    } catch (e) {
      console.log(e);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.f1 !== prevProps.f1 || this.props.f2 !== prevProps.f2) {
      var a = new Array(2).fill(false)
      this.setState({
        checked: a,
      })
      this.componentDidMount();
    }
  }


  render() {
    const certificadosLista = this.state.certificadosFiltrados
    const contarPaginas = Math.ceil(certificadosLista.length / this.state.elementosPag);
    const { paginaActual, elementosPag } = this.state;
    const fin = (paginaActual + 1) * elementosPag;
    const inicio = fin - elementosPag;
    const listaCert = certificadosLista.slice(inicio, fin);

    return (
      <div>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-list-ol fa-lg mt-1"></i> Certificados Emitidos</strong>
                  <div className="card-header-actions">
                    <form>
                      <FormGroup row>
                        <Col md="12">
                          <PDFcertificado  f1={this.props.f1} f2={this.props.f2} certificados={this.state.certificados} certificadosFiltrados={this.state.certificadosFiltrados} />
                        </Col>
                      </FormGroup>
                    </form>
                  </div>
                </CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive size="sm" hover>
                  <thead>
                    <tr>
                      <td align="center"><strong>Nro.</strong></td>
                      <td align="center"><strong>C.I.</strong></td>
                      <td align="left"><strong>Estudiante</strong></td>
                      <td align="left"><strong>Carrera</strong></td>
                      <td align="center"><strong>Fecha Emisión</strong></td>
                      <td align="center"><strong>Motivo</strong></td>
                      <td align="left"><strong>Observacion</strong></td>
                      {/* <td align="center"><strong>Accion</strong></td> */}
                    </tr>
                  </thead>
                  <tbody>
                    {listaCert.map((cert, i) => (
                      <CertificadosExtendidos datoscert={cert} key={i} />
                    ))}
                  </tbody>
                </Table>
                <Row>
                  <Col md="10">
                    <Pagination size="">
                      {[...Array(contarPaginas)].map((page, i) =>
                        <PaginationItem active={i === paginaActual} key={i}>
                          <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                            {i + 1}
                          </PaginationLink>
                        </PaginationItem>
                      )}
                    </Pagination>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ListaCertificados;
