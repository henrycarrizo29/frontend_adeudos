import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  FormGroup, FormText,
  Input, Label,
  Button,
} from 'reactstrap';
import ListaCertificados from './ListaCertificados';

const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }

class VerCertificados extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fecha1: '',
      fecha2: '',
      c1: false,
      c2: false,
      checked: [false, false, false],
      certificadoFecha: false,
    };
    this.handleSearchKey = this.handleSearchKey.bind(this)
    this.handleChange = this.handleChange.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSearchKey(e) {
    const { value, name, id } = e.target;
    var a = new Array(4).fill(false)
    switch (id) {
      case "1":
        a[0] = true
        this.setState({
          fecha1: '',
          fecha2: '',
          certificadoFecha: false,
        })
        break;
      case "2":
        a[1] = true
        break;
      default:
        a[0] = true
    }
    this.setState({
      checked: a,
    })
  }

  handleOnControll() {
    const { fecha1, fecha2 } = this.state

    fecha1 === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    fecha2 === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    if (fecha1 !== '' && fecha2 !== '') {
      this.setState({
        certificadoFecha: true
      });
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col md="2"></Col>
          <Col xs="12" md="8">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-list-ol fa-lg mt-1"></i> Ver Certificados Emitidos</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col md="12">
                    <FormGroup check inline>
                      <Input onChange={this.handleSearchKey} checked={this.state.checked[0]} className="form-check-input" type="radio" id="1" name="busqueda" value="option1" />
                      <Label className="form-check-label" check htmlFor="radio1"><i className="fa fa-copy fa-lg mt-0" style={stylesUsuarios}></i> Todos los Certificados</Label>
                    </FormGroup>
                    <FormGroup check inline>
                      <Input onChange={this.handleSearchKey} checked={this.state.checked[1]} className="form-check-input" type="radio" id="2" name="busqueda" value="option2" />
                      <Label className="form-check-label" check htmlFor="radio2"><i className="fa fa-calendar fa-lg mt-0" style={stylesFecha}></i> Certificados por Fecha</Label>
                    </FormGroup>
                  </Col>
                </FormGroup>

                <hr></hr>

                {this.state.checked[1] && <FormGroup row>
                  <Col xs="12" md="4">
                    <Label htmlFor="text-input">Fecha inicio <i className="fa fa-calendar-minus-o fa-lg mt-0" style={stylesIcono}></i></Label>
                    <Input type="date" id="fecha1" name="fecha1" value={this.state.fecha1} required valid onChange={this.handleChange} />
                    {this.state.c1 && <FormText color="danger">Ingrese fecha inicio </FormText>}
                  </Col>
                  <Col xs="12" md="4">
                    <Label htmlFor="text-input">Fecha fin <i className="fa fa-calendar-plus-o fa-lg mt-0" style={stylesIcono}></i></Label>
                    <Input type="date" id="fecha2" name="fecha2" value={this.state.fecha2} required valid onChange={this.handleChange} />
                    {this.state.c2 && <FormText color="danger">Ingrese fecha fin</FormText>}
                  </Col>
                  <Col xs="12" md="4">
                    <br></br>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="reset" onClick={this.handleOnControll}><i className="fa fa-eye fa-sm"></i><span><strong>Ver</strong></span></Button>
                  </Col>
                </FormGroup>}
              </CardBody>
            </Card>
          </Col>

          <Col md="12">
            {this.state.checked[0] && <ListaCertificados f1={this.state.fecha1} f2={this.state.fecha2} />}
          </Col>
          <Col md="12">
            {this.state.certificadoFecha && <ListaCertificados f1={this.state.fecha1} f2={this.state.fecha2} />}
          </Col>
        </Row>
      </div>
    );
  }
}

export default VerCertificados;
