import React, { Component } from 'react';
import { Popover, PopoverBody, PopoverHeader, Button, Badge } from 'reactstrap';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'

class CertificadosExtendidos extends Component {
  constructor(props) {
    super(props)

    this.state = {
      estudiante: {},
      motivo: {},
      carrera: {},

      dia: '',
      mes: '',
      anio: '',
    }
  }

  async componentDidMount() {
    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.datoscert.cer_ciestudiante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json2 = await respuesta2.json();
      this.setState({ estudiante: json2 });

      try {
        const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallecarrera/' + this.state.estudiante.Carrera, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        });
        const json4 = await respuesta4.json();
        this.setState({ carrera: json4[0] });
      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/certificados_de_noadeudo/v1/detallemotivo/' + this.props.datoscert.cer_motivo + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json5 = await respuesta5.json();
      this.setState({ motivo: json5[0] });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    /* const tipoColor = (this.props.datoscom.com_estado === 'CANCELADO') ? 'success' : 'danger' */
    return (
      <tr>
        <td align="center">
          {this.props.datoscert.id}
        </td>
        <td align="center">
          {this.state.estudiante.ci}
        </td>
        <td>
          {this.state.estudiante.nombres + ' ' + this.state.estudiante.primer_apellido + ' ' + this.state.estudiante.segundo_apellido}
        </td>
        <td>
          {this.state.carrera.carrera}
        </td>

        <td align="center">
          {this.props.datoscert.cer_fecha}
        </td>
        <td align="center">
          {this.state.motivo.motivo}
        </td>
        <td align="left">
          {this.props.datoscert.cer_observacion}
        </td>
      </tr>
    )
  }
}

export default CertificadosExtendidos;
