import React, { Component } from 'react';
import {
  Col, Row,
  TabContent, TabPane,
  Nav, NavItem, NavLink,
  Card, CardBody, CardHeader, CardTitle,
  FormGroup,
  Badge,
} from 'reactstrap';
import classnames from 'classnames';
import AdicionarGasto from './AdicionarGasto'
import InformeApertura from './InformeApertura';
import InformeReposicion from './InformeReposicion';
import InformeCierre from './InformeCierre';
import SolicitarFondos from './SolicitarFondos'

const stylesCompromisos = { color: '#20a8d8' }
const stylesLiquidaciones = { color: '#ff7f50' }
const stylesDepositos = { color: '#4dbd74' }
const stylesInformeSocial = { color: '#e83e8c' }
const stylesInformeCompromiso = { color: '#63c2de' }

class Gastos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '3',
    };

    this.cambiarTabla = this.cambiarTabla.bind(this);
  }

  cambiarTabla(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-dollar fa-lg mt-1"></i> Gastos</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12" className="mb-4">
                    <Nav tabs>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.cambiarTabla('3'); }} >
                          <span className={this.state.activeTab === '3' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-dollar fa-lg mt-1" style={stylesDepositos}></i> Gastos
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.cambiarTabla('4'); }} >
                          <span className={this.state.activeTab === '4' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-folder-open fa-lg mt-0" style={stylesInformeSocial}></i> Apertura de Fondos
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.cambiarTabla('5'); }} >
                          <span className={this.state.activeTab === '4' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-refresh fa-lg mt-0" style={stylesInformeSocial}></i> Reposición de Fondos
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '6' })} onClick={() => { this.cambiarTabla('6'); }} >
                          <span className={this.state.activeTab === '4' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-folder fa-lg mt-0" style={stylesInformeSocial}></i> Cierre de Caja
                        </NavLink>
                      </NavItem>

                      {/* <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.cambiarTabla('5'); }} >
                          <span className={this.state.activeTab === '5' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-money fa-lg mt-1" style={stylesInformeCompromiso}></i> Solicitar Fondos
                        </NavLink>
                      </NavItem> */}
                    </Nav>

                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="3">
                        <AdicionarGasto />
                      </TabPane>
                      <TabPane tabId="4">
                        <InformeApertura />
                      </TabPane>
                      <TabPane tabId="5">
                        <InformeReposicion />
                      </TabPane>
                      <TabPane tabId="6">
                        <InformeCierre />
                      </TabPane>
                      {/* <TabPane tabId="5">
                        <SolicitarFondos />
                      </TabPane> */}
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Gastos;
