import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  FormGroup,
  Table,
  Badge,
  ButtonGroup
} from 'reactstrap';
import AdicionarGastoAinforme from './AdicionarGastoAinforme'
import CancelarGastoAinforme from './CancelarGastoAinforme'

const stylesIcono = { color: '#4682b4' }

class AdjuntarGastos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gastosReg: [],
      listaCategoria: [],
      listaPersonal: [],
      gastosAdjuntos: [],
      modal0: false,
      modal1: false,
      modal2: false,
      modal3: false,
    };
    this.modalsw0 = this.modalsw0.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    //this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.actualizaa = this.actualizaa.bind(this);
  }

  modalsw0() {
    this.setState({
      modal0: !this.state.modal0,
    });
  }
  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }
  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
  }

  actualizaa() {
    this.componentDidMount()
  }

  async componentDidMount() {

    const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json1 = await respuesta1.json();
    this.setState({
      listaPersonal: json1
    })

    const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json2 = await respuesta2.json();
    this.setState({
      listaCategoria: json2
    })

    const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoestado/ABIERTO/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json5 = await respuesta5.json();
    this.setState({ gastosReg: json5 });

    const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoestado/ENVIADO/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json6 = await respuesta6.json();
    this.setState({ gastosAdjuntos: json6 });
  }

  /* async handleOnSubmit(e) {
    let nuevoInforme = {
      cc_tipodeinforme: this.props.itemInforme.cc_tipodeinforme,
      cc_receptor: this.props.itemInforme.cc_receptor,
      cc_receptorcargo: this.props.itemInforme.cc_receptorcargo,
      cc_emisor1: this.props.itemInforme.cc_emisor1,
      cc_emisorcargo1: this.props.itemInforme.cc_emisorcargo1,
      cc_emisor2: this.props.itemInforme.cc_emisor2,
      cc_emisorcargo2: this.props.itemInforme.cc_emisorcargo2,
      cc_referencia: this.props.itemInforme.cc_referencia,
      cc_fecha: this.props.itemInforme.cc_fecha,
      cc_contenido: this.props.itemInforme.cc_contenido,
      cc_estado: 'ENVIADO',
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadofondo/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();

      if (json2.length === 0) {
        const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/ENVIADO/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json3 = await respuesta3.json();
        const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/APROBADO/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json4 = await respuesta4.json();
        if (json3.length === 0 && json4.length === 0) {
          try {
            fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.itemInforme.id + '/', {
              method: 'PUT', // or 'PUT'
              body: JSON.stringify(nuevoInforme), // data can be string or {object}!
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
              }
            }).then(res => res.json())
              .catch(error => console.error('Error:', error))
              .then(response => console.log('Success:', response))
              .then(() =>
                this.setState({
                  modal1: !this.state.modal1
                })
              );
          } catch (e) {
            console.log(e);
          }
        } else {
          this.setState({
            modal3: !this.state.modal3
          })
        }
      } else {
        this.setState({
          modal2: !this.state.modal2
        })
      }

    } catch (error) {
      console.log(error);
    }
  } */

  buscaSolicitante(sol) {
    let solNombre = this.state.listaPersonal.filter(lp => lp.ci === sol);
    return solNombre[0].nombres + ' ' + solNombre[0].primer_apellido
  }

  buscaCategoria(cat) {
    let catNombre = this.state.listaCategoria.filter(lp => lp.id === cat);
    return catNombre[0].cat_descripcion
  }

  render() {

    return (
      <div>
        <Button size="sm" color="success" onClick={this.modalsw0} disabled={this.props.itemInforme.cc_estado === 'APROBADO' || this.props.itemInforme.cc_estado === 'ASIGNADO' || this.props.itemInforme.cc_estado === 'ENVIADO' ? true : false}><i className="fa fa-upload fa-lg mt-0"></i>Adjuntar</Button>

        <Modal isOpen={this.state.modal0} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            Gastos en Caja Chica
          </ModalHeader>
          <ModalBody>
            <h4><strong><code style={stylesIcono}>Gastos para Reposicion</code></strong></h4>
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="left"><strong>Solicitante</strong></td>
                  <td align="center"><strong>Fecha_Reg</strong></td>
                  <td align="left"><strong>Categoria</strong></td>
                  <td align="left"><strong>Detalle</strong></td>
                  <td align="center"><strong>Factura</strong></td>
                  <td align="center"><strong>Recibo</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="right"><strong>Total(Bs.)</strong></td>
                  <td align="center"><strong>Accion Gastos</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.state.gastosReg.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{u.id}</td>
                      <td align="left">{this.buscaSolicitante(u.gas_solicitante)}</td>
                      <td align="center">{u.gas_fecha}</td>
                      <td align="left">{this.buscaCategoria(u.gas_categoria)}</td>
                      <td align="left">{u.gas_detalle}</td>
                      <td align="center">{u.gas_factura}</td>
                      <td align="center">{u.gas_recibo}</td>
                      <td align="center">
                        <Badge className="mr-1" color={u.gas_estado === 'ABIERTO' ? 'success' : 'dark'} pill>{u.gas_estado}</Badge>
                      </td>
                      <td align="right">{u.gas_total}</td>
                      <td align="center">
                        <ButtonGroup size="sm">
                          <AdicionarGastoAinforme itemGasto={u} actualizaa={this.actualizaa} itemInforme={this.props.itemInforme} />&nbsp;&nbsp;
                          {/*<CancelarEnvio itemInforme={u} actualiza={this.props.actualiza} />
                           <Button color="warning" onClick={this.enviarInforme(u)}><i className="fa fa-check fa-lg mt-0"></i></Button>&nbsp;&nbsp;
                                  <Button color="primary"><i className="fa fa-mail-reply fa-lg mt-0"></i></Button> */}
                        </ButtonGroup>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>

            <hr />
            <h4><strong><code style={stylesIcono}>Gastos Adjuntos en Informe</code></strong></h4>

            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="left"><strong>Solicitante</strong></td>
                  <td align="center"><strong>Fecha_Reg</strong></td>
                  <td align="left"><strong>Categoria</strong></td>
                  <td align="left"><strong>Detalle</strong></td>
                  <td align="center"><strong>Factura</strong></td>
                  <td align="center"><strong>Recibo</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="right"><strong>Total(Bs.)</strong></td>
                  <td align="center"><strong>Accion Gastos</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.state.gastosAdjuntos.map((uu, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{uu.id}</td>
                      <td align="left">{this.buscaSolicitante(uu.gas_solicitante)}</td>
                      <td align="center">{uu.gas_fecha}</td>
                      <td align="left">{this.buscaCategoria(uu.gas_categoria)}</td>
                      <td align="left">{uu.gas_detalle}</td>
                      <td align="center">{uu.gas_factura}</td>
                      <td align="center">{uu.gas_recibo}</td>
                      <td align="center">
                        <Badge className="mr-1" color={uu.gas_estado === 'ABIERTO' ? 'success' : 'dark'} pill>{uu.gas_estado}</Badge>
                      </td>
                      <td align="right">{uu.gas_total}</td>
                      <td align="center">
                        <ButtonGroup size="sm">
                          <CancelarGastoAinforme itemGasto={uu} actualizaa={this.actualizaa} />
                          {/* <Button color="warning" onClick={this.enviarInforme(u)}><i className="fa fa-check fa-lg mt-0"></i></Button>&nbsp;&nbsp;
                                  <Button color="primary"><i className="fa fa-mail-reply fa-lg mt-0"></i></Button> */}
                        </ButtonGroup>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw0}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Solicitud de Apertura de Caja Chica Enviado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            No puede solicitar la apertura de fondos debido a que no hizo el cierre de caja
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Ya cuenta con una solicitud para la apertura de fondos, cancele la solicitud enviada para enviar una nueva.
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw3(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AdjuntarGastos;