import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
  Popover, PopoverHeader, PopoverBody,
  Button, ButtonGroup,
  Badge,
} from 'reactstrap';

import EditarInformeGasto from './EditarInformeGasto'
import PDFinformeGastos from './PDFinformeGastos'
import AdjuntarGastos from './AdjuntarGastos'
import EnviarInformeReposicion from './EnviarInformeReposicion';
import CancelarEnvioReposicion from './CancelarEnvioReposicion';


class InformesRegistradosReposicion extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaInf: this.props.listaInformes
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.listaInformes !== prevProps.listaInformes) {
      this.setState({
        listaInf: this.props.listaInformes
      })
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="center"><strong>Informe</strong></td>
                  <td align="left"><strong>Remitente</strong></td>
                  <td align="left"><strong>Emisor</strong></td>
                  <td align="left"><strong>Referencia</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="center"><strong>Observacion</strong></td>
                  <td align="center"><strong>FechaInforme</strong></td>
                  <td align="center"><strong>PDF</strong></td>
                  <td align="center"><strong>Editar</strong></td>
                  <td align="center"><strong>Accion Informe</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.state.listaInf.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{u.id}</td>
                      <td align="left">{u.cc_tipodeinforme}</td>
                      <td align="left">{u.cc_receptor}</td>
                      <td align="left">{u.cc_emisor1}</td>
                      <td align="left">{u.cc_referencia}</td>
                      <td align="center">
                        <Badge className="mr-1" color={u.cc_estado === 'RECHAZADO' ? 'danger' : u.cc_estado === 'EN_INFORME' ? 'primary' : u.cc_estado === 'ENVIADO' ? 'warning' : u.cc_estado === 'APROBADO' ? 'secondary' : 'success'} pill>{u.cc_estado}</Badge>
                      </td>
                      <td align="left">{u.cc_observacion}</td>
                      <td align="center">{u.cc_fecha}</td>
                      <td align="center">
                        <PDFinformeGastos idinforme={u.id} />
                      </td>
                      <td align="center">
                        <EditarInformeGasto informe={u} actualiza={this.props.actualiza} />
                      </td>
                      <td align="center">
                        <ButtonGroup size="sm">
                          <AdjuntarGastos itemInforme={u} />&nbsp;&nbsp;
                          <EnviarInformeReposicion itemInforme={u} actualiza={this.props.actualiza} />&nbsp;&nbsp;
                          <CancelarEnvioReposicion itemInforme={u} actualiza={this.props.actualiza} />
                          {/* <Button color="warning" onClick={this.enviarInforme(u)}><i className="fa fa-check fa-lg mt-0"></i></Button>&nbsp;&nbsp;
                                  <Button color="primary"><i className="fa fa-mail-reply fa-lg mt-0"></i></Button> */}
                        </ButtonGroup>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default InformesRegistradosReposicion;

