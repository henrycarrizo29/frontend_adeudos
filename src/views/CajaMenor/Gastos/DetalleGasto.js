import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
  Popover, PopoverHeader, PopoverBody,
  Button,
  Badge,
} from 'reactstrap';

import EditarGasto from './EditarGasto'
import PDFreciboGastos from './PDFreciboGastos'
import ImagenComprobante from './ImagenComprobante'

class DetalleGasto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listaGasto: this.props.gastosReg,
      listaCategoria: [],
      listaPersonal: [],
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.gastosReg !== prevProps.gastosReg) {
      this.setState({
        listaGasto: this.props.gastosReg
      })
    }
  }

  async componentDidMount() {
    const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json1 = await respuesta1.json();
    this.setState({
      listaPersonal: json1
    })

    const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json2 = await respuesta2.json();
    this.setState({
      listaCategoria: json2
    })
  }

  buscaSolicitante(sol) {
    let solNombre = this.state.listaPersonal.filter(lp => lp.ci === sol);
    return solNombre[0].nombres + ' ' + solNombre[0].primer_apellido
  }

  buscaCategoria(cat) {
    let catNombre = this.state.listaCategoria.filter(lp => lp.id === cat);
    return catNombre[0].cat_descripcion
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="left"><strong>Solicitante</strong></td>
                  <td align="center"><strong>Fecha_Reg</strong></td>
                  <td align="left"><strong>Categoria</strong></td>
                  <td align="left"><strong>Detalle</strong></td>
                  <td align="center"><strong>Factura</strong></td>
                  <td align="center"><strong>Recibo</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="right"><strong>Total(Bs.)</strong></td>
                  <td align="center"><strong>Img.</strong></td>
                  <td align="center"><strong>Rec.</strong></td>
                  <td align="center"><strong>Edit</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.state.listaGasto.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{u.id}</td>
                      <td align="left">{this.buscaSolicitante(u.gas_solicitante)}</td>
                      <td align="center">{u.gas_fecha}</td>
                      <td align="left">{this.buscaCategoria(u.gas_categoria)}</td>
                      <td align="left">{u.gas_detalle}</td>
                      <td align="center">{u.gas_factura}</td>
                      <td align="center">{u.gas_recibo}</td>
                      <td align="center">
                        <Badge className="mr-1" color={u.gas_estado === 'ABIERTO' ? 'success' :  'dark'} pill>{u.gas_estado}</Badge>                        
                      </td>
                      <td align="right">{u.gas_total}</td>
                      <td align='center'><ImagenComprobante comprobante={u.gas_archivo} /></td>
                      <td align="center">
                        <PDFreciboGastos idgasto={u.id} />
                      </td>
                      <td align="center">
                        <EditarGasto gasto={u} actualiza={this.props.actualiza} />
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleGasto;

