import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class EditarInformeGasto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      informeGasto: {
        cc_tipodeinforme: this.props.informe.cc_tipodeinforme,
        cc_receptor: this.props.informe.cc_receptor,
        cc_receptorcargo: this.props.informe.cc_receptorcargo,
        cc_emisor1: this.props.informe.cc_emisor1,
        cc_emisorcargo1: this.props.informe.cc_emisorcargo1,
        cc_emisor2: this.props.informe.cc_emisor2,
        cc_emisorcargo2: this.props.informe.cc_emisorcargo2,
        cc_referencia: this.props.informe.cc_referencia,
        cc_fecha: this.props.informe.cc_fecha,
        cc_observacion: '',
        cc_contenido: this.props.informe.cc_contenido,
      },
      contenido1: this.props.informe.cc_contenido.split("&%#")[0],
      contenido2: this.props.informe.cc_contenido.split("&%#")[1],
      contenido3: this.props.informe.cc_contenido.split("&%#")[2],
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      c6: false,
      modal1: false,
      modal2: false,
    }
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeContenido = this.handleChangeContenido.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.informe !== prevProps.informe) {
      this.setState({
        informeGasto: this.props.informe,
        contenido1: this.props.informe.cc_contenido.split("&%#")[0],
        contenido2: this.props.informe.cc_contenido.split("&%#")[1],
        contenido3: this.props.informe.cc_contenido.split("&%#")[2],
      })
    }
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleOnControll() {
    let inf1 = this.state.informeGasto;
    inf1['cc_contenido'] = this.state.contenido1 + '&%#' + this.state.contenido2 + '&%#' + this.state.contenido3;
    this.setState({ inf1 });

    const { cc_tipodeinforme, cc_receptor, cc_emisor1, cc_referencia, cc_fecha, cc_contenido } = this.state.informeGasto

    cc_tipodeinforme === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    cc_receptor === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    cc_emisor1 === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    cc_referencia === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    cc_fecha === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    cc_contenido === '' || cc_contenido === '&%#&%#'
      ? (this.setState({ c6: true }))
      : (this.setState({ c6: false }))

    if (cc_tipodeinforme !== '' && cc_receptor !== '' && cc_emisor1 !== '' && cc_referencia !== '' && cc_fecha !== '' && cc_contenido !== '' && cc_contenido !== '&%#&%#') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.informe.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(this.state.informeGasto), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange(event) {
    let infGasto = this.state.informeGasto;
    infGasto[event.target.name] = event.target.value;
    this.setState({ infGasto });
  }

  handleChangeContenido(event) {
    const { value, name } = event.target
    this.setState({
      [name]: value,
    })
  }

  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1} disabled={this.props.informe.cc_estado !== 'EN_INFORME' && this.props.informe.cc_estado !== 'RECHAZADO' ? true : false}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file-o fa-lg mt-1"></i> Editar Informe
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <FormGroup row>
                <Col sm="12" md={{ size: 7, offset: 4 }}>
                  <strong>NOTA N° </strong>{this.props.informe.id}
                </Col>
              </FormGroup>
              <hr />
              <Row>
                <Col xs="12" sm="6">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Informe:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="select" name="cc_tipodeinforme" id="cc_tipodeinforme" required valid value={this.state.informeGasto.cc_tipodeinforme} onChange={this.handleChange} disabled>
                        <option value=""></option>
                        <option value="APERTURA">APERTURA DE CAJA CHICA</option>
                        <option value="CIERRE">CIERRE DE CAJA CHICA</option>
                        <option value="REPOSICION">REPOSICIÓN DE CAJA CHICA</option>
                      </Input>
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-file-text"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Seleccione tipo de informe</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>A:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_receptor" name="cc_receptor" required valid value={this.state.informeGasto.cc_receptor} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese nombre del receptor</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Cargo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_receptorcargo" name="cc_receptorcargo" value={this.state.informeGasto.cc_receptorcargo} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>De:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_emisor1" name="cc_emisor1" required valid value={this.state.informeGasto.cc_emisor1} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c3 && <FormText color="danger">Ingrese nombre del emisor</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Cargo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_emisorcargo1" name="cc_emisorcargo1" value={this.state.informeGasto.cc_emisorcargo1} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                 {/*  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>De:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_emisor2" name="cc_emisor2" value={this.state.informeGasto.cc_emisor2} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-plus"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Cargo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_emisorcargo2" name="cc_emisorcargo2" value={this.state.informeGasto.cc_emisorcargo2} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user-plus"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                  </FormGroup> */}
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Ref:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cc_referencia" name="cc_referencia" required valid value={this.state.informeGasto.cc_referencia} onChange={this.handleChange} />
                    </InputGroup>
                    {this.state.c4 && <FormText color="danger">Ingrese referencia</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="cc_fecha" name="cc_fecha" required valid value={this.state.informeGasto.cc_fecha} onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c5 && <FormText color="danger">Ingrese fecha</FormText>}
                  </FormGroup>
                  {/* <FormGroup row>
                    <Col sm="12" md={{ size: 10, offset: 2 }}>
                      <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar informe</strong></span></Button>
                    </Col>
                  </FormGroup> */}
                </Col>
                <Col xs="12" sm="6">
                  <FormGroup>
                    <Label htmlFor="text-input">Párrafo 1</Label>
                    <Input type="textarea" rows="3" id="contenido1" name="contenido1" required valid value={this.state.contenido1} onChange={this.handleChangeContenido} />
                    {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="text-input">Párrafo 2</Label>
                    <Input type="textarea" rows="4" id="contenido2" name="contenido2" required valid value={this.state.contenido2} onChange={this.handleChangeContenido} />
                    {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="text-input">Párrafo 3</Label>
                    <Input type="textarea" rows="3" id="contenido3" name="contenido3" required valid value={this.state.contenido3} onChange={this.handleChangeContenido} />
                    {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default EditarInformeGasto;

