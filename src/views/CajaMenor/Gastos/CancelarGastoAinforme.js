import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class CancelarGastoAinforme extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemGas: this.props.itemGasto
    };
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.itemGasto !== prevProps.itemGasto) {
      this.setState({
        itemGas: this.props.itemGasto
      })
    }
  }

  handleOnSubmit(e) {
    let nuevoGasto = {
      gas_fecha: this.state.itemGas.gas_fecha,
      gas_total: this.state.itemGas.gas_total,
      gas_detalle: this.state.itemGas.gas_detalle,
      gas_factura: this.state.itemGas.gas_factura,
      gas_recibo: this.state.itemGas.gas_recibo,
      gas_nota: this.state.itemGas.gas_nota,
      gas_estado: 'ABIERTO',
      gas_solicitante: this.state.itemGas.gas_solicitante,
      gas_fondos: this.state.itemGas.gas_fondos,
      gas_categoria: this.state.itemGas.gas_categoria,
      gas_informe_cc: '',
    }

    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + this.state.itemGas.id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(nuevoGasto), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="dark" onClick={(event) => { this.props.actualizaa(); this.handleOnSubmit(); }}><i className="fa fa-mail-reply fa-lg mt-0"></i>Cancelar</Button>      
      </div>
    );
  }
}

export default CancelarGastoAinforme;