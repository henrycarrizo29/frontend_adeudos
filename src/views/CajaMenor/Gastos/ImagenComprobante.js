import React, { Component } from 'react';
import {
  Modal, ModalHeader, ModalBody, ModalFooter,
  Button,
  Card, CardBody, CardImg,
} from 'reactstrap';


class ImagenComprobante extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modal1: false,
    }
    this.modalsw1 = this.modalsw1.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  render() {

    return (
      <div>
        <Button size="sm" className="btn-tumblr btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-file-photo-o fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-secondary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-file-image-o fa-lg mt-0"></i> Comprobante
          </ModalHeader>
          <ModalBody>           
            <img src={`http://0.0.0.0:8000` + this.props.comprobante} className="img-responsive" width="100%" />
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vk btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default ImagenComprobante;

