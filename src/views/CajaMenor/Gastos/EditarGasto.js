import React from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup, FormText,
  Input, InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Progress,
} from 'reactstrap';
import DetalleGastos from './DetalleGasto'

const stylesIcono = { color: '#4682b4' }

class EditarGasto extends React.Component {
  constructor(props) {
    super(props);
    //var hoy = new Date(),
    //date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.state = {
      actualizarGasto: {
        gas_solicitante: this.props.gasto.gas_solicitante,
        gas_fondos: this.props.gasto.gas_fondos,
        gas_fecha: this.props.gasto.gas_fecha,
        gas_total: this.props.gasto.gas_total,
        gas_categoria: this.props.gasto.gas_categoria,
        gas_detalle: this.props.gasto.gas_detalle,
        gas_factura: this.props.gasto.gas_factura,
        gas_recibo: this.props.gasto.gas_recibo,
        gas_nota: this.props.gasto.gas_nota,
        gas_archivo: this.props.gasto.gas_archivo,
      },
      gas_total_copia: this.props.gasto.gas_total,
      listaPersonal: [],
      listaCategoria: [],
      fondoAbierto: [],
      totalF: '',
      saldoF: '',
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      imagen: null,
      file: null,
      modal1: false,
      modal2: false,
      modal4: false,
      f1: true,
      f2: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFoto = this.handleChangeFoto.bind(this);
    this.handleChangeFondos = this.handleChangeFondos.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw4 = this.modalsw4.bind(this);
    this.actualizaFondoGastado = this.actualizaFondoGastado.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.gasto !== prevProps.gasto) {
      this.setState({
        actualizarGasto: this.props.gasto
      })
    }
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw4() {
    this.setState({
      modal4: !this.state.modal4,
    });
  }

  handleChange(event) {
    let gasto = this.state.actualizarGasto;
    gasto[event.target.name] = event.target.value;
    this.setState({ gasto });
  }

  handleChangeFondos(event) {
    let gasto = this.state.actualizarGasto;
    gasto[event.target.name] = event.target.value;
    this.setState({ gasto });
    console.log(gasto)

    if (this.state.fondoAbierto.length === 1) {
      if (parseInt(this.state.actualizarGasto.gas_total) <= parseInt(this.state.fondoAbierto[0].fon_saldo)) {
        let idFondo = this.state.actualizarGasto;
        idFondo["gas_fondos"] = this.state.fondoAbierto[0].id;
        this.setState({ idFondo });
      } else {
        if (this.state.actualizarGasto.gas_total !== '') {
          this.setState({
            modal4: !this.state.modal4
          })
          let inf3 = this.state.actualizarGasto;
          inf3['gas_total'] = '';
          this.setState({ inf3 });
        }
      }
    } else {
      if (this.state.fondoAbierto.lenght === 0) {
        this.setState({
          modal5: !this.state.modal5
        })
      } else {
        console.log(this.state.fondoAbierto)
      }
    }
  }

  actualizaFondoGastado() {

    const sustraerFondo = {
      id: this.state.fondoAbierto[0].id,
      fon_fecha: this.state.fondoAbierto[0].fon_fecha,
      fon_detalle: this.state.fondoAbierto[0].fon_detalle,
      fon_asignado: this.state.fondoAbierto[0].fon_asignado,
      fon_gastado: (parseFloat(this.state.fondoAbierto[0].fon_gastado) + parseFloat(this.state.actualizarGasto.gas_total) - parseFloat(this.state.gas_total_copia)).toString(),
      fon_saldo: (parseFloat(this.state.fondoAbierto[0].fon_saldo) - parseFloat(this.state.actualizarGasto.gas_total) + parseFloat(this.state.gas_total_copia)).toString(),
      fon_factura: this.state.fondoAbierto[0].fon_factura,
      fon_recibo: this.state.fondoAbierto[0].fon_recibo,
      fon_nota: this.state.fondoAbierto[0].fon_nota,
      fon_estado: this.state.fondoAbierto[0].fon_estado,
      fon_solicitante: this.state.fondoAbierto[0].fon_solicitante,
      fon_categoria: this.state.fondoAbierto[0].fon_categoria,
      fon_usuario: this.state.fondoAbierto[0].fon_usuario,
      fon_informecajachica: this.state.fondoAbierto[0].fon_informecajachica,
    }
    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallefondos/' + this.state.fondoAbierto[0].id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(sustraerFondo),
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .then(() =>
          this.props.actualiza()
        )
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    } catch (e) {
      console.log(e);
    }
  }


  handleOnControll() {
    const { gas_solicitante, gas_fecha, gas_total, gas_categoria, gas_detalle } = this.state.actualizarGasto
    let ai = 0
    gas_solicitante === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    gas_fecha === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    gas_total === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    gas_categoria === 0
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    gas_detalle === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    if (gas_solicitante !== '' && gas_fecha !== '' && gas_total !== '' && gas_categoria !== 0 && gas_detalle !== '') {
      const data = new FormData()
      data.append('gas_solicitante', this.state.actualizarGasto.gas_solicitante);
      data.append('gas_fondos', this.state.actualizarGasto.gas_fondos);
      data.append('gas_fecha', this.state.actualizarGasto.gas_fecha);
      data.append('gas_total', this.state.actualizarGasto.gas_total);
      data.append('gas_categoria', this.state.actualizarGasto.gas_categoria);
      data.append('gas_detalle', this.state.actualizarGasto.gas_detalle);
      data.append('gas_factura', this.state.actualizarGasto.gas_factura);
      data.append('gas_recibo', this.state.actualizarGasto.gas_recibo);
      data.append('gas_nota', this.state.actualizarGasto.gas_nota);
      this.state.file !== null
        ? data.append('gas_archivo', this.state.imagen)
        : (ai = ai + 1)

      const nuevosDatosGasto = {
        gas_solicitante: this.state.actualizarGasto.gas_solicitante,
        gas_fondos: this.state.actualizarGasto.gas_fondos,
        gas_fecha: this.state.actualizarGasto.gas_fecha,
        gas_total: this.state.actualizarGasto.gas_total,
        gas_categoria: this.state.actualizarGasto.gas_categoria,
        gas_detalle: this.state.actualizarGasto.gas_detalle,
        gas_factura: this.state.actualizarGasto.gas_factura,
        gas_recibo: this.state.actualizarGasto.gas_recibo,
        gas_nota: this.state.actualizarGasto.gas_nota,
      }

      if (this.state.file !== null) {
        try {
          fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + this.props.gasto.id + '/', {
            method: 'PUT', // or 'PUT'
            body: data, // data can be string or {object}!   body: JSON.stringify(cdpCancelado),             
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
                f1: true,
                f2: false,
              })
            )
            .then(() =>
              this.actualizaFondoGastado()
            )
        } catch (e) {
          console.log(e);
        }
      } else {
        try {
          fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + this.props.gasto.id + '/', {
            method: 'PUT', // or 'PUT'
            body: JSON.stringify(nuevosDatosGasto),
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`,
              'Content-Type': 'application/json'
            }
          }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Success:', response))
            .then(() =>
              this.setState({
                modal1: !this.state.modal1,
                modal2: !this.state.modal2,
                f1: true,
                f2: false,
              })
            )
            .then(() =>
              this.actualizaFondoGastado()
            );
        } catch (e) {
          console.log(e);
        }
      }
    }
  }

  async componentDidMount() {
    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ listaPersonal: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaCategoria: json4 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuestaF = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadofondo/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const jsonF = await respuestaF.json();
      this.setState({ fondoAbierto: jsonF });
      this.setState({
        totalF: jsonF[0].fon_asignado,
        saldoF: jsonF[0].fon_saldo,
      });
    } catch (error) {
      console.log(error);
    }
  }


  /*  handleChangeFoto(event) {
     console.log("img", event)
     this.setState({
       file: URL.createObjectURL(event.target.files[0]),
       imagen: event.target.files[0]
     })
     let img = event.target.files[0]
     console.log(img)
   } */

  handleChangeFoto(event) {
    let compr = this.state.actualizarGasto;
    compr['gas_archivo'] = event.target.files[0];
    this.setState({ compr });

    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      imagen: event.target.files[0],
      f1: false,
      f2: true,
    })
    let img = event.target.files[0]
  }

  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1} disabled={this.props.gasto.gas_estado === 'CERRADO' || this.props.gasto.gas_estado === 'ENVIADO' ? true : false}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            <i className="fa fa-dollar fa-lg mt-1"></i> Editar Gasto
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" md="6">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Solicitante:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="select" name="gas_solicitante" id="gas_solicitante" value={this.state.actualizarGasto.gas_solicitante} required valid onChange={this.handleChange}>
                        <option value=""></option>
                        {this.state.listaPersonal.map((item2, i) => (
                          <option value={item2.ci} key={i}>{item2.nombres + ' ' + item2.primer_apellido}</option>
                        ))}
                      </Input>
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Se requiere nombre del solicitante</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Fecha:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="date" id="gas_fecha" name="gas_fecha" value={this.state.actualizarGasto.gas_fecha} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c2 && <FormText color="danger">Ingrese fecha</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Total Bs.:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="number" id="gas_total" name="gas_total" value={this.state.actualizarGasto.gas_total} required valid onChange={this.handleChangeFondos} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-money"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c3 && <FormText color="danger">Ingrese monto total solicitado</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Categoria:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="select" name="gas_categoria" id="gas_categoria" value={this.state.actualizarGasto.gas_categoria} required valid onChange={this.handleChange}>
                        <option value=""></option>
                        {this.state.listaCategoria.map((item3, j) => (
                          <option value={item3.id} key={j}>{item3.cat_descripcion}</option>
                        ))}
                      </Input>
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-cubes"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c4 && <FormText color="danger">Seleccione categoria</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Detalle:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="gas_detalle" name="gas_detalle" value={this.state.actualizarGasto.gas_detalle} required valid onChange={this.handleChange} />
                      <InputGroupAddon addonType="append">
                        <InputGroupText><i className="fa fa-cube"></i></InputGroupText>
                      </InputGroupAddon>
                    </InputGroup>
                    {this.state.c5 && <FormText color="danger">Describa detalle de la categoria</FormText>}
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Factura:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="gas_factura" name="gas_factura" value={this.state.actualizarGasto.gas_factura} onChange={this.handleChange} placeholder="Codigo de factura" />
                      <InputGroupAddon addonType="append">
                        <InputGroupText>Recibo:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="gas_recibo" name="gas_recibo" value={this.state.actualizarGasto.gas_recibo} onChange={this.handleChange} placeholder="Codigo de recibo" />
                    </InputGroup>
                  </FormGroup>
                  <FormGroup>
                    <InputGroup>
                      <Input type="file" onChange={this.handleChangeFoto} />
                    </InputGroup>
                    <FormText color="dark">Cargar imagen</FormText>
                  </FormGroup>
                </Col>
                <Col xs="12" md="6">
                  {this.state.f1 && <FormGroup>
                    <img src={'http://0.0.0.0:8000' + this.state.actualizarGasto.gas_archivo} className="img-responsive" width="100%" />
                    <br />
                  </FormGroup>}
                  {this.state.f2 && <FormGroup>
                    <img src={this.state.file} className="img-responsive" width="100%" />
                    <br />
                  </FormGroup>}
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Guargar Cambios</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Datos Actualizados
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal4} className={'modal-danger ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            El monto solicitado supera los fondos con los que cuenta.
                  </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw4}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default EditarGasto;