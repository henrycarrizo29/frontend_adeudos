import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle, CardSubtitle,
  Col, Row,
  Table,
  Badge,
  Label,
  Input,
  FormText, FormGroup,
  InputGroup, InputGroupAddon, InputGroupText,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }

class SolicitarFondos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listainformegasto: [],
      listainformesapertura: [],
      modal1: false,
      modal2: false,
    };
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  async componentDidMount() {
    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listainformegasto', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      this.setState({ listainformegasto: json5 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscainformecajachica/APERTURA/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json6 = await respuesta6.json();
      this.setState({ listainformesapertura: json6 });
    } catch (error) {
      console.log(error);
    }
  }


  render() {

    return (
      <div>
        <Row>
          <Col sm={{ size: 8, offset: 3 }}>
            <Button color="ghost-success" onClick={this.modalsw2}>
              <i className="fa fa-refresh fa-lg mt-0" style={stylesFecha}></i>&nbsp;<strong>Reposición de Fondos</strong>
            </Button>
            <Button color="ghost-primary" onClick={this.modalsw1}>
              <i className="fa fa-folder-open fa-lg mt-0" style={stylesUsuarios}></i>&nbsp;<strong>Apertura de Fondos</strong>
            </Button>
          </Col>
          <hr/>
        </Row>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size='lg'>
          <ModalHeader>
            Apertura de Fondos
          </ModalHeader>
          <ModalBody>
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="center"><strong>Informe</strong></td>
                  <td align="left"><strong>Remitente</strong></td>
                  <td align="left"><strong>Emisor</strong></td>
                  <td align="left"><strong>Referencia</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="center"><strong>FechaInforme</strong></td>
                  <td align="center"><strong>Adicionar</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.state.listainformesapertura.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{u.id}</td>
                      <td align="left">{u.cc_tipodeinforme}</td>
                      <td align="left">{u.cc_receptor}</td>

                      <td align="left">{u.cc_emisor1}</td>
                      <td align="left">{u.cc_referencia}</td>
                      <td align="center">
                        <Badge className="mr-1" color={u.cc_estado === 'RECHAZADO' ? 'danger' : u.cc_estado === 'EN_INFORME' ? 'primary' : 'success'} pill>{u.cc_estado}</Badge>
                      </td>
                      <td align="center">{u.cc_fecha}</td>
                      <td align="center">
                      <Button size="sm" className="btn-vine btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-plus fa-lg mt-0"></i></Button>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </ModalBody>
          <ModalFooter>
           {/*  <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button> */}
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size=''>
          <ModalHeader>
            Reposición de Fondos
          </ModalHeader>
          <ModalBody>
            fgfgf
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Informe registrado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default SolicitarFondos;