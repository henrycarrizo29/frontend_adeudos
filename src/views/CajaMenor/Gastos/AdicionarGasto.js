import React from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup, FormText,
  Input, InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Progress,
} from 'reactstrap';
import DetalleGastos from './DetalleGasto'

const stylesIcono = { color: '#4682b4' }

class AdicionarGasto extends React.Component {
  constructor(props) {
    super(props);
    //var hoy = new Date(),
    //date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.state = {
      solicitarGasto: {
        gas_solicitante: '',
        gas_fondos: 0,
        gas_fecha: '',
        gas_total: '',
        gas_categoria: 0,
        gas_detalle: '',
        gas_factura: '',
        gas_recibo: '',
        gas_nota: '',
      },
      total: 9999999999,
      listaPersonal: [],
      listaCategoria: [],
      fondoAbierto: [],
      gastosReg: [],
      totalF: '',
      saldoF: '',
      gastadoF:'',
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      imagen: null,
      file: null,
      modal2: false,
      modal3: false,
      modal4: false,
      modal5: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeFoto = this.handleChangeFoto.bind(this);
    this.handleChangeFondos = this.handleChangeFondos.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.modalsw4 = this.modalsw4.bind(this);
    this.modalsw5 = this.modalsw5.bind(this);
    this.actualiza = this.actualiza.bind(this);
    this.actualizaFondoGastado = this.actualizaFondoGastado.bind(this);
  }

  actualiza() {
    this.componentDidMount()
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf0 = this.state.solicitarGasto;
    let inf1 = this.state.solicitarGasto;
    let inf2 = this.state.solicitarGasto;
    let inf3 = this.state.solicitarGasto;
    let inf4 = this.state.solicitarGasto;
    let inf5 = this.state.solicitarGasto;
    let inf6 = this.state.solicitarGasto;
    let inf7 = this.state.solicitarGasto;
    let inf8 = this.state.solicitarGasto;

    inf0['gas_solicitante'] = '';
    inf1['gas_fondos'] = 0;
    inf2['gas_fecha'] = '';
    inf3['gas_total'] = '';
    inf4['gas_categoria'] = 0;
    inf5['gas_detalle'] = '';
    inf6['gas_factura'] = '';
    inf7['gas_recibo'] = '';
    inf8['gas_nota'] = '';
    this.setState({ inf0, inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8 });
    this.setState({
      imagen: null,
      file: null,
    });
  }

  modalsw4() {
    this.setState({
      modal4: !this.state.modal4,
    });
  }

  modalsw5() {
    this.setState({
      modal5: !this.state.modal5,
    });
  }

  handleChange(event) {
    let gasto = this.state.solicitarGasto;
    gasto[event.target.name] = event.target.value;
    this.setState({ gasto });
  }

  handleChangeFondos(event) {
    let gasto = this.state.solicitarGasto;
    gasto[event.target.name] = event.target.value;
    this.setState({ gasto });
    console.log(gasto)

    if (this.state.fondoAbierto.length === 1) {
      if (parseFloat(this.state.solicitarGasto.gas_total) <= parseFloat(this.state.fondoAbierto[0].fon_saldo)) {
        let idFondo = this.state.solicitarGasto;
        idFondo["gas_fondos"] = this.state.fondoAbierto[0].id;
        this.setState({ idFondo });
      } else {
        if (this.state.solicitarGasto.gas_total !== '') {
          this.setState({
            modal4: !this.state.modal4
          })
          let inf0 = this.state.solicitarGasto;
          let inf1 = this.state.solicitarGasto;
          let inf2 = this.state.solicitarGasto;
          let inf3 = this.state.solicitarGasto;
          let inf4 = this.state.solicitarGasto;
          let inf5 = this.state.solicitarGasto;
          let inf6 = this.state.solicitarGasto;
          let inf7 = this.state.solicitarGasto;
          let inf8 = this.state.solicitarGasto;

          inf0['gas_solicitante'] = '';
          inf1['gas_fondos'] = 0;
          inf2['gas_fecha'] = '';
          inf3['gas_total'] = '';
          inf4['gas_categoria'] = 0;
          inf5['gas_detalle'] = '';
          inf6['gas_factura'] = '';
          inf7['gas_recibo'] = '';
          inf8['gas_nota'] = '';
          this.setState({ inf0, inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8 });
          this.setState({
            imagen: null,
            file: null,
          });
        }
      }
    } else {
      if (this.state.fondoAbierto.length === 0) {
        this.setState({
          modal5: !this.state.modal5
        })
        let inf0 = this.state.solicitarGasto;
        let inf1 = this.state.solicitarGasto;
        let inf2 = this.state.solicitarGasto;
        let inf3 = this.state.solicitarGasto;
        let inf4 = this.state.solicitarGasto;
        let inf5 = this.state.solicitarGasto;
        let inf6 = this.state.solicitarGasto;
        let inf7 = this.state.solicitarGasto;
        let inf8 = this.state.solicitarGasto;

        inf0['gas_solicitante'] = '';
        inf1['gas_fondos'] = 0;
        inf2['gas_fecha'] = '';
        inf3['gas_total'] = '';
        inf4['gas_categoria'] = 0;
        inf5['gas_detalle'] = '';
        inf6['gas_factura'] = '';
        inf7['gas_recibo'] = '';
        inf8['gas_nota'] = '';
        this.setState({ inf0, inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8 });
        this.setState({
          imagen: null,
          file: null,
        });
      } else {
        console.log(this.state.fondoAbierto.length)
        console.log(this.state.fondoAbierto)
      }
    }
  }

  handleOnControll() {
    const { gas_solicitante, gas_fecha, gas_total, gas_categoria, gas_detalle } = this.state.solicitarGasto

    gas_solicitante === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    gas_fecha === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    gas_total === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    gas_categoria === 0
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    gas_detalle === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    if (gas_solicitante !== '' && gas_fecha !== '' && gas_total !== '' && gas_categoria !== 0 && gas_detalle !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  async componentDidMount() {
    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ listaPersonal: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaCategoria: json4 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuestaF = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadofondo/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const jsonF = await respuestaF.json();
      console.log(jsonF)
      if (jsonF.length !== 0) {
        this.setState({ fondoAbierto: jsonF });
        this.setState({
          totalF: jsonF[0].fon_asignado,
          saldoF: jsonF[0].fon_saldo,
          gastadoF:jsonF[0].fon_gastado,
        });
      } else {
        this.setState({ fondoAbierto: jsonF });
        this.setState({
          totalF: 1,
          saldoF: 0,
        });
      }

    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listagastos', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      this.setState({ gastosReg: json5 });
    } catch (error) {
      console.log(error);
    }
  }


  handleChangeFoto(event) {
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      imagen: event.target.files[0]
    })
    let img = event.target.files[0]
  }

  actualizaFondoGastado() {

    const sustraerFondo = {
      id: this.state.fondoAbierto[0].id,
      fon_fecha: this.state.fondoAbierto[0].fon_fecha,
      fon_detalle: this.state.fondoAbierto[0].fon_detalle,
      fon_asignado: this.state.fondoAbierto[0].fon_asignado,
      fon_gastado: (parseFloat(this.state.fondoAbierto[0].fon_gastado) + parseFloat(this.state.solicitarGasto.gas_total)).toString(),
      fon_saldo: (parseFloat(this.state.fondoAbierto[0].fon_saldo) - parseFloat(this.state.solicitarGasto.gas_total)).toString(),
      fon_factura: this.state.fondoAbierto[0].fon_factura,
      fon_recibo: this.state.fondoAbierto[0].fon_recibo,
      fon_nota: this.state.fondoAbierto[0].fon_nota,
      fon_estado: this.state.fondoAbierto[0].fon_estado,
      fon_solicitante: this.state.fondoAbierto[0].fon_solicitante,
      fon_categoria: this.state.fondoAbierto[0].fon_categoria,
      fon_usuario: this.state.fondoAbierto[0].fon_usuario,
      fon_informecajachica: this.state.fondoAbierto[0].fon_informecajachica,
    }
    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallefondos/' + this.state.fondoAbierto[0].id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(sustraerFondo),
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .then(() =>
          this.actualiza()
        )
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    } catch (e) {
      console.log(e);
    }
  }


  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    // e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    const data = new FormData()
    data.append('gas_solicitante', this.state.solicitarGasto.gas_solicitante);
    data.append('gas_fondos', this.state.solicitarGasto.gas_fondos);
    data.append('gas_fecha', this.state.solicitarGasto.gas_fecha);
    data.append('gas_total', this.state.solicitarGasto.gas_total);
    data.append('gas_categoria', this.state.solicitarGasto.gas_categoria);
    data.append('gas_detalle', this.state.solicitarGasto.gas_detalle);
    data.append('gas_factura', this.state.solicitarGasto.gas_factura);
    data.append('gas_recibo', this.state.solicitarGasto.gas_recibo);
    data.append('gas_nota', this.state.solicitarGasto.gas_nota);
    data.append('gas_archivo', this.state.imagen);

    if (this.state.imagen !== null) {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listagastos', {
          method: 'POST', // or 'PUT'
          body: data, // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaFondoGastado()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }

    } else {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listagastos', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.solicitarGasto), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaFondoGastado()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-dollar fa-lg mt-1"></i> Nuevo Gasto</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <div className="text-center"><strong>{'Fondos en caja Bs.: ' + parseFloat(this.state.saldoF) + ' de ' + (this.state.totalF === 1 ? 0 : parseFloat(this.state.saldoF) + parseFloat(this.state.gastadoF))}</strong></div>
                <Progress animated={true} striped={true} color={this.state.saldoF > (this.state.totalF) * 0.75 ? "success" : this.state.saldoF > this.state.totalF * 0.50 && this.state.saldoF < this.state.totalF * 0.75 ? "primary" : "danger"} value={(100 * parseFloat(this.state.saldoF) / (parseFloat(this.state.saldoF) + parseFloat(this.state.gastadoF))).toFixed(2)}><strong>{(100 * parseFloat(this.state.saldoF) / (parseFloat(this.state.saldoF) + parseFloat(this.state.gastadoF))).toFixed(2)} %</strong></Progress>
                <br />
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Solicitante:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="gas_solicitante" id="gas_solicitante" value={this.state.solicitarGasto.gas_solicitante} required valid onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaPersonal.map((item2, i) => (
                            <option value={item2.ci} key={i}>{item2.nombres + ' ' + item2.primer_apellido}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Se requiere nombre del solicitante</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="gas_fecha" name="gas_fecha" value={this.state.solicitarGasto.gas_fecha} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese fecha</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Total Bs.:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="number" id="gas_total" name="gas_total" value={this.state.solicitarGasto.gas_total} required valid onChange={this.handleChangeFondos} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-money"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese monto total solicitado</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Categoria:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="gas_categoria" id="gas_categoria" value={this.state.solicitarGasto.gas_categoria} required valid onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaCategoria.map((item3, j) => (
                            <option value={item3.id} key={j}>{item3.cat_descripcion}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-cubes"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c4 && <FormText color="danger">Seleccione categoria</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Detalle:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="gas_detalle" name="gas_detalle" value={this.state.solicitarGasto.gas_detalle} required valid onChange={this.handleChange} placeholder="Por concepto de..." />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-cube"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c5 && <FormText color="danger">Describa detalle de la categoria</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Factura:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="gas_factura" name="gas_factura" value={this.state.solicitarGasto.gas_factura} onChange={this.handleChange} placeholder="Codigo de factura" />
                        <InputGroupAddon addonType="append">
                          <InputGroupText>Recibo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="gas_recibo" name="gas_recibo" value={this.state.solicitarGasto.gas_recibo} onChange={this.handleChange} placeholder="Codigo de recibo" />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <Input type="file" onChange={this.handleChangeFoto} />
                      </InputGroup>
                      <FormText color="dark">Cargar imagen</FormText>
                    </FormGroup>
                    <FormGroup row>
                      <Col sm={{ size: 8, order: 2, offset: 4 }}>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Gasto</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <img src={this.state.file} className="img-responsive" width="100%" />
                      <br />
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <FormGroup row>
                  <Col xs="12" md="12">
                    <h4><strong><code style={stylesIcono}>Gastos registrados</code></strong></h4>
                    <DetalleGastos gastosReg={this.state.gastosReg} actualiza={this.actualiza} />
                  </Col>
                </FormGroup>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    <i className="fa fa-user fa-lg mt-1"></i> Datos del Solicitante
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col xs="12" md="12">
                        <strong>Solicitante: </strong> {this.state.solicitarGasto.gas_solicitante}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Fecha: </strong> {this.state.solicitarGasto.gas_fecha}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Total Bs.: </strong> {this.state.solicitarGasto.gas_total}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Categoria: </strong> {this.state.solicitarGasto.gas_categoria}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Detalle: </strong> {this.state.solicitarGasto.gas_detalle}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Factura: </strong> {this.state.solicitarGasto.gas_factura}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Recibo: </strong> {this.state.solicitarGasto.gas_recibo}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar Gasto</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Gasto registrado
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal4} className={'modal-danger ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    El monto solicitado supera los fondos con los que cuenta.
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw4}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal5} className={'modal-primary ' + this.props.className} size='sm'>
                  <ModalHeader>No cuenta con fondos</ModalHeader>
                  <ModalBody>
                    Cree un nuevo informe solicitando la apertura de fondos en caja chica
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw5}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AdicionarGasto;