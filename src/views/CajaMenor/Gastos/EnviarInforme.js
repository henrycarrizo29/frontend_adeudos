import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class EnviarInforme extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal1: false,
      modal2: false,
      modal3: false,
    };
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }
  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }
  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
  }

  async handleOnSubmit(e) {
    let nuevoInforme = {
      cc_tipodeinforme: this.props.itemInforme.cc_tipodeinforme,
      cc_receptor: this.props.itemInforme.cc_receptor,
      cc_receptorcargo: this.props.itemInforme.cc_receptorcargo,
      cc_emisor1: this.props.itemInforme.cc_emisor1,
      cc_emisorcargo1: this.props.itemInforme.cc_emisorcargo1,
      cc_emisor2: this.props.itemInforme.cc_emisor2,
      cc_emisorcargo2: this.props.itemInforme.cc_emisorcargo2,
      cc_referencia: this.props.itemInforme.cc_referencia,
      cc_fecha: this.props.itemInforme.cc_fecha,
      cc_contenido: this.props.itemInforme.cc_contenido,
      cc_estado: 'ENVIADO',
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadofondo/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();

      if (json2.length === 0) {
        const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/ENVIADO/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json3 = await respuesta3.json();
        const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/APROBADO/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json4 = await respuesta4.json();
        if (json3.length === 0 && json4.length === 0) {
          try {
            fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.itemInforme.id + '/', {
              method: 'PUT', // or 'PUT'
              body: JSON.stringify(nuevoInforme), // data can be string or {object}!
              headers: {
                Authorization: `JWT ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
              }
            }).then(res => res.json())
              .catch(error => console.error('Error:', error))
              .then(response => console.log('Success:', response))
              .then(() =>
                this.setState({
                  modal1: !this.state.modal1
                })
              );
          } catch (e) {
            console.log(e);
          }
        } else {
          this.setState({
            modal3: !this.state.modal3
          })
        }
      } else {
        this.setState({
          modal2: !this.state.modal2
        })
      }

    } catch (error) {
      console.log(error);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="warning" onClick={this.handleOnSubmit} disabled={this.props.itemInforme.cc_estado === 'APROBADO' || this.props.itemInforme.cc_estado === 'ASIGNADO' || this.props.itemInforme.cc_estado === 'ENVIADO' ? true : false}><i className="fa fa-send-o fa-lg mt-0"></i>Enviar</Button>
        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Solicitud de Apertura de Caja Chica Enviado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            No puede solicitar la apertura de fondos debido a que no hizo el cierre de caja
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Ya cuenta con una solicitud para la apertura de fondos, cancele la solicitud enviada para enviar una nueva.
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw3(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default EnviarInforme;