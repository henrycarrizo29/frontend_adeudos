import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Table, Alert, Badge, ButtonGroup,
} from 'reactstrap';

import InformesRegistrados from './InformesRegistrados'
import EnviarInforme from './EnviarInforme'
import CancelarEnvio from './CancelarEnvio'
import EnviarInformeReposicion from './EnviarInformeReposicion'
import CancelarEnvioReposicion from './CancelarEnvioReposicion'

const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }


class InformeApertura extends Component {
  constructor(props) {
    super(props);
    this.state = {
      informeGasto: {
        cc_tipodeinforme: 'APERTURA',
        cc_receptor: 'Mg. Sc. Dra. Viviana G. Kaiser Avila',
        cc_receptorcargo: 'DIRECTORA – PROMES a.i.',
        cc_emisor1: 'Lic. German Soruco Gutierrez',
        cc_emisorcargo1: 'AUXILIAR CONTABLE - PROMES',
        cc_emisor2: '',
        cc_emisorcargo2: '',
        cc_referencia: 'Solicitud de apertura de caja chica “PROMES”',
        cc_fecha: '',
        cc_contenido: '',
      },
      enviarInforme: {
        idinforme: '',
      },

      informesReg: [],
      listainformesapertura: [],
      listainformesreposicion: [],
      listagastosabierto: [],
      listagastosabiertocopia: [],
      listagastoscerradocopia: [],
      detalleinforme: {},
      contenido1: 'Mediante el presente, solicito a su autoridad, la apertura de Fondos de Caja Chica  para el “PROMES”.',
      contenido2: 'Los fondos asignados a caja chica serán  para cubrir únicamente gastos menores comprendidos en el presupuesto de la Gestión, los mismos que corresponden: Gastos transporte personal, Gastos para la compra de materiales, Gastos de refrigerio, Gastos menores de comunicación, Gastos destinados  a la adquisición de medicamentos y Gastos para compra de materiales de tratamiento',
      contenido3: 'Sin otro particular, es en cuanto informo para fines consiguientes.',
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      c5: false,
      c6: false,
      modal0: false,
      modal1: false,
      modal2: false,
      modal3: false,
      show1: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeEnviar = this.handleChangeEnviar.bind(this);
    this.handleChangeContenido = this.handleChangeContenido.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.modalsw0 = this.modalsw0.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.actualiza = this.actualiza.bind(this);

  }

  modalsw0() {
    this.setState({
      modal0: !this.state.modal0,
    });
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  // modalsw2 para confirmar el registro 
  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  // modalsw3 confirmacion de registro 
  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
    });
    let inf1 = this.state.informeGasto;
    let inf2 = this.state.informeGasto;
    let inf3 = this.state.informeGasto;
    let inf4 = this.state.informeGasto;
    let inf5 = this.state.informeGasto;
    let inf6 = this.state.informeGasto;
    let inf7 = this.state.informeGasto;
    let inf8 = this.state.informeGasto;
    let inf9 = this.state.informeGasto;
    let inf10 = this.state.informeGasto;
    inf1['cc_tipodeinforme'] = 'APERTURA';
    inf2['cc_receptor'] = 'Mg. Sc. Dra. Viviana G. Kaiser Avila';
    inf3['cc_receptorcargo'] = 'DIRECTORA – PROMES a.i.';
    inf4['cc_emisor1'] = 'Lic. German Soruco Gutierrez';
    inf5['cc_emisorcargo1'] = 'AUXILIAR CONTABLE - PROMES';
    inf6['cc_emisor2'] = '';
    inf7['cc_emisorcargo2'] = '';
    inf8['cc_referencia'] = 'Solicitud de apertura de caja chica “PROMES”';
    inf9['cc_fecha'] = '';
    inf10['cc_contenido'] = '';
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8, inf9, inf10 });
    this.setState({
      contenido1: 'Mediante el presente, solicito a su autoridad, la apertura de Fondos de Caja Chica  para el “PROMES”.',
      contenido2: 'Los fondos asignados a caja chica serán  para cubrir únicamente gastos menores comprendidos en el presupuesto de la Gestión, los mismos que corresponden: Gastos transporte personal, Gastos para la compra de materiales, Gastos de refrigerio, Gastos menores de comunicación, Gastos destinados  a la adquisición de medicamentos y Gastos para compra de materiales de tratamiento',
      contenido3: 'Sin otro particular, es en cuanto informo para fines consiguientes.',
    });
  }

  actualiza() {
    this.componentDidMount()
  }

  async componentDidMount() {
    let inf1 = this.state.informeGasto;
    inf1['cc_contenido'] = this.state.contenido1 + '&%#' + this.state.contenido2 + '&%#' + this.state.contenido3;
    this.setState({ inf1 });

    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listainformecajachica', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ informesReg: json1 });
      let itemFiltrado = json1.filter(item => item.cc_tipodeinforme === 'APERTURA');
      this.setState({ listainformesapertura: itemFiltrado });
      let itemFiltrado1 = json1.filter(item => item.cc_tipodeinforme === 'REPOSICION' && item.cc_estado !== 'ASIGNADO');
      this.setState({ listainformesreposicion: itemFiltrado1 });

    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoestado/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json2 = await respuesta2.json();
      this.setState({
        listagastosabierto: json2,
        listagastosabiertocopia: json2,
      });

    } catch (error) {
      console.log(error);
    }
  }

  handleOnControll() {
    let inf1 = this.state.informeGasto;
    inf1['cc_contenido'] = this.state.contenido1 + '&%#' + this.state.contenido2 + '&%#' + this.state.contenido3;
    this.setState({ inf1 });

    const { cc_tipodeinforme, cc_receptor, cc_emisor1, cc_referencia, cc_fecha, cc_contenido } = this.state.informeGasto

    cc_tipodeinforme === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    cc_receptor === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    cc_emisor1 === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    cc_referencia === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))

    cc_fecha === ''
      ? (this.setState({ c5: true }))
      : (this.setState({ c5: false }))

    cc_contenido === '' || cc_contenido === '&%#&%#'
      ? (this.setState({ c6: true }))
      : (this.setState({ c6: false }))

    if (cc_tipodeinforme !== '' && cc_receptor !== '' && cc_emisor1 !== '' && cc_referencia !== '' && cc_fecha !== '' && cc_contenido !== '' && cc_contenido !== '&%#&%#') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  handleChange(event) {
    let infGasto = this.state.informeGasto;
    infGasto[event.target.name] = event.target.value;
    this.setState({ infGasto });
  }

  handleChangeEnviar(event) {
    let env = this.state.enviarInforme;
    env[event.target.name] = event.target.value;
    this.setState({ env });

    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.state.enviarInforme.idinforme + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      }).then(res => res.json())
        .then(detalleinforme =>
          this.setState({
            detalleinforme
          })
        )
        .then(() =>
          this.setState({
            show1: true
          })
        )
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
    } catch (e) {
      console.log(e);
    }
  }

  handleChangeContenido(event) {
    const { value, name } = event.target
    this.setState({
      [name]: value,
    })
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listainformecajachica', {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.informeGasto), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal3: !this.state.modal3
          })
        )
        .then(() =>
          this.componentDidMount()
        );
    } catch (e) {
      console.log(e);
    }
  }

  AdicionarParaReposicion(idinf) {
    let lista = this.state.listagastosabiertocopia
    let gastosabierto = [];
    let gastoscerrado = [];
    let gasto = {}
    for (let i = 0; i < lista.length; i++) {
      gasto = lista[i]
      if (gasto.gas_estado === "CERRADO") {
        gastoscerrado.push(gasto)
      } else {
        if (gasto.id === idinf) {
          const gastoEstInf = {}
          gastoEstInf["id"] = gasto.id
          gastoEstInf["gas_fecha"] = gasto.gas_fecha
          gastoEstInf["gas_total"] = gasto.gas_total
          gastoEstInf["gas_detalle"] = gasto.gas_detalle
          gastoEstInf["gas_factura"] = gasto.gas_factura
          gastoEstInf["gas_recibo"] = gasto.gas_recibo
          gastoEstInf["gas_archivo"] = gasto.gas_archivo
          gastoEstInf["gas_nota"] = gasto.gas_nota
          gastoEstInf["gas_estado"] = "CERRADO"
          gastoEstInf["gas_solicitante"] = gasto.gas_solicitante
          gastoEstInf["gas_fondos"] = gasto.gas_fondos
          gastoEstInf["gas_categoria"] = gasto.gas_categoria
          gastoEstInf["gas_informe_cc"] = this.state.enviarInforme.id

          gastoscerrado.push(gastoEstInf)
        } else {
          gastosabierto.push(gasto)
        }
      }
    }
    this.setState({
      listagastosabiertocopia: gastosabierto,
      listagastoscerradocopia: gastoscerrado,
    })
  }

  CancelarParaReposicion(idinf) {
    let lista = this.state.listagastosabiertocopia
    let gastosabierto = [];
    let gastoscerrado = [];
    let gasto = {}
    for (let i = 0; i < lista.length; i++) {
      gasto = lista[i]
      if (gasto.gas_estado === "ABIERTO") {
        gastosabierto.push(gasto)
      } else {
        if (gasto.id === idinf) {
          const gastoEstInf = {}
          gastoEstInf["id"] = gasto.id
          gastoEstInf["gas_fecha"] = gasto.gas_fecha
          gastoEstInf["gas_total"] = gasto.gas_total
          gastoEstInf["gas_detalle"] = gasto.gas_detalle
          gastoEstInf["gas_factura"] = gasto.gas_factura
          gastoEstInf["gas_recibo"] = gasto.gas_recibo
          gastoEstInf["gas_archivo"] = gasto.gas_archivo
          gastoEstInf["gas_nota"] = gasto.gas_nota
          gastoEstInf["gas_estado"] = "ABIERTO"
          gastoEstInf["gas_solicitante"] = gasto.gas_solicitante
          gastoEstInf["gas_fondos"] = gasto.gas_fondos
          gastoEstInf["gas_categoria"] = gasto.gas_categoria
          gastoEstInf["gas_informe_cc"] = ''

          gastosabierto.push(gastoEstInf)
        } else {
          gastoscerrado.push(gasto)
        }
      }
    }
    this.setState({
      listagastosabiertocopia: gastosabierto,
      listagastoscerradocopia: gastoscerrado,
    })
  }

  render() {
    const lisAbierto = this.state.listagastosabiertocopia
    const lisCerrado = this.state.listagastoscerradocopia

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-file-o fa-lg mt-1"></i> Nuevo Informe</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <FormGroup row>
                  <Col sm="12" md={{ size: 7, offset: 5 }}>
                    <strong>NOTA N° </strong>{}
                  </Col>
                </FormGroup>
                <hr />
                <Row>
                  <Col xs="12" sm="5">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Informe:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="cc_tipodeinforme" id="cc_tipodeinforme" required valid value={this.state.informeGasto.cc_tipodeinforme} onChange={this.handleChange} placeholder="APERTURA DE CAJA CHICA" disabled>
                          <option value=""></option>
                          <option value="APERTURA">APERTURA DE CAJA CHICA</option>
                          <option value="CIERRE">CIERRE DE CAJA CHICA</option>
                          <option value="REPOSICION">REPOSICIÓN DE CAJA CHICA</option>
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-file-text"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Seleccione tipo de informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>A:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_receptor" name="cc_receptor" required valid value={this.state.informeGasto.cc_receptor} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese nombre del receptor</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Cargo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_receptorcargo" name="cc_receptorcargo" value={this.state.informeGasto.cc_receptorcargo} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-o"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>De:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_emisor1" name="cc_emisor1" required valid value={this.state.informeGasto.cc_emisor1} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese nombre del emisor</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Cargo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_emisorcargo1" name="cc_emisorcargo1" value={this.state.informeGasto.cc_emisorcargo1} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    {/* <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>De:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_emisor2" name="cc_emisor2" value={this.state.informeGasto.cc_emisor2} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-plus"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Cargo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_emisorcargo2" name="cc_emisorcargo2" value={this.state.informeGasto.cc_emisorcargo2} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user-plus"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup> */}
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Ref:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="cc_referencia" name="cc_referencia" required valid value={this.state.informeGasto.cc_referencia} onChange={this.handleChange} />
                      </InputGroup>
                      {this.state.c4 && <FormText color="danger">Ingrese referencia</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="cc_fecha" name="cc_fecha" required valid value={this.state.informeGasto.cc_fecha} onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c5 && <FormText color="danger">Ingrese fecha</FormText>}
                    </FormGroup>
                    <FormGroup row>
                      <Col sm="12" md={{ size: 10, offset: 2 }}>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar informe</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col xs="12" sm="7">
                    <FormGroup>
                      <Label htmlFor="text-input">Párrafo 1</Label>
                      <Input type="textarea" rows="3" id="contenido1" name="contenido1" required valid value={this.state.contenido1} onChange={this.handleChangeContenido} />
                      {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="text-input">Párrafo 2</Label>
                      <Input type="textarea" rows="4" id="contenido2" name="contenido2" required valid value={this.state.contenido2} onChange={this.handleChangeContenido} />
                      {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="text-input">Párrafo 3</Label>
                      <Input type="textarea" rows="3" id="contenido3" name="contenido3" required valid value={this.state.contenido3} onChange={this.handleChangeContenido} />
                      {this.state.c6 && <FormText color="danger">Ingrese contenido del informe</FormText>}
                    </FormGroup>
                  </Col>
                </Row>
                <hr />
                <h5><strong><code style={stylesIcono}>Informes de Apertura de Fondos</code></strong></h5>
                <InformesRegistrados listaInformes={this.state.listainformesapertura} actualiza={this.actualiza} />

              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    Datos del informe
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col sm="12" md={{ size: 12, offset: 0 }}>
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Informe:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_tipodeinforme}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_receptor}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cargo:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_receptorcargo}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_emisor1}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cargo:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_emisorcargo1}
                      </Col>
                      {/*  <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_emisor2}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cargo:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_emisorcargo2}
                      </Col> */}
                      <Col xs="12" md="12">
                        <strong>Referencia:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_referencia}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fecha:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_fecha}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>&nbsp;Contenido:&nbsp;&nbsp;</strong> {this.state.informeGasto.cc_contenido}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Informe registrado
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw3}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default InformeApertura;