import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class CancelarEnvioReposicion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal1: false,
      gastoEstado: [],
    };
    this.modalsw1 = this.modalsw1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }
  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });    
    this.state.gastoEstado.map((data, i) => {
      let gastoAbierto = {
        gas_solicitante: data.gas_solicitante,
        gas_fondos: data.gas_fondos,
        gas_fecha: data.gas_fecha,
        gas_total: data.gas_total,
        gas_categoria: data.gas_categoria,
        gas_detalle: data.gas_detalle,
        gas_factura: data.gas_factura,
        gas_recibo: data.gas_recibo,
        gas_nota: data.gas_nota,
        gas_estado: 'ABIERTO',
        gas_informe_cc: ''
      }
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + data.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(gastoAbierto), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
      } catch (e) {
        console.log(e);
      }
    })
  }

  async handleOnSubmit(e) {
    let nuevoInforme = {
      cc_tipodeinforme: this.props.itemInforme.cc_tipodeinforme,
      cc_receptor: this.props.itemInforme.cc_receptor,
      cc_receptorcargo: this.props.itemInforme.cc_receptorcargo,
      cc_emisor1: this.props.itemInforme.cc_emisor1,
      cc_emisorcargo1: this.props.itemInforme.cc_emisorcargo1,
      cc_emisor2: this.props.itemInforme.cc_emisor2,
      cc_emisorcargo2: this.props.itemInforme.cc_emisorcargo2,
      cc_referencia: this.props.itemInforme.cc_referencia,
      cc_fecha: this.props.itemInforme.cc_fecha,
      cc_contenido: this.props.itemInforme.cc_contenido,
      cc_estado: 'EN_INFORME',
    }

    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoinforme/' + this.props.itemInforme.id + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ gastoEstado: json1 });

    } catch (error) {
      console.log(error);
    }

    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.itemInforme.id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(nuevoInforme), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal1: !this.state.modal1
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="primary" onClick={this.handleOnSubmit} disabled={this.props.itemInforme.cc_estado === 'APROBADO' || this.props.itemInforme.cc_estado === 'ASIGNADO' ? true : false}><i className="fa fa-mail-reply fa-lg mt-0"></i>Cancelar</Button>
        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Envio Cancelado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default CancelarEnvioReposicion;