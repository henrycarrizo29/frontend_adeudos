import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Table,
} from 'reactstrap';

class AdicionarParaReposicion extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };

    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleOnSubmit(e) {
    let gastoCerrado = {
      gas_solicitante: this.props.itemGasto.gas_solicitante,
      gas_fondos: this.props.itemGasto.gas_fondos,
      gas_fecha: this.props.itemGasto.gas_fecha,
      gas_total: this.props.itemGasto.gas_total,
      gas_categoria: this.props.itemGasto.gas_categoria,
      gas_detalle: this.props.itemGasto.gas_detalle,
      gas_factura: this.props.itemGasto.gas_factura,
      gas_recibo: this.props.itemGasto.gas_recibo,
      gas_nota: this.props.itemGasto.gas_nota,
      gas_estado: 'CERRADO',
      gas_informe_cc: this.props.informeGasto,
    }


    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + this.props.itemGasto.id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(gastoCerrado), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="info" onClick={(event) => { this.handleOnSubmit(); this.props.actualiza(); }} ><i className="fa fa-plus fa-lg mt-0"></i> Adicionar </Button>
      </div>
    );
  }
}

export default AdicionarParaReposicion;