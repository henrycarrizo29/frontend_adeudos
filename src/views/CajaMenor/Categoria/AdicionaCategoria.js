import React, { Component } from 'react';
import {
  Card, CardBody,
  Col, Row,
  Input,
  FormText, FormGroup,
  Button,
  InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

const stylesIcono = { color: '#4682b4' }

class AdicionaCategoria extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoria: {
        cat_descripcion: '',
      },
      c1: false,
      modal1: false,
      modal2: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
    let cat = this.state.categoria;
    cat['cat_descripcion'] = '';
    this.setState({ cat })
  }

  handleOnControll() {
    const { cat_descripcion } = this.state.categoria

    cat_descripcion === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    if (cat_descripcion !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.categoria), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }

  handleChange(event) {
    let cat = this.state.categoria;
    cat[event.target.name] = event.target.value.toUpperCase();
    this.setState({ cat });
  }

  /*   handleChange = event => {
      this.setState({ cat_descripcion: event.target.value })
    } */

  render() {

    return (
      <div>
        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-plus-square fa-sm"></i><span><strong>Adicionar Categoria</strong></span></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size=''>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Ingresar Nueva Categoria
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" sm="12">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Categoria:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cat_descripcion" name="cat_descripcion" required valid value={this.state.categoria.cat_descripcion} onChange={this.handleChange} />
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese descripcion</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Categoria Registrada
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
export default AdicionaCategoria;