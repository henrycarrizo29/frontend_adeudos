import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle, CardSubtitle,
  Col, Row,
  Table,
  Label,
  Input,
  FormText, FormGroup,
  InputGroup, InputGroupAddon, InputGroupText,
} from 'reactstrap';
import AdicionaCategoria from './AdicionaCategoria'
import ActualizaCategoria from './ActualizaCategoria'
import EliminaCategoria from './EliminaCategoria'

const stylesIcono = { color: '#4682b4' }

class Categoria extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listacategoria: [],
      valor: '',
    };
    this.buscarValor = this.buscarValor.bind(this)
    this.actualizaLista = this.actualizaLista.bind(this)
  }

  actualizaLista() {
    this.componentDidMount();
  }

  buscarValor(event) {
    this.setState({ valor: event.target.value.substr(0, 30) });
  }

  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      this.setState({ listacategoria: json1 });
    } catch (e) {
      console.log(e);
    }
  }


  render() {
    let items_filtrados = this.state.listacategoria.filter(
      (item) => {
        return (item.cat_descripcion)
          .toUpperCase().indexOf(
            this.state.valor.toUpperCase()) !== -1;
      }
    );

    return (
      <div>
        <Row>
          <Col sm={{ size: 8, order: 2, offset: 2 }}>
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-cubes fa-lg mt-1"></i> Categorias</strong>
                  <div className="card-header-actions">
                    <AdicionaCategoria actualiza={this.actualizaLista} />
                  </div>
                </CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md={{ size: 6, offset: 6 }} className="text-center mt-1">
                    <FormGroup>
                      <InputGroup>
                        <Input type="text" value={this.state.valor} onChange={this.buscarValor} valid placeholder="Busqueda por descripcion" />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-search fa-lg mt-1"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col xs="12" md="12">
                    <Table hover responsive size="sm">
                      <thead>
                        <tr>
                          <td align="center"><strong>Nro.</strong></td>
                          <td><strong>Descripcion</strong></td>
                          <td align="center"><strong></strong></td>
                          <td align="center"><strong></strong></td>
                        </tr>
                      </thead>
                      <tbody>
                        {items_filtrados.map((item, indice) => (
                          <tr key={indice}>
                            <td align="center">{indice + 1}</td>
                            <td>{item.cat_descripcion}</td>
                            <td align="center">
                              <ActualizaCategoria datosCat={item} actualizaLista={this.actualizaLista} />
                            </td>
                            <td align="center">
                              <EliminaCategoria datosCat={item} actualizaLista={this.actualizaLista} />
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </Table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Categoria;