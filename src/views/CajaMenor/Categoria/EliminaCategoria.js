import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
  Alert,
} from 'reactstrap';

const stylesIcono = { color: '#4682b4' }

class EliminaCategoria extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
    this.eliminarItem = this.eliminarItem.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  /* componentDidUpdate(prevProps) {
    if (this.props.datosItem !== prevProps.datosItem) {
      this.setState({
        itemPrecios: this.props.datosItem
      })
    }
  } */


  eliminarItem() {
    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallecategoria/' + this.props.datosCat.id + '/', {
        method: 'DELETE', // or 'PUT'
        body: JSON.stringify(this.props.datosCat), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(() => console.log('Success removed'))
        .catch(error => console.error('Error:', error))
        .then(() =>
          this.setState({
            modal1: !this.state.modal1,
            modal2: !this.state.modal2,
          })
        )
    } catch (e) {
      console.log(e);
    }
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-google-plus btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-trash-o fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-danger ' + this.props.className} size=''>
          <ModalHeader>
            Advertencia
          </ModalHeader>
          <ModalBody>
            <h5><strong><code style={stylesIcono}>Categoria a eliminar</code></strong></h5>
            <Alert color="info">
              <Col>
                <strong>Categoria: </strong> {this.props.datosCat.cat_descripcion}
              </Col>
            </Alert>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.eliminarItem}><i className="fa fa-remove fa-sm"></i><span><strong>Eliminar Item</strong></span></Button>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-reply fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Categoria Eliminada
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualizaLista(); this.modalsw2(); }} ><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>            
          </ModalFooter>
        </Modal>
      </div >
    );
  }
}
export default EliminaCategoria;

