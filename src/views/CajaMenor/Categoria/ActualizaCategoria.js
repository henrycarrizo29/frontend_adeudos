import React, { Component } from 'react';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'
import {
  Col, Row,
  Button,
  CardBody,
  FormGroup,
  Label,
  Input,
  InputGroup, InputGroupAddon, InputGroupText,
  FormText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';


class ActualizaCategoria extends Component {
  constructor(props) {
    super(props);

    this.state = {
      categoria: {
        cat_descripcion: this.props.datosCat,
      },
      c1: false,
      modal1: false,
      modal2: false,
    };
    this.handleOnControll = this.handleOnControll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
  }

  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2(event) {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.datosCat !== prevProps.datosCat) {
      this.setState({
        categoria: this.props.datosCat
      })
    }
  }

  handleOnControll() {
    const { cat_descripcion } = this.state.categoria

    cat_descripcion === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    if (cat_descripcion !== '') {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallecategoria/' + this.state.categoria.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(this.state.categoria), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.setState({
              modal1: !this.state.modal1,
              modal2: !this.state.modal2,
            })
          )
      } catch (e) {
        console.log(e);
      }
    }
  }


  handleChange(event) {
    let cat = this.state.categoria;
    cat[event.target.name] = event.target.value.toUpperCase();
    this.setState({ cat });
  }


  render() {

    return (
      <div>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" onClick={this.modalsw1}><i className="fa fa-edit fa-lg mt-0"></i></Button>
        <Modal isOpen={this.state.modal1} className={'modal-primary ' + this.props.className} size=''>
          <ModalHeader>
            <i className="fa fa-file fa-lg mt-1"></i> Actualizar Categoria
          </ModalHeader>
          <ModalBody>
            <CardBody>
              <Row>
                <Col xs="12" sm="12">
                  <FormGroup>
                    <InputGroup>
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Categoria:</InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" id="cat_descripcion" name="cat_descripcion" required valid value={this.state.categoria.cat_descripcion} onChange={this.handleChange} />
                    </InputGroup>
                    {this.state.c1 && <FormText color="danger">Ingrese descripcion</FormText>}
                  </FormGroup>
                </Col>
              </Row>
            </CardBody>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw1}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>Mensaje</ModalHeader>
          <ModalBody>
            Categoria Actualizada
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualizaLista(); this.modalsw2(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div >
    );
  }
}
export default ActualizaCategoria;

