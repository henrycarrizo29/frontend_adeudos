import React, { Component } from 'react';
import {
  Col,
  FormGroup,
  Table,
  Badge,
} from 'reactstrap';

/* import ImagenDeposito from './ImagenDeposito';
import EditarComprobante from './EditarComprobante' */


class DetalleFondosAsignados extends Component {
  constructor(props) {
    super(props);

    this.state = {
      listafondos: this.props.listafondos
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.listafondos !== prevProps.listafondos) {
      this.setState({
        listafondos: this.props.listafondos
      })
    }
  }

  render() {

    return (
      <div>
        <FormGroup row>
          <Col md="12">
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Nro</strong></td>
                  <td align="center"><strong>Fecha_As</strong></td>
                  <td align="center"><strong>Asignado ( Bs. )</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="center"><strong>Img</strong></td>
                  <td></td>
                </tr>
              </thead>
             <tbody>
                {this.state.listafondos.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align='center'>{u.id}</td>
                      <td align='center'>{u.fon_fecha}</td>
                      <td align='center'>{u.fon_asignado}</td>
                      <td align='center'>
                      <Badge className="mr-1" color={u.fon_estado === 'CERRADO' ? 'danger' : 'success'} pill>{u.fon_estado}</Badge>
                      </td>                      
                     {/*  <td align='center'><ImagenDeposito depo={u.dep_img_boleta} /></td>
                      <td align="center">
                        <EditarComprobante comprobante={u} actualiza={this.props.actualiza} />
                      </td> */}
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
export default DetalleFondosAsignados;

