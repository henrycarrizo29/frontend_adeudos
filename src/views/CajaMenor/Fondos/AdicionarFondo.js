import React from 'react';
import {
  Col, Row,
  Button,
  Card, CardBody, CardHeader, CardTitle, CardFooter,
  FormGroup, FormText,
  Input, InputGroup, InputGroupAddon, InputGroupText,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
/* import DetalleGasto from './DetalleGasto' */
import DetalleFondosAsignados from './DetalleFondosAsignados'

const stylesIcono = { color: '#4682b4' }

class AdicionarFondo extends React.Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
    this.state = {
      logged_in: localStorage.getItem('token') ? true : false,
      asignarFondo: {
        fon_fecha: '',
        fon_detalle: '',
        fon_asignado: '',
        fon_gastado: '',
        fon_saldo: '',
        fon_factura: '',
        fon_recibo: '',
        fon_archivo: null,
        fon_nota: '',
        fon_solicitante: '',
        fon_categoria: '',
        fon_usuario: '',
        fon_informecajachica: '',
      },
      listaCategoria: [],
      listaPersonal: [],
      listafondosAsignados: [],
      gastosInforme: [],
      estadosFondo: [],
      c0: false,
      c1: false,
      c2: false,
      c3: false,
      c4: false,
      imagen: null,
      file: null,
      modal2: false,
      modal3: false,
      modalFondo: false,
      fecha: date,
      activar:true,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeInformeGasto = this.handleChangeInformeGasto.bind(this);
    this.handleChangeFoto = this.handleChangeFoto.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
    this.handleOnControll = this.handleOnControll.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.modalsw3 = this.modalsw3.bind(this);
    this.modalFondoEstado = this.modalFondoEstado.bind(this);
    this.actualizaGastoAsignado = this.actualizaGastoAsignado.bind(this);
    this.actualizaInformeCC = this.actualizaInformeCC.bind(this);
    this.handleChangeMonto = this.handleChangeMonto.bind(this);
  }

  modalFondoEstado() {
    this.setState({
      modalFondo: !this.state.modalFondo,
    });
    let addFondo = this.state.asignarFondo;
    addFondo["fon_informecajachica"] = ''
    this.setState({ addFondo });
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  modalsw3() {
    this.setState({
      modal3: !this.state.modal3,
      activar:true,
    });
    let inf1 = this.state.asignarFondo;
    let inf2 = this.state.asignarFondo;
    let inf3 = this.state.asignarFondo;
    let inf4 = this.state.asignarFondo;
    let inf5 = this.state.asignarFondo;
    let inf6 = this.state.asignarFondo;
    let inf7 = this.state.asignarFondo;
    let inf8 = this.state.asignarFondo;
    let inf9 = this.state.asignarFondo;
    let inf10 = this.state.asignarFondo;
    let inf11 = this.state.asignarFondo;
    let inf12 = this.state.asignarFondo;
    let inf13 = this.state.asignarFondo;
    inf1['fon_fecha'] = '';
    inf2['fon_detalle'] = '';
    inf3['fon_asignado'] = '';
    inf4['fon_gastado'] = '';
    inf5['fon_saldo'] = '';
    inf6['fon_factura'] = '';
    inf7['fon_recibo'] = '';
    inf8['fon_archivo'] = null;
    inf9['fon_nota'] = '';
    inf10['fon_solicitante'] = '';
    inf11['fon_categoria'] = '';
    inf12['fon_usuario'] = '';
    inf13['fon_informecajachica'] = '';
    this.setState({ inf1, inf2, inf3, inf4, inf5, inf6, inf7, inf8, inf9, inf10, inf11, inf12, inf13 });
    this.setState({
      imagen: null,
      file: null,
    });
  }

  handleChange(event) {
    let addFondo = this.state.asignarFondo;
    addFondo[event.target.name] = event.target.value;
    this.setState({ addFondo });
    console.log(addFondo)    
  }

  handleChangeMonto(event) {
    let addFondoAsignado = this.state.asignarFondo;
    addFondoAsignado[event.target.name] = event.target.value;
    let addFondoGastado = this.state.asignarFondo;
    addFondoGastado['fon_gastado'] = 0;
    let addFondoSaldo = this.state.asignarFondo;
    addFondoSaldo['fon_saldo'] = event.target.value;
    this.setState({ addFondoAsignado, addFondoGastado, addFondoSaldo });    
  }

  async handleChangeInformeGasto(event) {
    let addFondo = this.state.asignarFondo;
    addFondo[event.target.name] = event.target.value;
    this.setState({ addFondo });

    let infFiltrado = this.props.listainformegastoAprobados.filter(item => item.id === parseInt(event.target.value));    

    if (infFiltrado[0].cc_tipodeinforme === 'APERTURA') {
      if (this.state.listafondosAsignados.length === 0) { //por primera vez
        let addFondoAsignado = this.state.asignarFondo;
        addFondoAsignado['fon_asignado'] = 8000;
        let addFondoGastado = this.state.asignarFondo;
        addFondoGastado['fon_gastado'] = 0;
        let addFondoSaldo = this.state.asignarFondo;
        addFondoSaldo['fon_saldo'] = 8000;
        this.setState({ addFondoAsignado, addFondoGastado, addFondoSaldo });
        this.setState({ activar:false});
      } else {
        if (this.state.estadosFondo.length === 0) {     //ya existe un anterior registro y ademas  los fondos estan todos cerrados

          let addFondoAsignado = this.state.asignarFondo;
          addFondoAsignado['fon_asignado'] = 8000;
          let addFondoGastado = this.state.asignarFondo;
          addFondoGastado['fon_gastado'] = 0;
          let addFondoSaldo = this.state.asignarFondo;
          addFondoSaldo['fon_saldo'] = 8000;
          this.setState({ addFondoAsignado, addFondoGastado, addFondoSaldo });
          this.setState({ activar:false});
        } else {
          this.modalFondoEstado()   //no hizo el cierre de  caja chica
        }
      }
    } else {
      try {
        const respuesta7 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoinforme/' + event.target.value + '/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        });
        const json7 = await respuesta7.json();
        this.setState({ gastosInforme: json7 });

        //fon_gastado: (parseFloat(this.state.fondoAbierto[0].fon_gastado) + parseFloat(this.state.solicitarGasto.gas_total)).toString(),

        var totalgasto = json7.reduce((total, { gas_total }) => parseFloat(total) + parseFloat(gas_total), 0);  // sumando todos los gastos enviados
        console.log(totalgasto)

        let addFondoAsignado = this.state.asignarFondo;
        addFondoAsignado['fon_asignado'] = totalgasto;
        let addFondoGastado = this.state.asignarFondo;
        addFondoGastado['fon_gastado'] = (totalgasto - parseFloat(this.state.listafondosAsignados[0].fon_gastado))
        let addFondoSaldo = this.state.asignarFondo;
        addFondoSaldo['fon_saldo'] = (totalgasto + parseFloat(this.state.listafondosAsignados[0].fon_saldo));
        this.setState({ addFondoAsignado, addFondoGastado, addFondoSaldo });

      } catch (error) {
        console.log(error);
      }
    }
  }

  handleOnControll() {
    const { fon_solicitante, fon_fecha, fon_categoria, fon_asignado, fon_informecajachica } = this.state.asignarFondo

    fon_informecajachica === ''
      ? (this.setState({ c0: true }))
      : (this.setState({ c0: false }))

    fon_solicitante === ''
      ? (this.setState({ c1: true }))
      : (this.setState({ c1: false }))

    fon_fecha === ''
      ? (this.setState({ c2: true }))
      : (this.setState({ c2: false }))

    fon_asignado === ''
      ? (this.setState({ c3: true }))
      : (this.setState({ c3: false }))

    fon_categoria === ''
      ? (this.setState({ c4: true }))
      : (this.setState({ c4: false }))


    if (fon_solicitante !== '' && fon_fecha !== '' && fon_asignado !== '' && fon_categoria !== '' && fon_informecajachica !== '') {
      this.setState({
        modal2: !this.state.modal2,
      });
    }
  }

  async componentDidMount() {
    try {
      const respuesta6 = await fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
      const json6 = await respuesta6.json();    
      let user = this.state.asignarFondo;
      user['fon_usuario'] = json6.id
      this.setState({ user });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/listapersonal/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json3 = await respuesta3.json();
      this.setState({ listaPersonal: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listacategoria', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json4 = await respuesta4.json();
      this.setState({ listaCategoria: json4 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listafondos', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json6 = await respuesta6.json();
      this.setState({ listafondosAsignados: json6 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta7 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadofondo/ABIERTO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json7 = await respuesta7.json();
      this.setState({ estadosFondo: json7 });
    } catch (error) {
      console.log(error);
    }

    /*  try {
       const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/APROBADO/', {
         headers: {
           Authorization: `JWT ${localStorage.getItem('token')}`
         }
       });
       const json5 = await respuesta5.json();
       this.setState({ listainformegastoAprobados: json5 });
     } catch (error) {
       console.log(error);
     } */

  }


  handleChangeFoto(event) {    
    this.setState({
      file: URL.createObjectURL(event.target.files[0]),
      imagen: event.target.files[0]
    })
    let img = event.target.files[0]    
  }

  async actualizaInformeCC() {
    try {
      const respuesta7 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.state.asignarFondo.fon_informecajachica + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json7 = await respuesta7.json();
      let informeGasto = {
        cc_tipodeinforme: json7.cc_tipodeinforme,
        cc_receptor: json7.cc_receptor,
        cc_receptorcargo: json7.cc_receptorcargo,
        cc_emisor1: json7.cc_emisor1,
        cc_emisorcargo1: json7.cc_emisorcargo1,
        cc_emisor2: json7.cc_emisor2,
        cc_emisorcargo2: json7.cc_emisorcargo2,
        cc_referencia: json7.cc_referencia,
        cc_fecha: json7.cc_fecha,
        cc_contenido: json7.cc_contenido,
        cc_estado: 'ASIGNADO',
        cc_fecha_aprobación: json7.cc_fecha_aprobación,
        cc_fecha_asignacion: this.state.fecha,
      }

      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + json7.id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(informeGasto),
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));
      } catch (e) {
        console.log(e);
      }

    } catch (error) {
      console.log(error);
    }
  }

  actualizaGastoAsignado() {
    if (this.state.listafondosAsignados.length !== 0) {
      console.log(this.state.estadosFondo[0])
      const { fondoAsignadoAbierto } = this.state.estadosFondo[0]
      console.log(fondoAsignadoAbierto)
      let asignarFondo = {
        fon_fecha: this.state.estadosFondo[0].fon_fecha,
        fon_detalle: this.state.estadosFondo[0].fon_detalle,
        fon_asignado: this.state.estadosFondo[0].fon_asignado,
        fon_gastado: this.state.estadosFondo[0].fon_gastado,
        fon_saldo: this.state.estadosFondo[0].fon_saldo,
        fon_factura: this.state.estadosFondo[0].fon_factura,
        fon_recibo: this.state.estadosFondo[0].fon_recibo,
        fon_nota: this.state.estadosFondo[0].fon_nota,
        fon_solicitante: this.state.estadosFondo[0].fon_solicitante,
        fon_categoria: this.state.estadosFondo[0].fon_categoria,
        fon_usuario: this.state.estadosFondo[0].fon_usuario,
        fon_informecajachica: this.state.estadosFondo[0].fon_informecajachica,
        fon_estado: "CERRADO",
      }
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallefondos/' + this.state.estadosFondo[0].id + '/', {
          method: 'PUT', // or 'PUT'
          body: JSON.stringify(asignarFondo),
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response));
      } catch (e) {
        console.log(e);
      }
    }
  }


  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })
    // e.preventDefault();   //si se llama a este método, la acción predeterminada del evento no se activará.
    const data = new FormData()
    data.append('fon_fecha', this.state.asignarFondo.fon_fecha);
    data.append('fon_detalle', this.state.asignarFondo.fon_detalle);
    data.append('fon_asignado', this.state.asignarFondo.fon_asignado);
    data.append('fon_gastado', this.state.asignarFondo.fon_gastado);
    data.append('fon_saldo', this.state.asignarFondo.fon_saldo);
    data.append('fon_factura', this.state.asignarFondo.fon_factura);
    data.append('fon_recibo', this.state.asignarFondo.fon_recibo);
    data.append('fon_archivo', this.state.imagen);
    data.append('fon_nota', this.state.asignarFondo.fon_nota);
    data.append('fon_solicitante', this.state.asignarFondo.fon_solicitante);
    data.append('fon_categoria', this.state.asignarFondo.fon_categoria);
    data.append('fon_usuario', this.state.asignarFondo.fon_usuario);
    data.append('fon_informecajachica', this.state.asignarFondo.fon_informecajachica);    

    if (this.state.imagen !== null) {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listafondos', {
          method: 'POST', // or 'PUT'
          body: data, // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaGastoAsignado()
          )
          .then(() =>
            this.actualizaInformeCC()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }

    } else {
      try {
        fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listafondos', {
          method: 'POST', // or 'PUT'
          body: JSON.stringify(this.state.asignarFondo), // data can be string or {object}!
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`,
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
          .catch(error => console.error('Error:', error))
          .then(response => console.log('Success:', response))
          .then(() =>
            this.actualizaGastoAsignado()
          )
          .then(() =>
            this.actualizaInformeCC()
          )
          .then(() =>
            this.componentDidMount()
          )
          .then(() =>
            this.setState({
              modal3: !this.state.modal3
            })
          );
      } catch (e) {
        console.log(e);
      }
    }
  }

  render() {

    return (
      <div>
        <Row>
          <Col xs="12" md="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-refresh fa-lg mt-1"></i> Asignar Nuevo Fondo</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="6">
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Informe:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="fon_informecajachica" id="fon_informecajachica" required valid value={this.state.asignarFondo.fon_informecajachica} onChange={this.handleChangeInformeGasto}>
                          <option value=""></option>
                          {this.props.listainformegastoAprobados.map((item1, i) => (
                            <option value={item1.id} key={i}>{item1.id + ': ' + item1.cc_referencia + ' - ' + item1.cc_estado + ' - ' + item1.cc_fecha}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c0 && <FormText color="danger">Seleccione informe</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Asignado a:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="fon_solicitante" id="fon_solicitante" required valid value={this.state.asignarFondo.fon_solicitante} onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaPersonal.map((item1, i) => (
                            <option value={item1.ci} key={i}>{item1.nombres + ' ' + item1.primer_apellido}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-user"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c1 && <FormText color="danger">Ingrese custodio de caja</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Fecha:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="date" id="fon_fecha" name="fon_fecha" value={this.state.asignarFondo.fon_fecha} required valid onChange={this.handleChange} />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-calendar"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c2 && <FormText color="danger">Ingrese fecha </FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Asignar:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="number" id="fon_asignado" name="fon_asignado" value={this.state.asignarFondo.fon_asignado} required valid onChange={this.handleChangeMonto} disabled={this.state.activar}/>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><strong>Bs.</strong></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c3 && <FormText color="danger">Ingrese monto a asignar</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Categoria:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="select" name="fon_categoria" id="fon_categoria" required valid value={this.state.asignarFondo.fon_categoria} onChange={this.handleChange}>
                          <option value=""></option>
                          {this.state.listaCategoria.map((item1, i) => (
                            <option value={item1.id} key={i}>{item1.cat_descripcion}</option>
                          ))}
                        </Input>
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-cubes"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                      {this.state.c4 && <FormText color="danger">Seleccione una categoria</FormText>}
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Detalle:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="fon_detalle" name="fon_detalle" value={this.state.asignarFondo.fon_detalle} onChange={this.handleChange} placeholder="Describa detalle de la categoria" />
                        <InputGroupAddon addonType="append">
                          <InputGroupText><i className="fa fa-cube"></i></InputGroupText>
                        </InputGroupAddon>
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Factura:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="fon_factura" name="fon_factura" value={this.state.asignarFondo.fon_factura} onChange={this.handleChange} placeholder="Codigo de factura" />
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>Recibo:</InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="fon_recibo" name="fon_recibo" value={this.state.asignarFondo.fon_recibo} onChange={this.handleChange} placeholder="Codigo de recibo" />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup>
                      <InputGroup>
                        <Input type="file" onChange={this.handleChangeFoto} />
                      </InputGroup>
                    </FormGroup>
                    <FormGroup row>
                      <Col sm={{ size: 12, offset: 0 }}>
                        <img src={this.state.file} className="img-responsive" width="100%" />
                      </Col>
                    </FormGroup>
                    <FormGroup row>
                      <Col sm={{ size: 8, order: 2, offset: 3 }}>
                        <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="submit" onClick={this.handleOnControll}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Asignar Fondo</strong></span></Button>
                      </Col>
                    </FormGroup>
                  </Col>
                  <Col md={{ size: 6, offset: 0 }}>
                    <h4><strong><code style={stylesIcono}>Fondos Asignados</code></strong></h4>
                    <DetalleFondosAsignados listafondos={this.state.listafondosAsignados} actualiza={this.actualiza} />
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
                  <ModalHeader>
                    <i className="fa fa-dollar fa-lg mt-1"></i> Datos Fondo a Asignar
                  </ModalHeader>
                  <ModalBody>
                    <FormGroup row>
                      <Col xs="12" md="12">
                        <strong>Asignado a: </strong> {this.state.asignarFondo.fon_solicitante}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Fecha: </strong> {this.state.asignarFondo.fon_fecha}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Efectivo: </strong> {this.state.asignarFondo.fon_asignado}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Categoria: </strong> {this.state.asignarFondo.fon_categoria}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Detalle: </strong> {this.state.asignarFondo.fon_detalle}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Factura: </strong> {this.state.asignarFondo.fon_factura}
                      </Col>
                      <Col xs="12" md="12">
                        <strong>Recibo: </strong> {this.state.asignarFondo.fon_recibo}
                      </Col>
                    </FormGroup>
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Asignar Fondos</strong></span></Button>
                    <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modal3} className={'modal-success ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Fondos Asignados
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw3(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>

                <Modal isOpen={this.state.modalFondo} className={'modal-primary ' + this.props.className} size='sm'>
                  <ModalHeader>Mensaje</ModalHeader>
                  <ModalBody>
                    Para la apertura de fondos debe realizar el cierre de caja chica o seleccione otro documento
                  </ModalBody>
                  <ModalFooter>
                    <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.modalFondoEstado}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
                  </ModalFooter>
                </Modal>
              </CardFooter>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AdicionarFondo;