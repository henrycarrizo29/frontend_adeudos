import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  FormGroup,
  Input,
  Label,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class RechazarInforme extends Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

    this.state = {
      modal1: false,
      modal2: false,
      fechaRechazo: date,
      obs: {
        observacion: '',
      },
    };
    this.modalsw1 = this.modalsw1.bind(this);
    this.modalsw2 = this.modalsw2.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }
  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  modalsw2() {
    this.setState({
      modal2: !this.state.modal2,
    });
  }

  handleChange3(event) {
    let ob = this.state.obs
    ob[event.target.name] = event.target.value
    this.setState({ ob })    
  }

  handleOnSubmit(e) {
    this.setState({
      modal2: !this.state.modal2
    })    
    let nuevoInforme = {
      cc_tipodeinforme: this.props.itemInforme.cc_tipodeinforme,
      cc_receptor: this.props.itemInforme.cc_receptor,
      cc_receptorcargo: this.props.itemInforme.cc_receptorcargo,
      cc_emisor1: this.props.itemInforme.cc_emisor1,
      cc_emisorcargo1: this.props.itemInforme.cc_emisorcargo1,
      cc_emisor2: this.props.itemInforme.cc_emisor2,
      cc_emisorcargo2: this.props.itemInforme.cc_emisorcargo2,
      cc_referencia: this.props.itemInforme.cc_referencia,
      cc_fecha: this.props.itemInforme.cc_fecha,
      cc_contenido: this.props.itemInforme.cc_contenido,
      cc_estado: 'RECHAZADO',
      cc_observacion: this.state.obs.observacion,
      cc_fecha_aprobación: this.state.fechaRechazo,
    }

    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.itemInforme.id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(nuevoInforme), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))
        .then(() =>
          this.setState({
            modal1: !this.state.modal1
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="danger" onClick={this.modalsw2}><i className="fa fa-close fa-lg mt-0"></i>Rechazar</Button>
        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Informe Rechazado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>

        <Modal isOpen={this.state.modal2} className={'modal-primary ' + this.props.className} size=''>
          <ModalHeader>
            Describa Observacion del Informe
          </ModalHeader>
          <ModalBody>
            <form>
              <FormGroup row>
                <Col md="4">
                  <Label htmlFor="text-input">Observacion:</Label>
                </Col>
                <Col md="8">
                  <Input type="text" id="observacion" name="observacion" value={this.state.observacion} onChange={this.handleChange3} />
                </Col>
              </FormGroup>
            </form>
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={this.handleOnSubmit}><i className="fa fa-floppy-o fa-sm"></i><span><strong>Registrar</strong></span></Button>
            <Button className="btn-google-plus btn-brand mr-1 mb-1" size="" type="" onClick={this.modalsw2}><i className="fa fa-close fa-sm"></i><span><strong>Cancelar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default RechazarInforme;