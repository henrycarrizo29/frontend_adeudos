import React, { Component } from 'react';
import {
  Col, Row,
  TabContent, TabPane,
  Nav, NavItem, NavLink,
  Card, CardBody, CardHeader, CardTitle,
  FormGroup,
  Badge,
} from 'reactstrap';
import classnames from 'classnames';
import AdicionarFondo from './AdicionarFondo'
import VerSolicitudesCC from './VerSolicitudesCC'
import SolicitudesRevisadas from './SolicitudesRevisadas'

const stylesCompromisos = { color: '#20a8d8' }
const stylesLiquidaciones = { color: '#ff7f50' }
const stylesDepositos = { color: '#4dbd74' }
const stylesInformeSocial = { color: '#e83e8c' }
const stylesInformeCompromiso = { color: '#63c2de' }

class Fondos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      solicitudesCC: [],
      solicitudesrevisadas: [],
      listainformegastoAprobados:[],
      activeTab: '4',
    };

    this.cambiarTabla = this.cambiarTabla.bind(this);
    this.actualiza = this.actualiza.bind(this);
  }

  cambiarTabla(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  actualiza() {
    this.componentDidMount()
  }

  async componentDidMount() {
    try {
      const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/listainformecajachica', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json5 = await respuesta5.json();
      let itemFiltrado = json5.filter(item => item.cc_estado === 'APROBADO' || item.cc_estado === 'RECHAZADO' || item.cc_estado === 'ASIGNADO');
      this.setState({ solicitudesrevisadas: itemFiltrado });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/ENVIADO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json6 = await respuesta6.json();
      this.setState({ solicitudesCC: json6 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta7 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscaestadoinformecajachica/APROBADO/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json7 = await respuesta7.json();
      this.setState({ listainformegastoAprobados: json7 });
    } catch (error) {
      console.log(error);
    }

  }

  render() {
    
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="fa fa-refresh fa-lg mt-1"></i> Fondos</strong></CardTitle>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="12" md="12" className="mb-4">
                    <Nav tabs>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '3' })} onClick={() => { this.cambiarTabla('3'); }} >
                          <span className={this.state.activeTab === '3' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-refresh fa-lg mt-1" style={stylesDepositos}></i> Fondos
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '4' })} onClick={() => { this.cambiarTabla('4'); }} >
                          <span className={this.state.activeTab === '4' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-file-text fa-lg mt-1" style={stylesInformeSocial}></i> Solicitudes Caja Chica
                        </NavLink>
                      </NavItem>
                      <NavItem>
                        <NavLink className={classnames({ active: this.state.activeTab === '5' })} onClick={() => { this.cambiarTabla('5'); }} >
                          <span className={this.state.activeTab === '5' ? '' : 'd-none'}></span>{'\u00A0'}<i className="fa fa-check-square fa-lg mt-1" style={stylesInformeCompromiso}></i> Solicitudes Revisadas
                        </NavLink>
                      </NavItem>
                    </Nav>

                    <TabContent activeTab={this.state.activeTab}>
                      <TabPane tabId="3">
                        {<AdicionarFondo listainformegastoAprobados={this.state.listainformegastoAprobados} actualiza={this.actualiza}/>}
                      </TabPane>
                      <TabPane tabId="4">
                        {<VerSolicitudesCC solicitudesCC={this.state.solicitudesCC} actualiza={this.actualiza} />}
                      </TabPane>
                      <TabPane tabId="5">
                        {<SolicitudesRevisadas solicitudesrevisadas={this.state.solicitudesrevisadas} actualiza={this.actualiza} />}
                      </TabPane>
                    </TabContent>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default Fondos;
