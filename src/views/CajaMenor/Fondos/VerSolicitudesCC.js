import React, { Component } from 'react';
import {
  Col, Row,
  Table,
  Badge,
  ButtonGroup,
} from 'reactstrap';

import PDFinformeGastos from '../Gastos/PDFinformeGastos'
import AprobarInforme from './AprobarInforme'
import RechazarInforme from './RechazarInforme'

const stylesIcono = { color: '#4682b4' }

class VerSolicitudesCC extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };
  }

  render() {

    return (
      <div>
        <Row>
          <Col sm={{ size: 12, offset: 0 }}>
            <Table responsive hover size="sm">
              <thead>
                <tr>
                  <td align="center"><strong>Cod.</strong></td>
                  <td align="center"><strong>Informe</strong></td>
                  <td align="left"><strong>Emisor</strong></td>
                  <td align="left"><strong>Referencia</strong></td>
                  <td align="center"><strong>Estado</strong></td>
                  <td align="center"><strong>FechaInforme</strong></td>
                  <td align="center"><strong>Archivo</strong></td>
                  <td align="center"><strong>Acciones</strong></td>
                </tr>
              </thead>
              <tbody>
                {this.props.solicitudesCC.map((u, i) => {
                  return (
                    <tr key={i}>
                      <td align="center">{u.id}</td>
                      <td align="left">{u.cc_tipodeinforme}</td>
                      <td align="left">{u.cc_emisor1}</td>
                      <td align="left">{u.cc_referencia}</td>
                      <td align="center">
                        <Badge className="mr-1" color={u.cc_estado === 'RECHAZADO' ? 'danger' : u.cc_estado === 'EN_INFORME' ? 'primary' : u.cc_estado === 'ENVIADO' ? 'warning' : 'success'} pill>{u.cc_estado}</Badge>
                      </td>
                      <td align="center">{u.cc_fecha}</td>
                      <td align="center">
                        <PDFinformeGastos idinforme={u.id} />
                      </td>
                      <td align="center">
                        <ButtonGroup size="sm">
                          <AprobarInforme itemInforme={u} actualiza={this.props.actualiza} />&nbsp;&nbsp;
                          <RechazarInforme itemInforme={u} actualiza={this.props.actualiza} />
                        </ButtonGroup>
                      </td>
                      <td>
                      </td>
                    </tr>
                  )
                })}
              </tbody>
            </Table>
          </Col>
        </Row>
      </div>
    );
  }
}
export default VerSolicitudesCC;