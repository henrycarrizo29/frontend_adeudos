import React, { Component } from 'react';
import {
  Col, Row,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';

class AprobarInforme extends Component {
  constructor(props) {
    super(props);
    var hoy = new Date(),
      date = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();

    this.state = {
      modal1: false,
      fechaAprobacion: date,
      gastosEnviados: [],
    };
    this.modalsw1 = this.modalsw1.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }
  modalsw1() {
    this.setState({
      modal1: !this.state.modal1,
    });
  }

  async handleOnSubmit(e) {
    let nuevoInforme = {
      cc_tipodeinforme: this.props.itemInforme.cc_tipodeinforme,
      cc_receptor: this.props.itemInforme.cc_receptor,
      cc_receptorcargo: this.props.itemInforme.cc_receptorcargo,
      cc_emisor1: this.props.itemInforme.cc_emisor1,
      cc_emisorcargo1: this.props.itemInforme.cc_emisorcargo1,
      cc_emisor2: this.props.itemInforme.cc_emisor2,
      cc_emisorcargo2: this.props.itemInforme.cc_emisorcargo2,
      cc_referencia: this.props.itemInforme.cc_referencia,
      cc_fecha: this.props.itemInforme.cc_fecha,
      cc_contenido: this.props.itemInforme.cc_contenido,
      cc_estado: 'APROBADO',
      cc_fecha_aprobación: this.state.fechaAprobacion,
    }

    const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/buscagastoinforme/' + this.props.itemInforme.id + '/', {
      headers: {
        Authorization: `JWT ${localStorage.getItem('token')}`
      }
    });
    const json3 = await respuesta3.json();
    this.setState({ gastosEnviados: json3 });


    try {
      fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detalleinformecajachica/' + this.props.itemInforme.id + '/', {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(nuevoInforme), // data can be string or {object}!
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`,
          'Content-Type': 'application/json'
        }
      }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))

        .then(() =>
          this.state.gastosEnviados.map((data, i) => {
            let nuevoGasto = {
              gas_fecha: data.gas_fecha,
              gas_total: data.gas_total,
              gas_detalle: data.gas_detalle,
              gas_factura: data.gas_factura,
              gas_recibo: data.gas_recibo,
              gas_nota: data.gas_nota,
              gas_estado: 'CERRADO',
              gas_solicitante: data.gas_solicitante,
              gas_fondos: data.gas_fondos,
              gas_categoria: data.gas_categoria,
              gas_informe_cc: data.gas_informe_cc,
            }

            try {
              fetch('http://0.0.0.0:8000/adeudos/caja_menor_gastos/v1/detallegastos/' + data.id + '/', {
                method: 'PUT', // or 'PUT'
                body: JSON.stringify(nuevoGasto), // data can be string or {object}!
                headers: {
                  Authorization: `JWT ${localStorage.getItem('token')}`,
                  'Content-Type': 'application/json'
                }
              }).then(res => res.json())
                .catch(error => console.error('Error:', error))
                .then(response => console.log('Success:', response))
            } catch (e) {
              console.log(e);
            }
          })
        )
        .then(() =>
          this.setState({
            modal1: !this.state.modal1
          })
        );
    } catch (e) {
      console.log(e);
    }
  }

  render() {

    return (
      <div>
        <Button size="sm" color="success" onClick={this.handleOnSubmit}><i className="fa fa-check fa-lg mt-0"></i>Aprobar</Button>
        <Modal isOpen={this.state.modal1} className={'modal-success ' + this.props.className} size='sm'>
          <ModalHeader>
            Mensaje
          </ModalHeader>
          <ModalBody>
            Informe Aprobado
          </ModalBody>
          <ModalFooter>
            <Button className="btn-vine btn-brand mr-1 mb-1" size="" type="" onClick={(event) => { this.props.actualiza(); this.modalsw1(); }}><i className="fa fa-close fa-sm"></i><span><strong>Cerrar</strong></span></Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default AprobarInforme;