import React, { Component } from 'react';
import {
  Card, CardBody, CardHeader, CardTitle,
  Col,
  Row,
  Table,
  Pagination, PaginationItem, PaginationLink, Button,
  FormGroup, Input, Label, Badge,
} from 'reactstrap';
import CompromisosFirmados from './CompromisosFirmados'
import PDFcompromiso from './PDFcompromisos';

const stylesIcono = { color: '#4682b4' }
const stylesUsuarios = { color: '#0288d1' }
const stylesFecha = { color: '#2e7d32' }
const stylesUsuario = { color: '#ef6c00' }

class ListaCompromisos extends Component {
  constructor(props) {
    super(props);

    this.state = {
      compromisos: [],
      compromisosFiltrados: [],
      paginaActual: 0,
      elementosPag: 10,
      checked: [false, false],
      estado: '',
    };
    this.handleClick = this.handleClick.bind(this)
    this.handleCheck = this.handleCheck.bind(this)
  }

  handleCheck(e) {
    const { value, name, id } = e.target;
    var a = new Array(2).fill(false)
    switch (id) {
      case "1":
        a[0] = true
        let cancelados = this.state.compromisos.filter(cdp => cdp.com_estado === 'CANCELADO');
        this.setState({ compromisosFiltrados: cancelados })
        break;
      default:
        a[1] = true
        let pendientes = this.state.compromisos.filter(cdp => cdp.com_estado === 'PENDIENTE');
        this.setState({ compromisosFiltrados: pendientes })
    }
    this.setState({
      checked: a,
      estado: value,
    })
  }

  handleClick(e, index) {
    e.preventDefault();
    this.setState({
      paginaActual: index
    });
  }

  // obteniendo la lista de compromisos registrados
  async componentDidMount() {
    try {
      const respuesta1 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/listacompromiso', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      });
      const json1 = await respuesta1.json();
      let jsonx = json1
      if (this.props.f1 !== '' && this.props.f2 !== '') {
        jsonx = json1.filter((item, indice) => {
          return item.com_fecha >= this.props.f1 && item.com_fecha <= this.props.f2;
        });

      }
      this.setState({
        compromisos: jsonx,
        compromisosFiltrados: jsonx
      });
    } catch (e) {
      console.log(e);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.f1 !== prevProps.f1 || this.props.f2 !== prevProps.f2) {
      var a = new Array(2).fill(false)
      this.setState({
        checked: a,
        estado: '',
      })
      this.componentDidMount();
    }
  }


  render() {
    const compromisoslista = this.state.compromisosFiltrados
    const contarPaginas = Math.ceil(compromisoslista.length / this.state.elementosPag);
    const { paginaActual, elementosPag } = this.state;
    const fin = (paginaActual + 1) * elementosPag;
    const inicio = fin - elementosPag;
    const listaComp = compromisoslista.slice(inicio, fin);

    return (
      <div>
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader tag="h5">
                <CardTitle><strong><i className="icon-list icons font-3xl"></i> Compromisos de Pago</strong>
                  <div className="card-header-actions">
                    <form>
                      <FormGroup row>
                        <Col md="12">
                          <FormGroup check inline>
                            <Input onChange={this.handleCheck} checked={this.state.checked[0]} className="form-check-input" type="radio" id="1" name="busqueda" value="CANCELADO" />
                            <Label className="form-check-label" check htmlFor="radio1"> <Badge className="mr-1" color="success" pill>CANCELADOS</Badge></Label>
                          </FormGroup>
                          <FormGroup check inline>
                            <Input onChange={this.handleCheck} checked={this.state.checked[1]} className="form-check-input" type="radio" id="2" name="busqueda" value="PENDIENTE" />
                            <Label className="form-check-label" check htmlFor="radio2"> <Badge className="mr-1" color="danger" pill>PENDIENTES</Badge></Label>
                          </FormGroup>
                          <PDFcompromiso estadoCDP={this.state.estado} f1={this.props.f1} f2={this.props.f2} />
                        </Col>
                      </FormGroup>
                    </form>
                  </div>
                </CardTitle>
              </CardHeader>
              <CardBody>
                <Table responsive size="sm" hover>
                  <thead>
                    <tr>
                      <td align="center"><strong>CDP</strong></td>
                      <td align="center"><strong>C.I. Est.</strong></td>
                      <td align="left"><strong>Estudiante</strong></td>
                      <td align="left"><strong>Carrera</strong></td>
                      <td align="left"><strong>Garante</strong></td>
                      <td align="center"><strong>Parentesco</strong></td>
                      <td align="center"><strong>Fecha_Comp</strong></td>
                      <td align="center"><strong>Estado</strong></td>
                      <td align="center"><strong>Informe</strong></td>
                      <td align="center"><strong>Obs.</strong></td>
                      {/* <td align="center"><strong>Accion</strong></td> */}
                    </tr>
                  </thead>
                  <tbody>
                    {listaComp.map((cdp, i) => (
                      <CompromisosFirmados datoscom={cdp} key={i} />
                    ))}
                  </tbody>
                </Table>
                <Row>
                  <Col md="10">
                    <Pagination size="">
                      {[...Array(contarPaginas)].map((page, i) =>
                        <PaginationItem active={i === paginaActual} key={i}>
                          <PaginationLink onClick={e => this.handleClick(e, i)} href="#">
                            {i + 1}
                          </PaginationLink>
                        </PaginationItem>
                      )}
                    </Pagination>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default ListaCompromisos;
