import React, { Component } from 'react';
import { Popover, PopoverBody, PopoverHeader, Button, Badge } from 'reactstrap';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable'

class PopoverItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      popoverOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen,
    });
  }

  render() {
    return (
      <span>
        <Button size="sm" className="btn-twitter btn-brand icon mr-1 mb-1" id={'Popover-' + this.props.id} onClick={this.toggle}><i className="fa fa-file-text fa-lg mt-0"></i></Button>
        <Popover placement="left" isOpen={this.state.popoverOpen} target={'Popover-' + this.props.id} toggle={this.toggle}>
          <PopoverHeader>Observaciones</PopoverHeader>
          <PopoverBody>{this.props.itemObs}</PopoverBody>
        </Popover>
      </span>
    );
  }
}


class CompromisosFirmados extends Component {
  constructor(props) {
    super(props)

    this.state = {
      estudiante: {},
      garante: {},
      medico: {},
      carrera: {},
      facultad: {},
      parentesco: {},
      dia: '',
      mes: '',
      anio: '',
    }
  }

  async componentDidMount() {
    try {
      const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detalleestudiante/' + this.props.datoscom.com_estudiante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json2 = await respuesta2.json();
      this.setState({ estudiante: json2 });

      try {
        const respuesta4 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallecarrera/' + this.state.estudiante.Carrera, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          },
        });
        const json4 = await respuesta4.json();
        this.setState({ carrera: json4[0] });

        try {
          const respuesta5 = await fetch('http://0.0.0.0:8000/adeudos/estudiante/v1/detallefacultad/' + this.state.carrera.facultad, {
            headers: {
              Authorization: `JWT ${localStorage.getItem('token')}`
            },
          });
          const json5 = await respuesta5.json();
          this.setState({ facultad: json5[0] });
        } catch (error) {
          console.log(error);
        }
      } catch (error) {
        console.log(error);
      }
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/garante/v1/detallegarante/' + this.props.datoscom.com_garante + '/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json3 = await respuesta3.json();
      this.setState({ garante: json3 });
    } catch (error) {
      console.log(error);
    }

    try {
      const respuesta6 = await fetch('http://0.0.0.0:8000/adeudos/compromiso_de_pago/v1/detalleparentesco/' + this.props.datoscom.com_parentesco, {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        },
      });
      const json6 = await respuesta6.json();
      this.setState({ parentesco: json6[0] });
    } catch (error) {
      console.log(error);
    }

    var fe = this.props.datoscom.com_fecha;
    var fecha = fe.split("-");
    this.setState({
      dia: fecha[2],
      mes: fecha[1],
      anio: fecha[0],
    })
  }

  render() {
    const tipoColor = (this.props.datoscom.com_estado === 'CANCELADO') ? 'success' : 'danger'
    return (
      <tr>
        <td align="center">
          {this.props.datoscom.id}
        </td>
        <td>
          {this.state.estudiante.ci}
        </td>
        <td>
          {this.state.estudiante.nombres + ' ' + this.state.estudiante.primer_apellido + ' ' + this.state.estudiante.segundo_apellido}
        </td>
        <td>
          {this.state.carrera.carrera}
        </td>
        <td>
          {this.state.garante.gar_nombre + ' ' + this.state.garante.gar_apellido1 + ' ' + this.state.garante.gar_apellido2}
        </td>
        <td align="center">
          {this.state.parentesco.parentesco}
        </td>
        <td align="center">
          {this.props.datoscom.com_fecha}
        </td>
        <td align="center">
          <Badge className="mr-1" href="#/liquidaciones" color={tipoColor} pill>{this.props.datoscom.com_estado}</Badge>
        </td>
        <td align="center">
          {this.props.datoscom.com_nroinforme}
        </td>
        <td align="center">
          <PopoverItem itemObs={this.props.datoscom.com_observacion} id={this.props.datoscom.id} />
          {/*  {this.props.datoscom.com_observacion} */}
        </td>
      </tr>
    )
  }
}

export default CompromisosFirmados;
