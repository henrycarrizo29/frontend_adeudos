import React from 'react';

const Breadcrumbs = React.lazy(() => import('./views/Base/Breadcrumbs'));
const Cards = React.lazy(() => import('./views/Base/Cards'));
const Carousels = React.lazy(() => import('./views/Base/Carousels'));
const Collapses = React.lazy(() => import('./views/Base/Collapses'));
const Dropdowns = React.lazy(() => import('./views/Base/Dropdowns'));
const Forms = React.lazy(() => import('./views/Base/Forms'));
const Jumbotrons = React.lazy(() => import('./views/Base/Jumbotrons'));
const ListGroups = React.lazy(() => import('./views/Base/ListGroups'));
const Navbars = React.lazy(() => import('./views/Base/Navbars'));
const Navs = React.lazy(() => import('./views/Base/Navs'));
const Paginations = React.lazy(() => import('./views/Base/Paginations'));
const Popovers = React.lazy(() => import('./views/Base/Popovers'));
const ProgressBar = React.lazy(() => import('./views/Base/ProgressBar'));
const Switches = React.lazy(() => import('./views/Base/Switches'));
const Tables = React.lazy(() => import('./views/Base/Tables'));
const Tabs = React.lazy(() => import('./views/Base/Tabs'));
const Tooltips = React.lazy(() => import('./views/Base/Tooltips'));
const BrandButtons = React.lazy(() => import('./views/Buttons/BrandButtons'));
const ButtonDropdowns = React.lazy(() => import('./views/Buttons/ButtonDropdowns'));
const ButtonGroups = React.lazy(() => import('./views/Buttons/ButtonGroups'));
const Buttons = React.lazy(() => import('./views/Buttons/Buttons'));
const Charts = React.lazy(() => import('./views/Charts'));
//const Dashboard = React.lazy(() => import('./views/Dashboard'));
const CoreUIIcons = React.lazy(() => import('./views/Icons/CoreUIIcons'));
const Flags = React.lazy(() => import('./views/Icons/Flags'));
const FontAwesome = React.lazy(() => import('./views/Icons/FontAwesome'));
const SimpleLineIcons = React.lazy(() => import('./views/Icons/SimpleLineIcons'));
const Alerts = React.lazy(() => import('./views/Notifications/Alerts'));
const Badges = React.lazy(() => import('./views/Notifications/Badges'));
const Modals = React.lazy(() => import('./views/Notifications/Modals'));
const Colors = React.lazy(() => import('./views/Theme/Colors'));
const Typography = React.lazy(() => import('./views/Theme/Typography'));
const Widgets = React.lazy(() => import('./views/Widgets/Widgets'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

const Dashboard = React.lazy(() => import('./views/Dashboard/Dashboard'));
const Dashboard1 = React.lazy(() => import('./views/Dashboard/Dashboard1'));
const Dashboard2 = React.lazy(() => import('./views/Dashboard/Dashboard2'));
const Dashboard3 = React.lazy(() => import('./views/Dashboard/Dashboard3'));

// COMPROMISOS
const Garante = React.lazy(() => import('./views/Compromisos/Garante/Garante'));
const Compromiso = React.lazy(() => import('./views/Compromisos/Compromiso/Compromiso'));
const VerCompromiso = React.lazy(() => import('./views/Compromisos/VerCompromiso/VerCompromiso'));
const VerGarante = React.lazy(() => import('./views/Compromisos/Garante/VerGarante'));
const ActualizarDireccion = React.lazy(() => import('./views/Compromisos/Garante/ActualizarDireccion'));
const ActualizarTelefono = React.lazy(() => import('./views/Compromisos/Garante/ActualizarTelefono'));
const Busquedas = React.lazy(() => import('./views/Compromisos/Busquedas/BuscarCompromiso'));

// EMERGENCIAS
const HojaEvolucion = React.lazy(() => import('./views/Emergencias/HojaEvolucionUno/HojaEvolucionUno'));
const VerHojaEvolucion = React.lazy(() => import('./views/Emergencias/VerHojaEvolucion/VerHojaEvolucion'));

// ADEUDOS
const ListaDePrecios = React.lazy(() => import('./views/Liquidaciones/ListaDePrecios/ListaDePrecios'));
const Liquidaciones = React.lazy(() => import('./views/Liquidaciones/GestionarLiquidaciones/GestionarLiquidaciones'));

// CERTIFICADOS
const Certificados = React.lazy(() => import('./views/Certificados/Certificados'));

// VER COMPROMISOS CONTADOR
const VerCompromisoCont = React.lazy(() => import('./views/VerCompromisosCont/VerCompromisosCont'));

// VER CERTIFICADOS CONTADOR
const VerCertificados = React.lazy(() => import('./views/VerCertificados/VerCertificados'));

// CAJA MENOR
const Fondos = React.lazy(() => import('./views/CajaMenor/Fondos/Fondos'));
const Categoria = React.lazy(() => import('./views/CajaMenor/Categoria/Categoria'));
const Gastos = React.lazy(() => import('./views/CajaMenor/Gastos/Gastos'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Inicio' },
  { path: '/dashboard', name: 'Pagina Principal', component: Dashboard },
  { path: '/dashboard1', name: 'Pagina Principal', component: Dashboard1 },
  { path: '/dashboard2', name: 'Pagina Principal', component: Dashboard2 },
  { path: '/dashboard3', name: 'Pagina Principal', component: Dashboard3 },

  { path: '/compromiso', name: 'Compromiso de Pago', component: Compromiso },
  { path: '/garante', name: 'Garante', component: Garante },
  { path: '/actualizardireccion/:gar_ci', name: 'Actualizar Garante', component: ActualizarDireccion },
  { path: '/actualizarTelefono/:gar_ci', name: 'Actualizar Garante', component: ActualizarTelefono },
  { path: '/vercompromiso', name: 'Ver Compromiso', component: VerCompromiso },
  { path: '/vergarantes', name: 'Ver Garante', component: VerGarante },
  { path: '/buscarcompromiso', name: 'Buscar Compromiso', component: Busquedas },

  { path: '/hojaevolucion', name: 'Hoja Evolucion', component: HojaEvolucion },
  { path: '/verhojaevolucion', name: 'Ver Hoja Evolucion', component: VerHojaEvolucion },

  { path: '/listadeprecios', name: 'Lista de Precios', component: ListaDePrecios },
  { path: '/liquidaciones', name: 'Liquidaciones', component: Liquidaciones },

  { path: '/certificadosdna', name: 'Certificados', component: Certificados },

  { path: '/vercompromisocont', name: 'Lista Compromisos', component: VerCompromisoCont },

  { path: '/listacertificados', name: 'Lista Certificados', component: VerCertificados },

  { path: '/fondos', name: 'Asignacion de fondos', component: Fondos },
  { path: '/categoria', name: 'Categoria', component: Categoria },
  { path: '/gastos', name: 'Gastos', component: Gastos },

  { path: '/theme', exact: true, name: 'Theme', component: Colors },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', exact: true, name: 'Base', component: Cards },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/forms', name: 'Forms', component: Forms },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tables', name: 'Tables', component: Tables },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', exact: true, name: 'Buttons', component: Buttons },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Button Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/notifications', exact: true, name: 'Notifications', component: Alerts },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/widgets', name: 'Widgets', component: Widgets },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/users', exact: true, name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes;
