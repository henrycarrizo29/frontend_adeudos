import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
import navigation from '../../_nav';
import navigation1 from '../../_nav1';
import navigation2 from '../../_nav2';
import navigation3 from '../../_nav3';
// routes config
import routes from '../../routes';

const DefaultAside = React.lazy(() => import('./DefaultAside'));
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  constructor(props) {
    super(props);
    this.state = {
      logged_in: localStorage.getItem('token') ? true : false,
      username: {},
      usuario: {},
      unidad: '',
    };
  }

  async componentDidMount() {
    try {
      if (this.state.logged_in) {
        const respuesta1 = await fetch('http://localhost:8000/core/current_user/', {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json1 = await respuesta1.json();
        this.setState({ username: json1 });

        const respuesta2 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/detalleusuario/' + json1.id, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json2 = await respuesta2.json();
        this.setState({ usuario: json2[0] });

        const respuesta3 = await fetch('http://0.0.0.0:8000/adeudos/personal/v1/detalleunidad/' + this.state.usuario.unidad, {
          headers: {
            Authorization: `JWT ${localStorage.getItem('token')}`
          }
        })
        const json3 = await respuesta3.json();
        this.setState({ unidad: json3[0].descripcion });

      } else {
        window.location.href = '#/Login/'
      }
    } catch (error) {
      console.log(error);
    }
  }

  signOut(e) {
    e.preventDefault()
    localStorage.clear()
    this.setState({ logged_in: false });
    window.location.href = '#/Login'
    /* localStorage.removeItem('token');
    this.props.history.push('/login') */
  }

  render() {

    let roles = null
    let dashboard = null
    switch (this.state.unidad) {
      case 'EMERGENCIA':
        roles = navigation2
        dashboard = "/dashboard2"
        break;
      case "CONTABILIDAD":
        roles = navigation1
        dashboard = "/dashboard1"
        break;
      case "ENFERMERÍA":
        roles = navigation3
        dashboard = "/dashboard3"
        break;
      default:
        roles = navigation
        dashboard = "/dashboard4"
    }

    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader datosUsuario={this.state.usuario} datosUsername={this.state.username} datosUnidad={this.state.unidad} onLogout={e => this.signOut(e)} />
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={roles} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>

          <main className="main">
            <AppBreadcrumb appRoutes={routes} />
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                  return route.component ? (
                    <Route
                      key={idx}
                      path={route.path}
                      exact={route.exact}
                      name={route.name}
                      render={props => (
                        <route.component {...props} />
                      )} />
                  ) : (null);
                })}
                {this.state.username.username === '' ? <Redirect from="/" to="/login" /> : <Redirect from="/" to={dashboard} />}
              </Switch>
            </Container>
          </main>

          <AppAside fixed>
            <Suspense fallback={this.loading()}>
              <DefaultAside />
            </Suspense>
          </AppAside>
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
