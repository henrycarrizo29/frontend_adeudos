import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppHeaderDropdown, AppSidebarToggler } from '@coreui/react';
/* const styles = { color: '#191970' }
const styles0 = { color: '#0288d1' } */
const styles1 = { color: '#5f9ea0' }
const styles2 = { color: '#6495ed' }

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {

  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <NavLink to="#" className="nav-link" ><h5><strong>SI-GESHO-PROMES</strong></h5></NavLink>
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        {/* <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Users</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link">Settings</NavLink>
          </NavItem>
        </Nav> */}

        <Nav className="ml-auto" navbar>
          <NavItem className="px-1">
            <h6 style={styles1}><strong>{this.props.datosUsuario.nombres + ' ' + this.props.datosUsuario.primer_apellido}</strong></h6>
          </NavItem>
          <NavItem className="px-1">
            <h6 style={styles2}><strong>{this.props.datosUnidad}</strong></h6>
          </NavItem>

          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'../../assets/img/avatars/user.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center">
                <strong>Cuenta</strong>
              </DropdownItem>
              <DropdownItem>
                <div className="message">
                  <div className="py-0 pb-1 mr-3 float-left">
                    <div className="avatar">
                      <img src={'../../assets/img/avatars/user.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
                      <span className="avatar-status badge-success"></span>
                    </div>
                  </div>
                  <div >
                    <small className="text-truncate font-weight-bold">{this.props.datosUsername.username}</small>
                  </div>
                </div>
              </DropdownItem>
              <DropdownItem onClick={e => this.props.onLogout(e)}><i className="fa fa-lock"></i> Cerrar sesión</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>

        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
