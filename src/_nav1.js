export default {
  items: [
    {
      title: true,
      name: 'FONDOS CAJA CHICA',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Asignar fondos',
      url: '/fondos',
      icon: 'fa fa-dollar fa-lg mt-1',
    },
    {
      title: true,
      name: 'GASTOS CAJA CHICA',
      wrapper: {
        element: '',
        attributes: {},
      },
    },

    {
      name: 'Categoria - Gastos',
      url: '/categoria',
      icon: 'fa fa-cubes fa-lg mt-1',
    },
    {
      name: 'Gastos',
      url: '/gastos',
      icon: 'fa fa-dollar fa-lg mt-1',
    },

    {
      title: true,
      name: 'CERTIFICADOS - ADEUDOS',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Certificados',
      url: '/certificadosdna',
      icon: 'fa fa-id-card fa-lg mt-1',
    },
    {
      name: 'Ver Cerificados',
      url: '/listacertificados',
      icon: 'fa fa-newspaper-o fa-lg mt-1',
    },
    /* {
      name: 'Liquidaciones',
      url: '/liquidaciones',
      icon: 'fa fa-list-alt fa-lg mt-1',
    },
    {
      name: 'Costos',
      url: '/listadeprecios',
      icon: 'fa fa-dollar fa-lg mt-1',
    },
    {
      name: 'Ver Compromisos',
      url: '/vercompromisocont',
      icon: 'fa fa-copy fa-lg mt-1 fa-lg mt-1',
    }, 

    {
      title: true,
      name: 'ATENCIÓN – CONSULTORIO ',
      wrapper: {
        element: '',
        attributes: {},
      },
    },

    {
      name: 'Hoja Evolucion',
      url: '/hojaevolucion',
      icon: 'fa fa-edit fa-lg mt-1',
    },

    {
      name: 'Ver Hoja Evolucion',
      url: '/verhojaevolucion',
      icon: 'fa fa-folder-open fa-lg mt-1',
    },*/

    {
      title: true,
      name: 'COMPROMISOS DE PAGO',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Liquidaciones',
      url: '/liquidaciones',
      icon: 'fa fa-list-alt fa-lg mt-1',
    },
    {
      name: 'Costos',
      url: '/listadeprecios',
      icon: 'fa fa-dollar fa-lg mt-1',
    },
    {
      name: 'Nuevo Compromiso',
      url: '/compromiso',
      icon: 'fa fa-handshake-o fa-lg mt-1',
    },
    {
      name: 'Nuevo Garante',
      url: '/garante',
      icon: 'fa fa-user-plus fa-lg mt-1',
    },
    {
      name: 'Ver Compromisos',
      url: '/vercompromiso',
      icon: 'fa fa-file-text-o fa-lg mt-1',
    },
    {
      name: 'Ver Garantes',
      url: '/vergarantes',
      icon: 'fa fa-users fa-lg mt-1',
    },
    {
      name: 'Buscar Compromiso',
      url: '/buscarcompromiso',
      icon: 'fa fa-search fa-lg mt-1',
    },
    {
      name: 'Reportes Compromisos',
      url: '/vercompromisocont',
      icon: 'fa fa-copy fa-lg mt-1 fa-lg mt-1',
    },

    /* {
      name: 'COMPROMISOS ',
      url: '/compromisos',
      children: [
        {
          name: 'Liquidaciones',
          url: '/liquidaciones',
          icon: 'fa fa-list-alt fa-lg mt-1',
        },
        {
          name: 'Costos',
          url: '/listadeprecios',
          icon: 'fa fa-dollar fa-lg mt-1',
        },
        {
          name: 'Nuevo Compromiso',
          url: '/compromiso',
          icon: 'fa fa-handshake-o fa-lg mt-1',
        },
        {
          name: 'Nuevo Garante',
          url: '/garante',
          icon: 'fa fa-user-plus fa-lg mt-1',
        },
        {
          name: 'Ver Compromisos',
          url: '/vercompromiso',
          icon: 'fa fa-file-text-o fa-lg mt-1',
        },
        {
          name: 'Ver Garantes',
          url: '/vergarantes',
          icon: 'fa fa-users fa-lg mt-1',
        },
        {
          name: 'Buscar Compromiso',
          url: '/buscarcompromiso',
          icon: 'fa fa-search fa-lg mt-1',
        },
        {
          name: 'Reportes Compromisos',
          url: '/vercompromisocont',
          icon: 'fa fa-copy fa-lg mt-1 fa-lg mt-1',
        },
      ],
    }, */

  ],
};