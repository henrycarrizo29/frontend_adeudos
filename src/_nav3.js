export default {
  items: [

    {
      title: true,
      name: 'COMPROMISOS DE PAGO',
      wrapper: {
        element: '',
        attributes: {},
      },
    },

    {
      name: 'Nuevo Compromiso',
      url: '/compromiso',
      icon: 'fa fa-handshake-o fa-lg mt-1',
    },
    {
      name: 'Nuevo Garante',
      url: '/garante',
      icon: 'fa fa-user-plus fa-lg mt-1',
    },
    {
      name: 'Ver Compromisos',
      url: '/vercompromiso',
      icon: 'fa fa-file-text-o fa-lg mt-1',
    },
    {
      name: 'Ver Garantes',
      url: '/vergarantes',
      icon: 'fa fa-users fa-lg mt-1',
    },
    {
      name: 'Buscar Compromiso',
      url: '/buscarcompromiso',
      icon: 'fa fa-search fa-lg mt-1',
    },
   /*  {
      name: 'Liquidaciones',
      url: '/liquidaciones',
      icon: 'fa fa-list-alt fa-lg mt-1',
    },
    {
      name: 'Costos',
      url: '/listadeprecios',
      icon: 'fa fa-dollar fa-lg mt-1',
    },
    {
      name: 'Reportes Compromisos',
      url: '/vercompromisocont',
      icon: 'fa fa-copy fa-lg mt-1 fa-lg mt-1',
    }, */
  ],
};
