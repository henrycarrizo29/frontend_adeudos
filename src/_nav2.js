export default {
  items: [
    {
      title: true,
      name: 'ATENCIÓN – CONSULTORIO ',
      wrapper: {
        element: '',
        attributes: {},
      },
    },

    {
      name: 'Hoja Evolucion',
      url: '/hojaevolucion',
      icon: 'fa fa-edit fa-lg mt-1',
    },

    {
      name: 'Ver Hoja Evolucion',
      url: '/verhojaevolucion',
      icon: 'fa fa-folder-open fa-lg mt-1',
    },

    {
      title: true,
      name: 'COMPROMISOS DE PAGO',
      wrapper: {
        element: '',
        attributes: {},
      },
    },

    {
      name: 'Nuevo Compromiso',
      url: '/compromiso',
      icon: 'fa fa-handshake-o fa-lg mt-1',
    },
    {
      name: 'Nuevo Garante',
      url: '/garante',
      icon: 'fa fa-user-plus fa-lg mt-1',
    },
    {
      name: 'Ver Compromisos',
      url: '/vercompromiso',
      icon: 'fa fa-file-text-o fa-lg mt-1',
    },
    {
      name: 'Ver Garantes',
      url: '/vergarantes',
      icon: 'fa fa-users fa-lg mt-1',
    },
    {
      name: 'Buscar Compromiso',
      url: '/buscarcompromiso',
      icon: 'fa fa-search fa-lg mt-1',
    },

  ],
};